package com.gaboardi.common.model

sealed class ApiResponse<T> {
    data class Success<T>(val body: T, val code: Int = 200) : ApiResponse<T>()

    data class Error<T>(val message: ErrorMessage) : ApiResponse<T>()

    companion object {

        /**
         * Returns [ApiResponse.Success] instance.
         * @param body Data to emit with status.
         */
        fun <T> success(body: T, code: Int = 200) =
            Success(body, code)

        /**
         * Returns [ApiResponse.Error] instance.
         * @param message Description of failure.
         */
        fun <T> error(message: ErrorMessage) =
            Error<T>(message)

        fun <T> error(message: String, code: Int = 200) =
            Error<T>(ErrorMessage.of(message, code = code))
    }

}