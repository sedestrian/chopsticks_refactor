package com.gaboardi.common.model

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 */
open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun consume(lambda: ((content: T) -> Unit)){
        if (!hasBeenHandled) {
            hasBeenHandled = true
            lambda.invoke(content)
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peek(): T = content
}