package com.gaboardi.common.model

import androidx.annotation.StringRes

sealed class ErrorMessage(val type: String? = null, val code: Int = 200) {
    data class StringError(val message: String, private val id: String? = null, private val serverCode: Int = 200): ErrorMessage(id, serverCode)
    data class ResourceError(@StringRes val message: Int, private val id: String? = null, private val serverCode: Int = 200): ErrorMessage(id, serverCode)

    companion object {
        fun of(message: String, id: String? = null, code: Int = 200) = StringError(message, id, code)
        fun of(@StringRes message: Int, id: String? = null, code: Int = 200) = ResourceError(message, id, code)
    }
}