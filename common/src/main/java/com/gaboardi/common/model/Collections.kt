package com.gaboardi.common.model

object Collections{
    val Tables = "tables"
    val Users = "users"
    val Restaurants = "restaurants"
    val Orders = "orders"
    val Plates = "plates"
}