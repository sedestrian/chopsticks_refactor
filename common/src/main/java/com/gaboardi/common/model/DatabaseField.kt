package com.gaboardi.common.model

object DatabaseField {
    object Restaurant{
        const val Id = "id"
        const val UserId = "userId"
    }

    object Table {
        const val Id = "id"
        const val Code = "code"
        const val DynamicLink = "dynamicLink"
        const val Users = "users"
        const val UserIds = "userIds"
    }

    object Order{
        const val Id = "id"
        const val RestaurantId = "restaurantId"
        const val TableId = "tableId"
        const val UserId = "userId"
    }

    object Plate{
        const val Id = "id"
        const val OrderId = "orderId"
        const val UserId = "userId"
        const val Quantity = "quantity"
        const val Received = "received"
        const val State = "state"
    }
}