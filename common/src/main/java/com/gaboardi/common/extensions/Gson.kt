package com.gaboardi.common.extensions

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

inline fun <reified T> Gson.fromJson(json: String): T {
    return Gson().fromJson(json, object: TypeToken<T>(){}.type)
}

inline fun <reified T> String.fromJson(): T {
    return Gson().fromJson<T>(this)
}

fun Any?.toJson(): String{
    return Gson().toJson(this)
}