package com.gaboardi.common.extensions

import java.security.MessageDigest

fun String.hash(hashName: String = "MD5"): String {
    val bytes = MessageDigest
        .getInstance(hashName)
        .digest(toByteArray())
    return printHexBinary(bytes)
}

private val HEX_CHARS = "0123456789abcdef".toCharArray()

fun printHexBinary(data: ByteArray): String {
    val r = StringBuilder(data.size * 2)
    data.forEach { b ->
        val i = b.toInt()
        r.append(HEX_CHARS[i shr 4 and 0xF])
        r.append(HEX_CHARS[i and 0xF])
    }
    return r.toString()
}