package com.gaboardi.common.extensions

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.core.content.ContextCompat

fun Context.vibrate() {
    val v = ContextCompat.getSystemService(this, Vibrator::class.java)
    when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
            val effect = VibrationEffect.createPredefined(VibrationEffect.EFFECT_HEAVY_CLICK)
            v?.vibrate(effect)
        }
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
            val effect = VibrationEffect.createOneShot(20, 255)
            v?.vibrate(effect)
        }
        else -> {
            v?.vibrate(20)
        }
    }
}