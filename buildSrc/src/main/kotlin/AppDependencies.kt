import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.kotlin.dsl.project

object AppDependencies {
    //region Kotlin
    private const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    //endregion

    //region Base libs
    private const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    private const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    private const val fragment = "androidx.fragment:fragment-ktx:${Versions.fragment}"
    private const val activity = "androidx.activity:activity-ktx:${Versions.activity}"
    private const val legacy = "androidx.legacy:legacy-support-v4:${Versions.legacy}"
    //endregion

    //region Hilt
    private const val hilt = "com.google.dagger:hilt-android:${Versions.hilt}"
    private const val hiltViewModel =
        "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltViewmodel}"
    private const val hiltAndroidCompiler =
        "com.google.dagger:hilt-android-compiler:${Versions.hilt}"
    private const val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.hiltViewmodel}"
    //endregion

    //region MVI
    private const val orbit = "org.orbit-mvi:orbit-core:${Versions.orbit}"
    private const val orbitViewModel = "org.orbit-mvi:orbit-viewmodel:${Versions.orbit}"
    private const val orbitCoroutines = "org.orbit-mvi:orbit-coroutines:${Versions.orbit}"
    //endregion

    //region UI
    const val material = "com.google.android.material:material:${Versions.material}"
    const val swipeRefresh =
        "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipeRefresh}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val firebaseAuthUi = "com.firebaseui:firebase-ui-auth:${Versions.authentication}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recycler}"
    const val lottie = "com.airbnb.android:lottie:${Versions.lottie}"
    const val ratingBar = "com.github.ome450901:SimpleRatingBar:${Versions.ratingBar}"
    const val zxing = "com.google.zxing:core:${Versions.zxing}"
    const val qrScanner = "com.google.mlkit:barcode-scanning:${Versions.qrScanner}"
    //endregion

    //region Camera
    const val cameraX = "androidx.camera:camera-core:${Versions.cameraX}"
    const val camera2 = "androidx.camera:camera-camera2:${Versions.cameraX}"
    const val cameraLifecycle = "androidx.camera:camera-lifecycle:${Versions.cameraX}"
    const val cameraView = "androidx.camera:camera-view:${Versions.cameraXBeta}"
    const val cameraExtensions = "androidx.camera:camera-extensions:${Versions.cameraXBeta}"
    const val guava = "com.google.guava:guava:${Versions.guava}"
    //endregion

    //region Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitLogging =
        "com.squareup.okhttp3:logging-interceptor:${Versions.retrofitLogging}"
    const val gsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    //endregion

    //region Coil
    const val coil = "io.coil-kt:coil:${Versions.coil}"
    //endregion

    //region Gson
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    //endregion

    //region Desugaring
    const val coreDesugaring = "com.android.tools:desugar_jdk_libs:${Versions.desugaring}"
    //endregion

    //region Facebook login
    const val facebook = "com.facebook.android:facebook-android-sdk:${Versions.facebook}"
    //endregion

    //region Play services login
    const val playLogin = "com.google.android.gms:play-services-auth:${Versions.playServicesAuth}"
    //endregion

    //region Firebase
    const val firebaseBom = "com.google.firebase:firebase-bom:${Versions.firebase}"
    const val crashlitics = "com.google.firebase:firebase-crashlytics-ktx"
    const val analytics = "com.google.firebase:firebase-analytics"
    const val dynamicLinks = "com.google.firebase:firebase-dynamic-links-ktx"
    const val firebaseCommon = "com.google.firebase:firebase-common-ktx"
    const val firestore = "com.google.firebase:firebase-firestore-ktx"
    const val firebaseAuth = "com.google.firebase:firebase-auth"
    const val analyticsKtx = "com.google.firebase:firebase-analytics-ktx"
    const val performance = "com.google.firebase:firebase-perf"
    //endregion

    //region Coroutines
    private const val coroutines =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
    private const val coroutinesAndroid =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    private const val coroutinesPlayServices =
        "org.jetbrains.kotlinx:kotlinx-coroutines-play-services:${Versions.coroutines}"
    //endregion

    //region Room
    private const val room = "androidx.room:room-runtime:${Versions.room}"
    private const val roomKtx = "androidx.room:room-ktx:${Versions.room}"
    private const val roomProcessor = "androidx.room:room-compiler:${Versions.room}"
    //endregion

    //region Lifecycle
    const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val livedata = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
    const val savedState = "androidx.lifecycle:lifecycle-viewmodel-savedstate:${Versions.lifecycle}"
    //endregion

    //region Navigation
    private const val navigationFragment =
        "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    private const val navigationUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    //endregion

    object Modules {
        const val app = ":app"
        const val common = ":common"
        const val commonui = ":commonui"
        const val home = ":home"
        const val host = ":host"
        const val login = ":login"
        const val navigation = ":navigation"
        const val tablecreation = ":tablecreation"
        const val orderdetail = ":orderdetail"
        const val plate = ":plate"
        const val platecreation = ":platecreation"
        const val platechoicecreation = ":platechoicecreation"
        const val qrcode = ":qrcode"
        const val restaurant = ":restaurant"
        const val restaurantcreation = ":restaurantcreation"
        const val restaurantdetail = ":restaurantdetail"
        const val splash = ":splash"
        const val table = ":table"
        const val tabledetail = ":tabledetail"
        const val tableinvite = ":tableinvite"
        const val tablejoin = ":tablejoin"
        const val user = ":user"

        val all = arrayListOf<String>().apply {
            add(common)
            add(commonui)
            add(home)
            add(host)
            add(login)
            add(navigation)
            add(tablecreation)
            add(orderdetail)
            add(plate)
            add(platecreation)
            add(platechoicecreation)
            add(qrcode)
            add(restaurant)
            add(restaurantcreation)
            add(restaurantdetail)
            add(splash)
            add(table)
            add(tabledetail)
            add(tableinvite)
            add(tablejoin)
            add(user)
        }
    }

    fun DependencyHandler.implementFirebase() {
        add(Implementation, platform(firebaseBom))
        add(Implementation, crashlitics)
        add(Implementation, analytics)
        add(Implementation, firebaseCommon)
        add(Implementation, firestore)
        add(Implementation, firebaseAuth)
        add(Implementation, analyticsKtx)
        add(Implementation, performance)
        add(Implementation, firebaseAuthUi)
        add(Implementation, dynamicLinks)
    }

    fun DependencyHandler.implementHilt() {
        add(Implementation, hilt)
        add(Kapt, hiltAndroidCompiler)
        add(Kapt, hiltCompiler)
    }

    fun DependencyHandler.implementRetrofit() {
        add(Implementation, retrofit)
        add(Implementation, retrofitLogging)
        add(Implementation, gsonConverter)
    }

    fun DependencyHandler.implementCameraX() {
        add(Implementation, cameraX)
        add(Implementation, camera2)
        add(Implementation, cameraLifecycle)
        add(Implementation, cameraView)
        add(Implementation, cameraExtensions)
        add(Implementation, guava)
    }

    fun DependencyHandler.implementApp() {
        add(Implementation, kotlinStdLib)
        add(Implementation, coreKtx)
        add(Implementation, appcompat)
    }

    fun DependencyHandler.implementNavigation() {
        add(Implementation, navigationFragment)
        add(Implementation, navigationUi)
    }

    fun DependencyHandler.implementRoom() {
        add(Implementation, room)
        add(Implementation, roomKtx)
        add(Kapt, roomProcessor)
    }

    fun DependencyHandler.implementOrbit() {
        add(Implementation, orbit)
        add(Implementation, orbitViewModel)
        add(Implementation, orbitCoroutines)
    }

    fun DependencyHandler.implementCoroutines() {
        add(Implementation, coroutines)
        add(Implementation, coroutinesAndroid)
        add(Implementation, coroutinesPlayServices)
    }
}

private const val Implementation = "implementation"
private const val Kapt = "kapt"
private const val CoreLibraryDesugaring = "coreLibraryDesugaring"

//util functions for adding the different type dependencies from build.gradle file
fun DependencyHandler.implementModules(list: List<String>) {
    list.forEach { dependency ->
        add(Implementation, project(dependency))
    }
}

fun DependencyHandler.implementBom(dependency: String) {
    add(Implementation, platform(dependency))
}

fun DependencyHandler.implementModule(module: String) {
    add(Implementation, project(module))
}

fun DependencyHandler.kapt(list: List<String>) {
    list.forEach { dependency ->
        add(Kapt, dependency)
    }
}

fun DependencyHandler.implementation(list: List<String>) {
    list.forEach { dependency ->
        add(Implementation, dependency)
    }
}