package com.gaboardi.restaurantdetail.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.commonui.extensions.setGone
import com.gaboardi.commonui.extensions.setVisible
import com.gaboardi.restaurantdetail.R
import com.gaboardi.restaurantdetail.adapter.TableDiff
import com.gaboardi.restaurantdetail.databinding.TableItemBinding
import com.gaboardi.table.model.ui.Table
import java.time.format.DateTimeFormatter

class TableVH(private val binding: TableItemBinding) : RecyclerView.ViewHolder(binding.root) {
    private val dateFormat = DateTimeFormatter.ofPattern("HH:mm - dd/MM/yy")

    fun bind(
        table: Table,
        onClick: ((order: Table) -> Unit)?,
        onLongClick: ((order: Table) -> Unit)?
    ) {
        binding.orderName.text = table.name
        binding.date.text = table.date.format(dateFormat)

        binding.cardView.setOnClickListener {
            onClick?.invoke(table)
        }

        binding.cardView.setOnLongClickListener {
            onLongClick?.invoke(table)
            true
        }

        val isTableResource = if(table.isTable) R.drawable.group else R.drawable.single
        binding.isTableImage.setImageResource(isTableResource)

//        binding.plates.text = table.plates.size.toString()

        binding.notesLabel.setGone()
        binding.notes.setGone()

        hidePrice()
        hideRating()
        /*if (table.price != null && table.priceTime != null) {
            showPrice()
            binding.price.text =
                binding.root.context.getString(R.string.price_value, table.price)
            binding.priceImage.setImageResource(if (table.priceTime == 0) R.drawable.ic_sunlight else R.drawable.ic_moona)
        } else {
            hidePrice()
        }

        if (table.rating == null) {
            hideRating()
        } else {
            showRating()
            table.rating?.let {
                binding.ratingBar.rating = it.toFloat()
            }
        }*/
    }

    fun bindForChanges(diff: TableDiff) {
        if (diff.name != null) binding.orderName.text = diff.name
        if (diff.date != null) binding.date.text = diff.date.format(dateFormat)
        if (diff.isTable != null) {
            val isTableResource = if(diff.isTable) R.drawable.group else R.drawable.single
            binding.isTableImage.setImageResource(isTableResource)
        }
        /*if (diff.plates != null) binding.plates.text = diff.plates.toString()
        if (diff.priceTime != null)
            binding.priceImage.setImageResource(
                if (diff.priceTime == 0)
                    R.drawable.ic_sunlight
                else
                    R.drawable.ic_moona
            )
        if (diff.price != null) binding.price.text =
            binding.root.context.getString(R.string.price_value, diff.price)
        if (diff.rating != null) binding.ratingBar.rating = diff.rating.toFloat()*/
    }

    private fun hidePrice() {
        binding.price.setGone()
        binding.priceImage.setGone()
        binding.priceLabel.setGone()
    }

    private fun showPrice() {
        binding.price.setVisible()
        binding.priceImage.setVisible()
        binding.priceLabel.setVisible()
    }

    private fun hideRating() {
        binding.voteLabel.setGone()
        binding.ratingBar.setGone()
    }

    private fun showRating() {
        binding.voteLabel.setVisible()
        binding.ratingBar.setVisible()
    }
}