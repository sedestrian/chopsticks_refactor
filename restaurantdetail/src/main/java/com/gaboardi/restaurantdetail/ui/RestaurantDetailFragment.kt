package com.gaboardi.restaurantdetail.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gaboardi.common.extensions.dp
import com.gaboardi.commonui.utility.SpacingItemDecoration
import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.gaboardi.restaurantdetail.R
import com.gaboardi.restaurantdetail.adapter.TablesAdapter
import com.gaboardi.restaurantdetail.databinding.FragmentRestaurantDetailBinding
import com.gaboardi.restaurantdetail.dialog.OrderActionsBottomSheetDialog
import com.gaboardi.restaurantdetail.viewmodel.RestaurantDetailViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.card.MaterialCardView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
class RestaurantDetailFragment : Fragment() {
    private lateinit var binding: FragmentRestaurantDetailBinding
    private lateinit var bottomSheet: BottomSheetBehavior<MaterialCardView>

    private val viewModel: RestaurantDetailViewModel by viewModels()
    private val args: RestaurantDetailFragmentArgs by navArgs()

    private lateinit var adapter: TablesAdapter

    private val orderActionsTag = "OrderActions"

    @ExperimentalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRestaurantDetailBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        bottomSheet = BottomSheetBehavior.from(binding.bottomSheet)
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        setupOrders()
        setupToolbar()
        followUi()

        observe()
        listen()

        viewModel.loadRestaurant(args.restaurantId)

        bottomSheet.isGestureInsetBottomIgnored = true
    }

    private fun observe() {
        viewModel.restaurant.observe(viewLifecycleOwner) {
            renderRestaurant(it)
        }
        viewModel.peekHeight.observe(viewLifecycleOwner) {
            renderPeekHeight(it)
        }
    }

    private fun renderRestaurant(restaurant: RestaurantUI) {
        binding.restaurantName.text = restaurant.name

        binding.address.text = restaurant.address

        binding.phoneNumber.text = restaurant.phone
        binding.phoneGroup.isVisible = !restaurant.phone.isNullOrBlank()

        binding.website.text = restaurant.website
        binding.websiteGroup.isVisible = !restaurant.website.isNullOrBlank()

        adapter.setItems(restaurant.tables)

        binding.restaurantName.doOnLayout {
            getBottomSheetPeekHeight()
        }
    }

    private fun renderPeekHeight(peekHeight: Int) {
        bottomSheet.peekHeight = 0
        bottomSheet.setPeekHeight(peekHeight, true)
        if (bottomSheet.state != BottomSheetBehavior.STATE_EXPANDED)
            bottomSheet.state = BottomSheetBehavior.STATE_COLLAPSED

        binding.ordersRecycler.updatePadding(bottom = peekHeight)

        binding.addButton.updateLayoutParams<CoordinatorLayout.LayoutParams> {
            updateMargins(bottom = peekHeight)
        }
    }

    private fun followUi() {
        binding.addButton.setOnClickListener {
            val action =
                RestaurantDetailFragmentDirections.actionRestaurantDetailFragmentToOrderCreationFlow(
                    args.restaurantId
                )
            findNavController().navigate(action)
        }
    }

    private fun setupToolbar() {
        binding.toolbar.title = getString(R.string.orders_title)
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        binding.toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.table_join -> {
                    val action =
                        RestaurantDetailFragmentDirections.actionRestaurantDetailFragmentToJoinTableFlow(
                            args.restaurantId
                        )
                    findNavController().navigate(action)
                    true
                }
                else -> false
            }
        }
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val window = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        view.updatePadding(top = window.top, bottom = window.bottom)
        binding.bottomSheet.setContentPadding(
            binding.bottomSheet.contentPaddingLeft,
            binding.bottomSheet.contentPaddingTop,
            binding.bottomSheet.contentPaddingRight,
            window.bottom
        )
        viewModel.updateInsetPeekHeight(window.bottom)
        insets
    }

    private fun getBottomSheetPeekHeight() {
        var titleHeight = binding.restaurantName.height

        binding.pill.doOnLayout {
            titleHeight += binding.pill.marginTop +
                    binding.pill.height +
                    binding.restaurantName.marginTop +
                    binding.dividerAbove.marginTop

            viewModel.updateCalculatedPeekHeight(titleHeight)
        }
    }

    private fun listen() {
        adapter.setOnClickListener {
            val action = RestaurantDetailFragmentDirections.actionRestaurantDetailFragmentToTableDetailFlow(
                    it.id
            )
            findNavController().navigate(action)
        }

        adapter.setOnLongClickListener {
            val dialog = OrderActionsBottomSheetDialog {
//                viewModel.deleteOrder(it.id)
            }
            dialog.show(childFragmentManager, orderActionsTag)
        }
    }

    /*private fun observe() {
        viewModel.restaurant.observe(viewLifecycleOwner) {


//            adapter.setItems(it.orders.sortedByDescending { order -> order.date })

            binding.websiteLayer.setOnClickListener { _ ->
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(it.website)
                startActivity(i)
            }

            binding.phoneLayer.setOnClickListener { _ ->
                val intent =
                    Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", it.phone, null))
                startActivity(intent)
            }

            binding.addressLayer.setOnClickListener { _ ->
                val uri =
                    Uri.parse("google.navigation:q=${it.name},${it.address}")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            }

            *//*it.averageRating?.let { rating ->
                binding.likesGroup.setVisible()
                binding.likes.rating = rating.toFloat()
            } ?: setupEmptyLikes()*//*
            setupEmptyLikes()

            setPrices(it.prices)
        }
    }*/

    /*private fun setPrices(prices: List<Price>) {
        if (prices.isNotEmpty()) {
            binding.pricesGroup.setVisible()
            val week = prices.filter { Days.from(it.days) == Days.WEEK }
            val weekend = prices.filter { Days.from(it.days) == Days.WEEKEND }

            if (week.isEmpty()) {
                hideWeekPrices()
            } else {
                showWeekPrices()
                val day = week.firstOrNull { Time.from(it.time) == Time.LUNCH }
                val night = week.firstOrNull { Time.from(it.time) == Time.DINNER }

                if (day != null && night != null) binding.weekPriceDash.setVisible() else binding.weekPriceDash.setGone()

                day?.let {
                    showWeekDayPrices()
                    binding.weekPriceDay.text = getString(R.string.price_value, it.cost)
                } ?: hideWeekDayPrices()

                night?.let {
                    showWeekNightPrices()
                    binding.weekPriceNight.text = getString(R.string.price_value, it.cost)
                } ?: hideWeekNightPrices()
            }

            if (weekend.isEmpty()) {
                hideWeekendPrices()
            } else {
                showWeekendPrices()
                val day = weekend.firstOrNull { Time.from(it.time) == Time.LUNCH }
                val night = weekend.firstOrNull { Time.from(it.time) == Time.DINNER }

                if (day != null && night != null) binding.weekendPriceDash.setVisible() else binding.weekendPriceDash.setGone()

                day?.let {
                    showWeekendDayPrices()
                    binding.weekendDayPrice.text = getString(R.string.price_value, it.cost)
                } ?: hideWeekendDayPrices()

                night?.let {
                    showWeekendNightPrices()
                    binding.weekendNightPrice.text = getString(R.string.price_value, it.cost)
                } ?: hideWeekendNightPrices()
            }
        } else {
            binding.pricesGroup.setGone()
        }
    }*/

    /*private fun setupEmptyLikes() {
        binding.likesGroup.setVisible()
        binding.likes.rating = 0f
    }

    private fun hideWeekDayPrices() {
        binding.weekPriceDayIcon.setGone()
        binding.weekPriceDay.setGone()
    }

    private fun hideWeekNightPrices() {
        binding.weekPriceNightIcon.setGone()
        binding.weekPriceNight.setGone()
    }

    private fun showWeekDayPrices() {
        binding.weekPriceDayIcon.setVisible()
        binding.weekPriceDay.setVisible()
    }

    private fun showWeekNightPrices() {
        binding.weekPriceNightIcon.setVisible()
        binding.weekPriceNight.setVisible()
    }

    private fun hideWeekPrices() {
        binding.weekPricesLabel.setGone()
        hideWeekDayPrices()
        binding.weekPriceDash.setGone()
        hideWeekNightPrices()
    }

    private fun showWeekPrices() {
        binding.weekPricesLabel.setVisible()
        showWeekDayPrices()
        binding.weekPriceDash.setVisible()
        showWeekNightPrices()
    }

    private fun hideWeekendDayPrices() {
        binding.weekendDayPriceIcon.setGone()
        binding.weekendDayPrice.setGone()
    }

    private fun hideWeekendNightPrices() {
        binding.weekendNightPriceIcon.setGone()
        binding.weekendNightPrice.setGone()
    }

    private fun showWeekendDayPrices() {
        binding.weekendDayPriceIcon.setVisible()
        binding.weekendDayPrice.setVisible()
    }

    private fun showWeekendNightPrices() {
        binding.weekendNightPriceIcon.setVisible()
        binding.weekendNightPrice.setVisible()
    }

    private fun hideWeekendPrices() {
        binding.weekendPricesLabel.setGone()
        hideWeekendDayPrices()
        binding.weekendPriceDash.setGone()
        hideWeekendNightPrices()
    }

    private fun showWeekendPrices() {
        binding.weekendPricesLabel.setVisible()
        showWeekendDayPrices()
        binding.weekendPriceDash.setVisible()
        showWeekendNightPrices()
    }*/

    private fun setupOrders() {
        adapter = TablesAdapter()
        binding.ordersRecycler.layoutManager = LinearLayoutManager(context)
        binding.ordersRecycler.addItemDecoration(SpacingItemDecoration(8.dp))
        binding.ordersRecycler.adapter = adapter
    }
}