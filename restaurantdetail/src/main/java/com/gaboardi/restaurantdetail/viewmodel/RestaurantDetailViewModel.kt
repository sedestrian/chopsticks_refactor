package com.gaboardi.restaurantdetail.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.gaboardi.restaurant.usecase.ObserveRestaurantUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RestaurantDetailViewModel @Inject constructor(
    private val getRestaurantUseCase: ObserveRestaurantUseCase,
) : ViewModel(){
    private var insetHeight: Int? = null
    private var calculatedPeekHeight: Int? = null

    private val _restaurant: MutableLiveData<RestaurantUI> = MutableLiveData()
    val restaurant: LiveData<RestaurantUI> = _restaurant

    /*private val _loading: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val loading: LiveData<Event<Boolean>> = _loading

    private val _error: MutableLiveData<Event<ErrorMessage>> = MutableLiveData()
    val error: LiveData<Event<ErrorMessage>> = _error*/

    private val _peekHeight: MutableLiveData<Int> = MutableLiveData()
    val peekHeight: LiveData<Int> = _peekHeight

    fun loadRestaurant(restaurantId: String) {
        viewModelScope.launch {
            getRestaurantUseCase.observeRestaurant(restaurantId).collect {
                _restaurant.postValue(it)
            }
        }
    }

    /*fun deleteOrder(orderId: String) {
        viewModelScope.launch {
            deleteOrderUseCase.delete(orderId).collect { state ->
                when (state) {
                    is State.Loading -> {
                    }
                    is State.Success -> {
                    }
                    is State.Error -> {
                        _error.postValue(Event(state.message))
                    }
                }
            }
        }
    }*/

    fun updateCalculatedPeekHeight(height: Int) {
        this.calculatedPeekHeight = height
        updatePeekHeight()
    }

    fun updateInsetPeekHeight(height: Int) {
        this.insetHeight = height
        updatePeekHeight()
    }

    private fun updatePeekHeight() {
        if (calculatedPeekHeight != null && insetHeight != null) {
            val peek = (calculatedPeekHeight ?: 0) + (insetHeight ?: 0)
            _peekHeight.postValue(peek)
        }
    }
}