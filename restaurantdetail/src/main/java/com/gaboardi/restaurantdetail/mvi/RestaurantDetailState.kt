package com.gaboardi.restaurantdetail.mvi

import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.gaboardi.table.model.ui.Table

data class RestaurantDetailState(
    val insetHeight: Int? = null,
    val textHeight: Int? = null,
    val restaurant: RestaurantUI? = null,
    val tables: List<Table> = listOf()
)