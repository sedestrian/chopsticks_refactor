package com.gaboardi.restaurantdetail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.restaurantdetail.databinding.TableItemBinding
import com.gaboardi.restaurantdetail.viewholder.TableVH
import com.gaboardi.table.model.ui.Table

class TablesAdapter : RecyclerView.Adapter<TableVH>() {
    private lateinit var inflater: LayoutInflater

    private var onClickListener: ((order: Table) -> Unit)? = null
    private var onLongClickListener: ((order: Table) -> Unit)? = null
    private val items: MutableList<Table> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TableVH {
        if (!this::inflater.isInitialized) inflater = LayoutInflater.from(parent.context)
        val binding = TableItemBinding.inflate(inflater, parent, false)
        return TableVH(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TableVH, position: Int) {
        holder.bind(items[position], {
            onClickListener?.invoke(it)
        }, {
            onLongClickListener?.invoke(it)
        })
    }

    override fun onBindViewHolder(holder: TableVH, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty())
            super.onBindViewHolder(holder, position, payloads)
        else
            payloads.forEach {
                holder.bindForChanges(it as TableDiff)
            }
    }

    fun setItems(tables: List<Table>) {
        val diffUtil = TableDiffUtil(items, tables)
        val diff = DiffUtil.calculateDiff(diffUtil)
        diff.dispatchUpdatesTo(this)
        items.clear()
        items.addAll(tables)
    }

    fun setOnClickListener(listener: ((order: Table) -> Unit)?) {
        onClickListener = listener
    }

    fun setOnLongClickListener(listener: ((order: Table) -> Unit)?) {
        onLongClickListener = listener
    }
}