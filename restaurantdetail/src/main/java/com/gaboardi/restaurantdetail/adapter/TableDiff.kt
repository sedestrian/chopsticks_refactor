package com.gaboardi.restaurantdetail.adapter

import java.time.LocalDateTime

data class TableDiff(
    val name: String?,
    val date: LocalDateTime?,
    val isTable: Boolean?,
    /*val rating: Double?,
    val price: Double?,
    val priceTime: Int?,
    val plates: Int?*/
){
    fun isEmpty(): Boolean =
        name == null &&
                date == null &&
                isTable == null /*&&
                rating == null &&
                price == null &&
                priceTime == null &&
                plates == null*/
}