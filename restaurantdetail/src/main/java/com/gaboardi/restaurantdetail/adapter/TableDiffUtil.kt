package com.gaboardi.restaurantdetail.adapter

import androidx.recyclerview.widget.DiffUtil
import com.gaboardi.table.model.ui.Table

class TableDiffUtil(private val oldList: List<Table>, private val newList: List<Table>): DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]
        return oldItem == newItem
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        val changes = TableDiff(
            if(oldItem.name != newItem.name) newItem.name else null,
            if(oldItem.date != newItem.date) newItem.date else null,
            if(oldItem.isTable != newItem.isTable) newItem.isTable else null,
            /*if(oldItem.rating != newItem.rating) newItem.rating else null,
            if(oldItem.price != newItem.price) newItem.price else null,
            if(oldItem.priceTime != newItem.priceTime) newItem.priceTime else null,
            if(oldItem.plates.size != newItem.plates.size) newItem.plates.size else null*/
        )

        return if(changes.isEmpty()) null else changes
    }
}