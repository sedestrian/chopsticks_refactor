package com.gaboardi.restaurantdetail.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaboardi.restaurantdetail.databinding.OrderActionsBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class OrderActionsBottomSheetDialog(val onDelete: (() -> Unit)?) : BottomSheetDialogFragment() {
    private lateinit var binding: OrderActionsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = OrderActionsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.deleteLayout.setOnClickListener {
            onDelete?.invoke()
            dismiss()
        }
    }
}