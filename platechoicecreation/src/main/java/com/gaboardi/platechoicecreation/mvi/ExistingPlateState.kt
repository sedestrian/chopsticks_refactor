package com.gaboardi.platechoicecreation.mvi

import com.gaboardi.plate.model.ui.Plate

data class ExistingPlateState(
    val tablePlates: List<Plate> = listOf()
)