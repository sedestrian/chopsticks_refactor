package com.gaboardi.platechoicecreation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gaboardi.platechoicecreation.mvi.ExistingPlateSideEffect
import com.gaboardi.platechoicecreation.mvi.ExistingPlateState
import com.gaboardi.table.usecase.DownloadTableDataUseCase
import com.gaboardi.table.usecase.ObserveTableUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class ExistingPlateViewModel @Inject constructor(
    private val subscribeToTable: DownloadTableDataUseCase,
    private val observeTableUseCase: ObserveTableUseCase
) : ViewModel(), ContainerHost<ExistingPlateState, ExistingPlateSideEffect> {
    override val container: Container<ExistingPlateState, ExistingPlateSideEffect> = container(
        ExistingPlateState()
    )

    fun something(tableId: String) {
        viewModelScope.launch {
            launch { getTable(tableId) }
            launch { subscribeToTable.execute(tableId).collect { } }
        }
    }

    private suspend fun getTable(tableId: String) {
        observeTableUseCase.execute(tableId).collect {
            /*it?.let {
                val orders = it.users.mapNotNull { it.orderId }
                observeOrders(orders)
                getOrders(orders)
                getPlates(orders)
            }*/
        }
    }

    private suspend fun observeOrders(orders: List<String>) = intent {
        /*observeOrdersUseCase.execute(orders).collect {
            *//*val orders = it.orders + it.userOrder
            val plates = orders.mapNotNull { it }.flatMap { it.plates }*//*
        }*/
    }

    private suspend fun getOrders(orders: List<String>) {
        /*coroutineScope {
            launch { subscribeToTableOrders.execute(orders).collect {} }
        }*/
    }

    private suspend fun getPlates(orders: List<String>) {
        /*coroutineScope {
            launch { subscribeToPlateListUseCase.execute(orders).collect {} }
        }*/
    }
}