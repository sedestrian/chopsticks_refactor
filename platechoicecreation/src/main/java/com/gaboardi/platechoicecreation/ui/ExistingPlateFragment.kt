package com.gaboardi.platechoicecreation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.gaboardi.platechoicecreation.databinding.FragmentExistingPlateBinding
import com.gaboardi.platechoicecreation.viewmodel.ExistingPlateViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ExistingPlateFragment : Fragment() {
    private lateinit var binding: FragmentExistingPlateBinding

    private val viewModel: ExistingPlateViewModel by viewModels()
    private val args: ExistingPlateFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentExistingPlateBinding.inflate(inflater, container, false)
        return binding.root
    }
}