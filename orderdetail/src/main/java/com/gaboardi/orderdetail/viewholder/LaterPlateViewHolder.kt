package com.gaboardi.orderdetail.viewholder

import android.content.Context
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.common.extensions.vibrate
import com.gaboardi.commonui.extensions.colorCode
import com.gaboardi.commonui.extensions.getColorAttribute
import com.gaboardi.commonui.extensions.setGone
import com.gaboardi.commonui.extensions.setVisible
import com.gaboardi.commonui.inputflilter.PlateCodeInputFilter
import com.gaboardi.orderdetail.R
import com.gaboardi.orderdetail.callback.PlatesHandler
import com.gaboardi.orderdetail.databinding.LaterPlateItemBinding
import com.gaboardi.orderdetail.interfaces.PlateDiffViewHolder
import com.gaboardi.plate.model.ui.PlateDiff
import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.model.ui.PlateUI

class LaterPlateViewHolder(val binding: LaterPlateItemBinding) :
    RecyclerView.ViewHolder(binding.root), PlateDiffViewHolder {

    fun bind(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        val context = binding.root.context

        binding.plateName.text = plate.name
        setupNumberPicker(context, plate, platesHandler)

        binding.plusButton.setOnClickListener {
            context.vibrate()
            binding.numberPicker.value = binding.numberPicker.value + 1
            platesHandler.onQuantitySet(plate.id, binding.numberPicker.value)
        }

        binding.minusButton.setOnClickListener {
            context.vibrate()
            binding.numberPicker.value = binding.numberPicker.value - 1
            platesHandler.onQuantitySet(plate.id, binding.numberPicker.value)
        }

        binding.stateIcon.setOnClickListener {
            context.vibrate()
            platesHandler.changeGroup(plate, PlateState.ORDERED)
        }

        showCode(plate)
    }

    override fun bindForChanges(
        diff: PlateDiff,
        plate: PlateUI.Plate,
        platesHandler: PlatesHandler
    ) {

    }

    private fun setupNumberPicker(context: Context, plate: PlateUI.Plate, handler: PlatesHandler) {
        binding.numberPicker.setSelectedTypeface(
            ResourcesCompat.getFont(
                context,
                R.font.comfortaa_bold
            )
        )
        binding.numberPicker.typeface = ResourcesCompat.getFont(
            context,
            R.font.comfortaa_regular
        )
        binding.numberPicker.setOnValueChangedListener { numberPicker, oldValue, newValue ->
            handler.onQuantitySet(plate.id, newValue)
            if (newValue != oldValue) {
                numberPicker.context.vibrate()
            }
        }
        binding.numberPicker.value = plate.quantity
    }

    private fun showCode(plate: PlateUI.Plate) {
        if (plate.code.isNotEmpty()) {
            binding.codeFlow.setVisible()
            binding.codeDivider.setVisible()
            binding.plateCode.text = plate.code.colorCode(
                binding.root.context.getColorAttribute(R.attr.colorPrimary)
            )
        } else {
            binding.codeFlow.setGone()
            binding.codeDivider.setGone()
        }
    }
}