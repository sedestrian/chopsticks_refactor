package com.gaboardi.chopsticks.order.viewholders

import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.orderdetail.databinding.PlatesListHeaderBinding
import com.gaboardi.plate.model.ui.PlateUI

class PlatesHeaderViewHolder(val binding: PlatesListHeaderBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(header: PlateUI.Header) {
        binding.title.text = binding.root.context.getString(header.title)
    }
}