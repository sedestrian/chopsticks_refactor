package com.gaboardi.chopsticks.order.viewholders

import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.orderdetail.databinding.PlatesListEmptyBinding
import com.gaboardi.plate.model.ui.EmptyPlateType
import com.gaboardi.plate.model.ui.PlateUI

class EmptyPlatesViewHolder(val binding: PlatesListEmptyBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(empty: PlateUI.Empty, onClick: ((type: EmptyPlateType) -> Unit)?) {
        binding.constraintLayout.setOnClickListener {
            onClick?.invoke(empty.type)
        }
    }
}