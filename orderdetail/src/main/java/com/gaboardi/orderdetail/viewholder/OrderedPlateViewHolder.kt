package com.gaboardi.orderdetail.viewholder

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.common.extensions.vibrate
import com.gaboardi.commonui.extensions.getColorAttribute
import com.gaboardi.commonui.extensions.setGone
import com.gaboardi.commonui.extensions.setVisible
import com.gaboardi.commonui.inputflilter.PlateCodeInputFilter
import com.gaboardi.orderdetail.R
import com.gaboardi.orderdetail.callback.PlatesHandler
import com.gaboardi.orderdetail.databinding.OrderedPlateItemBinding
import com.gaboardi.orderdetail.interfaces.PlateDiffViewHolder
import com.gaboardi.plate.model.ui.PlateDiff
import com.gaboardi.plate.model.ui.PlateUI

class OrderedPlateViewHolder(val binding: OrderedPlateItemBinding) :
    RecyclerView.ViewHolder(binding.root), PlateDiffViewHolder {

    fun bind(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        binding.amountSeekbar.steps = plate.quantity
        binding.amountSeekbar.value = plate.received
        binding.amountSeekbar.setOnValueChangedListener { old, new ->
            binding.amountSeekbar.context.vibrate()
            platesHandler.onReceivedSet(plate.id, new)
        }

        binding.plusButton.setOnClickListener {
            binding.amountSeekbar.context.vibrate()
            binding.amountSeekbar.value = binding.amountSeekbar.value + 1
            platesHandler.onReceivedSet(plate.id, binding.amountSeekbar.value)
        }

        binding.minusButton.setOnClickListener {
            binding.amountSeekbar.context.vibrate()
            binding.amountSeekbar.value = binding.amountSeekbar.value - 1
            platesHandler.onReceivedSet(plate.id, binding.amountSeekbar.value)
        }
        binding.plateName.text = plate.name
        showCode(plate)
    }

    override fun bindForChanges(
        diff: PlateDiff,
        plate: PlateUI.Plate,
        platesHandler: PlatesHandler
    ) {

    }

    private fun showCode(plate: PlateUI.Plate) {
        if (plate.code.isNotEmpty()) {
            binding.codeFlow.setVisible()
            binding.codeDivider.setVisible()
            binding.plateCode.text = colorCodeText(
                plate.code,
                binding.root.context.getColorAttribute(R.attr.colorPrimary)
            )
        } else {
            binding.codeFlow.setGone()
            binding.codeDivider.setGone()
        }
    }

    private fun colorCodeText(code: String, letterColor: Int): CharSequence {
        val shouldSpan = code.any { it.isLetter() }
        val result = if (shouldSpan) {
            var editable = code
            val sections: MutableList<PlateCodeInputFilter.LetterSection> = mutableListOf()
            while (editable.any { it.isLetter() }) {
                val sectionStart = editable.indexOfFirst { it.isLetter() }
                val section = editable.substring(sectionStart, editable.length)
                val sectionEnd = if (section.any { !it.isLetter() })
                    section.indexOfFirst { !it.isLetter() }
                else
                    editable.length
                sections.add(PlateCodeInputFilter.LetterSection(sectionStart, sectionEnd))
                editable = editable.removeRange(sectionStart, sectionEnd)
            }
            val spannable = SpannableStringBuilder(code)
            sections.forEach {
                spannable.setSpan(
                    ForegroundColorSpan(letterColor),
                    it.start,
                    it.end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            spannable
        } else code
        return result
    }
}