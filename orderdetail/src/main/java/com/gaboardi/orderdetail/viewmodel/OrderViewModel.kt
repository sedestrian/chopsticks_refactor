package com.gaboardi.orderdetail.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gaboardi.chopsticks.order.usecase.*
import com.gaboardi.common.model.ErrorMessage
import com.gaboardi.common.model.Event
import com.gaboardi.common.model.State
import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.plate.usecase.AddQuantityUseCase
import com.gaboardi.plate.usecase.SetQuantityUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OrderViewModel @Inject constructor(
    private val addQuantityUseCase: AddQuantityUseCase,
    private val subtractQuantityUseCase: SubtractQuantityUseCase,
    private val addReceivedUseCase: AddReceivedUseCase,
    private val subtractReceivedUseCase: SubtractReceivedUseCase,
    private val updatePlateStateUseCase: UpdatePlateStateUseCase,
    private val deletePlateUseCase: DeletePlateUseCase,
    private val setQuantityUseCase: SetQuantityUseCase
) : ViewModel() {
    private var orderId: String? = null

    private val _plates: MutableLiveData<List<PlateUI>> = MutableLiveData()
    val plates: LiveData<List<PlateUI>> = _plates

    private val _loading: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val loading: LiveData<Event<Boolean>> = _loading

    private val _error: MutableLiveData<Event<ErrorMessage>> = MutableLiveData()
    val error: LiveData<Event<ErrorMessage>> = _error

    private fun observeOrder() {
        /*orderId?.let { id ->
            viewModelScope.launch {
                observeOrderUseCase.observe(id).collect {
                    _order.postValue(it)
                    _plates.postValue(it.plates)
                }
            }
        }*/
    }

    fun deletePlate(plate: PlateUI.Plate) {
        viewModelScope.launch {
            deletePlateUseCase.delete(plate.id).collect { state ->
                when (state) {
                    is State.Loading -> {
                    }
                    is State.Success -> {
                    }
                    is State.Error -> {
                    }
                }
            }
        }
    }

    fun addPlateQuantity(id: String) {
        viewModelScope.launch {
            addQuantityUseCase.execute(id)
        }
    }

    fun addPlateReceived(id: String) {
        viewModelScope.launch {
            addReceivedUseCase.execute(id)
        }
    }

    fun removePlateQuantity(id: String) {
        viewModelScope.launch {
            subtractQuantityUseCase.execute(id)
        }
    }

    fun removePlateReceived(id: String) {
        viewModelScope.launch {
            subtractReceivedUseCase.execute(id)
        }
    }

    fun setPlateReceived(id: String, amount: Int) {
        viewModelScope.launch {
//            setReceivedUseCase.execute(id, amount)
        }
    }

    fun setPlateQuantity(id: String, amount: Int) {
        viewModelScope.launch {
            setQuantityUseCase.execute(id, amount)
        }
    }

    fun changePlateGroup(plate: PlateUI.Plate, newState: PlateState) {
        viewModelScope.launch {
            updatePlateStateUseCase.setState(plate.id, newState)
        }
    }

    fun setOrderId(orderId: String) {
        this.orderId = orderId
        observeOrder()
    }

    fun getOrderId(): String? = orderId
}