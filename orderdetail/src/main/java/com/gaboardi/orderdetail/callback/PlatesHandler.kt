package com.gaboardi.orderdetail.callback

import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.model.ui.PlateUI

interface PlatesHandler {
    fun onAddQuantity(id: String)
    fun onAddReceived(id: String)
    fun onRemoveQuantity(id: String)
    fun onRemoveReceived(id: String)
    fun onQuantitySet(id: String, amount: Int)
    fun onReceivedSet(id: String, amount: Int)
    fun changeGroup(plate: PlateUI.Plate, state: PlateState)
    fun onLongClick(plate: PlateUI.Plate)
}