package com.gaboardi.orderdetail.interfaces

import com.gaboardi.orderdetail.callback.PlatesHandler
import com.gaboardi.plate.model.ui.PlateDiff
import com.gaboardi.plate.model.ui.PlateUI

interface PlateDiffViewHolder {
    fun bindForChanges(diff: PlateDiff, plate: PlateUI.Plate, platesHandler: PlatesHandler)
}