package com.gaboardi.orderdetail.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gaboardi.common.extensions.dp
import com.gaboardi.commonui.utility.SpacingItemDecoration
import com.gaboardi.orderdetail.R
import com.gaboardi.orderdetail.adapter.PlatesAdapter
import com.gaboardi.orderdetail.callback.PlatesHandler
import com.gaboardi.orderdetail.databinding.FragmentOrderBinding
import com.gaboardi.orderdetail.dialog.PlateOptionsDialog
import com.gaboardi.orderdetail.viewmodel.OrderViewModel
import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.model.ui.PlateUI
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
class OrderFragment : Fragment() {
    private val viewModel: OrderViewModel by viewModels()
    private lateinit var binding: FragmentOrderBinding
    private lateinit var adapter: PlatesAdapter

    @ExperimentalCoroutinesApi
    val plateHandler: PlatesHandler = object : PlatesHandler {
        override fun onAddQuantity(id: String) {
            viewModel.addPlateQuantity(id)
        }

        override fun onAddReceived(id: String) {
            viewModel.addPlateReceived(id)
        }

        override fun onRemoveQuantity(id: String) {
            viewModel.removePlateQuantity(id)
        }

        override fun onRemoveReceived(id: String) {
            viewModel.removePlateReceived(id)
        }

        override fun onQuantitySet(id: String, amount: Int) {
            viewModel.setPlateQuantity(id, amount)
        }

        override fun onReceivedSet(id: String, amount: Int) {
            viewModel.setPlateReceived(id, amount)
        }

        override fun changeGroup(plate: PlateUI.Plate, state: PlateState) {
            viewModel.changePlateGroup(plate, state)
        }

        override fun onLongClick(plate: PlateUI.Plate) {
            val dialog = PlateOptionsDialog()
            dialog.setOnDeleteListener {
                viewModel.deletePlate(plate)
            }
            dialog.show(childFragmentManager, "")
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOrderBinding.inflate(inflater, container, false)
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        binding.root.doOnLayout {
            binding.bottomAppBar.performHide()
        }

        setupToolbar()
        setupPlatesRecycler()
        observe()

        arguments?.let {
            val args = OrderFragmentArgs.fromBundle(it)
            viewModel.setOrderId(args.orderId)
        }

        listen()
    }

    @ExperimentalCoroutinesApi
    private fun setupPlatesRecycler() {
        adapter = PlatesAdapter(plateHandler)
        binding.platesRecycler.layoutManager = LinearLayoutManager(requireContext())
        binding.platesRecycler.addItemDecoration(SpacingItemDecoration(0.dp, 1.dp))
        binding.platesRecycler.adapter = adapter
    }

    private fun setupToolbar() {
        binding.toolbar.inflateMenu(R.menu.order_options_menu)
        binding.toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.recap -> {
                }
                R.id.qr_code -> {
                }
            }
            true
        }
    }

    private fun observe() {
        viewModel.loading.observe(viewLifecycleOwner) {

        }

        viewModel.plates.observe(viewLifecycleOwner) {
            adapter.setItems(it)
        }

        /*viewModel.order.observe(viewLifecycleOwner) {
            binding.toolbar.title = it.name
            binding.toolbar.subtitle = it.date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
        }*/

        viewModel.error.observe(viewLifecycleOwner) {

        }
    }

    private fun listen() {
        binding.addPlateButton.setOnClickListener {
            viewModel.getOrderId()?.let { id ->
                val action = OrderFragmentDirections.actionOrderFragmentToPlateCreationFlow(id)
                findNavController().navigate(action)
            }
        }

        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val window = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        view.updatePadding(top = window.top)
        binding.addPlateButton.doOnLayout {
            binding.platesRecycler.updatePadding(bottom = window.bottom + it.height + 16.dp)
        }
        insets
    }
}