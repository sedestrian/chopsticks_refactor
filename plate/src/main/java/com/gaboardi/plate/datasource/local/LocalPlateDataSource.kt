package com.gaboardi.plate.datasource.local

import com.gaboardi.plate.model.db.PlateEntity
import com.gaboardi.plate.model.ui.PlateSimple

interface LocalPlateDataSource {
    suspend fun insertPlates(plates: List<PlateEntity>): List<Long>
    suspend fun getPlateSuggestions(query: String): List<PlateSimple>
}