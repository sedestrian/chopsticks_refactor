package com.gaboardi.plate.datasource.local

import com.gaboardi.plate.converter.toUi
import com.gaboardi.plate.dao.PlateDao
import com.gaboardi.plate.model.db.PlateEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LocalPlateDataSourceImpl @Inject constructor(
    private val dao: PlateDao
) : LocalPlateDataSource {
    override suspend fun insertPlates(plates: List<PlateEntity>) =
        withContext(Dispatchers.IO) {
            dao.insertPlates(plates)
        }

    override suspend fun getPlateSuggestions(query: String) = withContext(Dispatchers.IO) {
        dao.getPlateSuggestions(query).map { it.toUi() }
    }
}