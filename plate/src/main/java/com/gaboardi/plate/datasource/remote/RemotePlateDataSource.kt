package com.gaboardi.plate.datasource.remote

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.plate.model.api.PlateResult
import kotlinx.coroutines.flow.Flow

interface RemotePlateDataSource {
    fun subscribeToFirestore(): Flow<ApiResponse<List<PlateResult>>>
    suspend fun addPlate(
        orderId: String,
        name: String,
        code: String,
        quantity: Int,
        state: Int
    ): ApiResponse<String>

    suspend fun deletePlate(plateId: String): ApiResponse<Unit>
    suspend fun addQuantityToPlate(id: String): ApiResponse<Unit>
    suspend fun subtractQuantityFromPlate(id: String): ApiResponse<Unit>
    suspend fun addReceivedToPlate(id: String): ApiResponse<Unit>
    suspend fun subtractReceivedFromPlate(id: String): ApiResponse<Unit>
    suspend fun setPlateState(plateId: String, state: Int): ApiResponse<Unit>
    suspend fun setPlateQuantity(plateId: String, amount: Int): ApiResponse<Unit>
//    suspend fun setPlateReceived(plateId: String, amount: Int): ApiResponse<Unit>
    fun startPlatesSubscription(orders: List<String>): Flow<ApiResponse<List<PlateResult>>>
}