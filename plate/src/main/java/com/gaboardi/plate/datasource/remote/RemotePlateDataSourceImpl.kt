package com.gaboardi.plate.datasource.remote

import com.gaboardi.common.extensions.fromJson
import com.gaboardi.common.extensions.toJson
import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.Collections
import com.gaboardi.common.model.DatabaseField
import com.gaboardi.plate.model.api.PlateResult
import com.gaboardi.plate.model.ui.PlateState
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RemotePlateDataSourceImpl @Inject constructor(
    private val firestore: FirebaseFirestore,
    private val firebaseUser: FirebaseUser?
) : RemotePlateDataSource {
    override suspend fun addPlate(
        orderId: String,
        name: String,
        code: String,
        quantity: Int,
        state: Int
    ): ApiResponse<String> {
        return try {
            firebaseUser?.let { user ->
                /*val plate = PlateRequest(orderId, user.uid, name, code, quantity, state)
                val platesCollection = firestore.collection(Collections.Plates)
                val result = platesCollection.add(plate).await()*/
                ApiResponse.success(/*result.id*/"")
            } ?: ApiResponse.error("User credentials missing")
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage ?: "Api error")
        }
    }

    override suspend fun deletePlate(plateId: String): ApiResponse<Unit> {
        return try {
            val platesCollection = firestore.collection(Collections.Plates)
            platesCollection.document(plateId).delete().await()
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    override suspend fun addQuantityToPlate(id: String): ApiResponse<Unit> {
        return try {
            val platesCollection = firestore.collection(Collections.Plates)
            val plateDocument = platesCollection.document(id)
            plateDocument.update(DatabaseField.Plate.Quantity, FieldValue.increment(1)).await()
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    override suspend fun subtractQuantityFromPlate(id: String): ApiResponse<Unit> {
        return try {
            val platesCollection = firestore.collection(Collections.Plates)
            val plateDocument = platesCollection.document(id)
            plateDocument.update(DatabaseField.Plate.Quantity, FieldValue.increment(-1)).await()
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    override suspend fun addReceivedToPlate(id: String): ApiResponse<Unit> {
        return try {
            val platesCollection = firestore.collection(Collections.Plates)
            val plateDocument = platesCollection.document(id)

            plateDocument.update(DatabaseField.Plate.Received, FieldValue.increment(1)).await()

            val plateData = plateDocument.get().await()
            val received = plateData.getLong(DatabaseField.Plate.Received)
            val quantity = plateData.getLong(DatabaseField.Plate.Quantity)
            if (received != null && quantity != null && received >= quantity) {
                plateDocument.update(
                    DatabaseField.Plate.State,
                    PlateState.RECEIVED.state
                )
            }
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    override suspend fun subtractReceivedFromPlate(id: String): ApiResponse<Unit> {
        return try {
            val platesCollection = firestore.collection(Collections.Plates)
            val plateDocument = platesCollection.document(id)

            plateDocument.update(DatabaseField.Plate.Received, FieldValue.increment(-1))
            val plateData = plateDocument.get().await()
            val received = plateData.getLong(DatabaseField.Plate.Received)
            val quantity = plateData.getLong(DatabaseField.Plate.Quantity)
            if (received != null && quantity != null && received < quantity) {
                plateDocument.update(
                    DatabaseField.Plate.State,
                    PlateState.ORDERED.state
                )
            }
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    override suspend fun setPlateState(plateId: String, state: Int): ApiResponse<Unit> {
        return try {
            val platesCollection = firestore.collection(Collections.Plates)
            val plateDocument = platesCollection.document(plateId)

            val batch = firestore.batch()

            /*val old = plateDocument.get().await().data.toJson().fromJson<PlateResult>()
            val currentState = PlateState.fromState(old.state)
            val newState = PlateState.fromState(state)*/

            /*batch.update(plateDocument, DatabaseField.Plate.State, state)
            if (currentState == PlateState.LATER && newState == PlateState.ORDERED) {
                batch.update(plateDocument, DatabaseField.Plate.Received, 0)
            }*/

            batch.commit().await()

            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    override suspend fun setPlateQuantity(plateId: String, amount: Int): ApiResponse<Unit> {
        return try {
            val platesCollection = firestore.collection(Collections.Plates)
            val plateDocument = platesCollection.document(plateId)
            plateDocument.update(DatabaseField.Plate.Quantity, amount).await()
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    /*override suspend fun setPlateReceived(tableId: String, , plateId: String, amount: Int): ApiResponse<Unit> {
        return try {
            val platesCollection = firestore.collection(Collections.)
            val plateDocument = platesCollection.document(plateId)
            plateDocument.update(DatabaseField.Plate.Received, amount).await()
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }*/

    @ExperimentalCoroutinesApi
    override fun subscribeToFirestore() = callbackFlow<ApiResponse<List<PlateResult>>> {
        firebaseUser?.let { user ->
            val subscription = firestore.collection(Collections.Plates)
                .whereEqualTo(DatabaseField.Plate.UserId, user.uid)
                .addSnapshotListener { value, error ->
                    if (error != null) trySendBlocking(ApiResponse.error(error.localizedMessage))
                    else {
                        val data = value?.documents?.map {
                            val plate = it.data.toJson().fromJson<PlateResult>()
                            plate.copy(id = it.id)
                        }
                        trySendBlocking(ApiResponse.success(data ?: listOf()))
                    }
                }
            awaitClose { subscription.remove() }
        }
    }

    override fun startPlatesSubscription(orders: List<String>) =
        callbackFlow<ApiResponse<List<PlateResult>>> {
            firebaseUser?.let { user ->
                val subscription = firestore.collection(Collections.Plates)
                    .whereIn(DatabaseField.Plate.OrderId, orders)
                    .addSnapshotListener { value, error ->
                        if (error != null) trySendBlocking(ApiResponse.error(error.localizedMessage))
                        else {
                            val data = value?.documents?.map {
                                val plate = it.data.toJson().fromJson<PlateResult>()
                                plate.copy(id = it.id)
                            }
                            trySendBlocking(ApiResponse.success(data ?: listOf()))
                        }
                    }
                awaitClose { subscription.remove() }
            }
        }
}