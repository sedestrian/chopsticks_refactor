package com.gaboardi.plate.model.api

data class PlateResult(
    val id: String,
    val name: String,
    val code: String?
)