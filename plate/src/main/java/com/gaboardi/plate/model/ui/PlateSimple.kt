package com.gaboardi.plate.model.ui

data class PlateSimple(
    val id: String,
    val tableId: String,
    val name: String,
    val code: String?
)