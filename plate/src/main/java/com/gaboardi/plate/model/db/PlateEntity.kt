package com.gaboardi.plate.model.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "plates")
data class PlateEntity(
    @PrimaryKey
    val id: String,
    @ColumnInfo(index = true)
    val tableId: String,
    val name: String,
    val code: String?
)