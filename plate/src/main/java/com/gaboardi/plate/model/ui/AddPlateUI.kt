package com.gaboardi.plate.model.ui

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.gaboardi.plate.BR

class AddPlateUI : BaseObservable() {
    @get:Bindable
    var name: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.name)
            notifyPropertyChanged(BR.canCreate)
        }

    @get:Bindable
    var code: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.code)
        }

    @get:Bindable
    var quantity: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.quantity)
            notifyPropertyChanged(BR.canCreate)
        }

    @get:Bindable
    var received: Int = 0
        get() = receivedString.toInt()
        set(value) {
            field = value
            notifyPropertyChanged(BR.received)
            notifyPropertyChanged(BR.receivedString)
        }

    @get:Bindable
    var receivedString: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.received)
            notifyPropertyChanged(BR.receivedString)
        }


    @get:Bindable
    var state: Int = PlateState.ORDERED.state
        set(value) {
            field = value
            notifyPropertyChanged(BR.state)
            notifyPropertyChanged(BR.ordered)
            notifyPropertyChanged(BR.later)
        }

    @get:Bindable
    var ordered: Boolean
        get() = state == PlateState.ORDERED.state
        set(value) {
            if (value) {
                state = PlateState.ORDERED.state
            }
            notifyPropertyChanged(BR.state)
            notifyPropertyChanged(BR.ordered)
            notifyPropertyChanged(BR.later)
        }

    @get:Bindable
    var later: Boolean
        get() = state == PlateState.LATER.state
        set(value) {
            if (value) {
                state = PlateState.LATER.state
            }
            notifyPropertyChanged(BR.state)
            notifyPropertyChanged(BR.ordered)
            notifyPropertyChanged(BR.later)
        }

    @get:Bindable
    val canCreate: Boolean
        get() {
            return name.isNotEmpty() &&
                    quantity > 0
        }
}