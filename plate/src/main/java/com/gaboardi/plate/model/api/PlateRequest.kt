package com.gaboardi.plate.model.api

/**
 * A class used to send requests to add new plates
 * Parameters:
 * @param[orderId] Parent order's Id
 * @param[name] The name of the plate
 * @param[code] The code of the plate
 * @param[quantity] The amount of portions
 * @param[state] The state of the plate ("Ordered" | "For later")
 */
data class PlateRequest(
    val tableId: String,
    val name: String,
    val code: String?
)