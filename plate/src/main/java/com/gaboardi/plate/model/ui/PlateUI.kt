package com.gaboardi.plate.model.ui

import androidx.annotation.StringRes

sealed class PlateUI{
    data class Plate(
        val id: String,
        val tableId: String,
        val name: String,
        val code: String,
        val quantity: Int,
        val received: Int,
        var state: Int,
        val tableQuantity: Int? = null,
        val tableReceived: Int? = null
    ): PlateUI()

    data class Header(
        @StringRes val title: Int,
        val type: PlateState
    ): PlateUI()

    data class Empty(val type: EmptyPlateType): PlateUI()
}

enum class PlateState(val viewType: Int, val state: Int){
    ORDERED(0, 0),
    LATER(1, 1),
    RECEIVED(2, 2),
    HEADER(3, 0),
    EMPTY(4, 0);

    companion object{
        fun fromViewType(id: Int) = values().first { it.viewType == id }
        fun fromState(id: Int) = values().first { it.state == id }
    }
}

enum class EmptyPlateType(){
    ORDERED,
    LATER;
}