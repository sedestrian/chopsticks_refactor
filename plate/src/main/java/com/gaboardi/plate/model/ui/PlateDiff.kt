package com.gaboardi.plate.model.ui

data class PlateDiff(
    val name: String?,
    val code: String?,
    val quantity: Int?,
    val received: Int?,
    var state: Int?,
    val tableQuantity: Int?,
    val tableReceived: Int?
){
    fun isEmpty(): Boolean =
        name == null &&
                code == null &&
                quantity == null &&
                received == null &&
                state == null &&
                tableQuantity == null &&
                tableReceived == null
}