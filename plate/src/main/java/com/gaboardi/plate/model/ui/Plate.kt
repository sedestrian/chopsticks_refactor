package com.gaboardi.plate.model.ui

data class Plate(
    val id: String,
    val tableId: String,
    val tableUserId: String,
    val name: String,
    val code: String,
    val quantity: Int,
    val received: Int,
    val state: Int
)