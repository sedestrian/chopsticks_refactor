package com.gaboardi.plate.model.db

import com.gaboardi.plate.model.api.PlateResult

data class PlateInsert(
    val plates: List<PlateResult>,
    val tableId: String
)