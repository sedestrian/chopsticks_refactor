package com.gaboardi.plate.repository

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.State
import com.gaboardi.plate.converter.toEntity
import com.gaboardi.plate.datasource.local.LocalPlateDataSource
import com.gaboardi.plate.datasource.remote.RemotePlateDataSource
import com.gaboardi.plate.model.db.PlateInsert
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class PlateRepositoryImpl @Inject constructor(
    private val remotePlateDataSource: RemotePlateDataSource,
    private val localPlateDataSource: LocalPlateDataSource
) : PlateRepository {
    override suspend fun savePlates(plates: List<PlateInsert>) {
        val data = plates.flatMap { data -> data.plates.map { it.toEntity(data.tableId) } }
        localPlateDataSource.insertPlates(data)
    }

    override fun addPlate(
        orderId: String,
        name: String,
        code: String,
        quantity: Int,
        state: Int
    ) = flow {
        emit(State.loading())
        val result = remotePlateDataSource.addPlate(orderId, name, code, quantity, state)
        when (result) {
            is ApiResponse.Success -> emit(State.success(result.body))
            is ApiResponse.Error -> emit(State.error(result.message))
        }
    }

    override fun deletePlate(plateId: String) = flow {
        emit(State.loading())
        val result = remotePlateDataSource.deletePlate(plateId)
        when (result) {
            is ApiResponse.Success -> emit(State.success())
            is ApiResponse.Error -> emit(State.error(result.message))
        }
    }

    override suspend fun getSuggestions(query: String) =
        localPlateDataSource.getPlateSuggestions(query)

    override suspend fun addPlateQuantity(id: String) {
        remotePlateDataSource.addQuantityToPlate(id)
    }

    override suspend fun subtractPlateQuantity(id: String) {
        remotePlateDataSource.subtractQuantityFromPlate(id)
    }

    override suspend fun addPlateReceived(id: String) {
        remotePlateDataSource.addReceivedToPlate(id)
    }

    override suspend fun subtractPlateReceived(id: String) {
        remotePlateDataSource.subtractReceivedFromPlate(id)
    }


    override suspend fun setPlateState(plateId: String, state: Int) {
        remotePlateDataSource.setPlateState(plateId, state)
    }

    override suspend fun setPlateQuantity(plateId: String, amount: Int) {
        remotePlateDataSource.setPlateQuantity(plateId, amount)
    }

    /*override suspend fun setPlateReceived(plateId: String, amount: Int) {
        remotePlateDataSource.setPlateReceived(plateId, amount)
    }*/
}