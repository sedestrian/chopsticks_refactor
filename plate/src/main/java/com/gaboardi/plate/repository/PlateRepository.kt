package com.gaboardi.plate.repository

import com.gaboardi.common.model.State
import com.gaboardi.plate.model.db.PlateInsert
import com.gaboardi.plate.model.ui.PlateSimple
import kotlinx.coroutines.flow.Flow

interface PlateRepository {
    suspend fun addPlateQuantity(id: String)
    suspend fun subtractPlateQuantity(id: String)
    suspend fun addPlateReceived(id: String)
    suspend fun subtractPlateReceived(id: String)
    suspend fun setPlateState(plateId: String, state: Int)
    suspend fun setPlateQuantity(plateId: String, amount: Int)
//    suspend fun setPlateReceived(plateId: String, amount: Int)

    fun addPlate(
        orderId: String,
        name: String,
        code: String,
        quantity: Int,
        state: Int
    ): Flow<State>
    suspend fun savePlates(plates: List<PlateInsert>)

    fun deletePlate(plateId: String): Flow<State>
    suspend fun getSuggestions(query: String): List<PlateSimple>
}