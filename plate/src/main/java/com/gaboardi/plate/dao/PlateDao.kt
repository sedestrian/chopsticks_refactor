package com.gaboardi.plate.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gaboardi.plate.model.db.PlateEntity

@Dao
abstract class PlateDao {
    @Query("SELECT * FROM plates WHERE name LIKE '%' || :text || '%' AND name != :text GROUP BY name ORDER BY COUNT(name) DESC LIMIT 3")
    abstract suspend fun getPlateSuggestions(text: String): List<PlateEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertPlates(plates: List<PlateEntity>): List<Long>

    @Query("DELETE FROM plates WHERE id NOT IN (:plates)")
    abstract suspend fun cleanPlates(plates: List<String>)
}