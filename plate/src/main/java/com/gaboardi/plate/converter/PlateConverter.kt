package com.gaboardi.plate.converter

import com.gaboardi.plate.model.api.PlateResult
import com.gaboardi.plate.model.db.PlateEntity
import com.gaboardi.plate.model.ui.Plate
import com.gaboardi.plate.model.ui.PlateSimple
import com.gaboardi.plate.model.ui.PlateUI

fun Plate.toPlateUi(): PlateUI.Plate{
    return PlateUI.Plate(id, "", name, code, 0, 0, 0)
}

fun PlateEntity.toUi(): PlateSimple {
    return PlateSimple(
        id,
        tableId,
        name,
        code
    )
}

fun PlateResult.toEntity(tableId: String): PlateEntity {
    return PlateEntity(
        id,
        tableId,
        name,
        code
    )
}