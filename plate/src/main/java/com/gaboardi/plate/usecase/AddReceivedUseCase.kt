package com.gaboardi.chopsticks.order.usecase

import com.gaboardi.plate.repository.PlateRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class AddReceivedUseCase @Inject constructor(
    private val platesRepository: PlateRepository
) {
    @ExperimentalCoroutinesApi
    suspend fun execute(id: String) {
        platesRepository.addPlateReceived(id)
    }
}