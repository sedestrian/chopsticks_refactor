package com.gaboardi.chopsticks.addplate.usecase

import com.gaboardi.plate.repository.PlateRepository
import javax.inject.Inject

class GetPlateSuggestionsUseCase @Inject constructor(
    private val platesRepository: PlateRepository
) {
    suspend fun request(query: String) = platesRepository.getSuggestions(query)
}