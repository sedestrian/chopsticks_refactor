package com.gaboardi.plate.usecase

import com.gaboardi.plate.repository.PlateRepository
import javax.inject.Inject

class AddQuantityUseCase @Inject constructor(
    private val platesRepository: PlateRepository
) {
    suspend fun execute(id: String) {
        platesRepository.addPlateQuantity(id)
    }
}