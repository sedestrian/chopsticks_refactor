package com.gaboardi.chopsticks.order.usecase

import com.gaboardi.common.model.State
import com.gaboardi.plate.repository.PlateRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DeletePlateUseCase @Inject constructor(
    private val platesRepository: PlateRepository
) {
    suspend fun delete(plateId: String): Flow<State> {
        return platesRepository.deletePlate(plateId)
    }
}