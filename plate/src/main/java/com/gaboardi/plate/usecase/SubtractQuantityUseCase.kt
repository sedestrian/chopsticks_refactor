package com.gaboardi.chopsticks.order.usecase

import com.gaboardi.plate.repository.PlateRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class SubtractQuantityUseCase @Inject constructor(
    private val platesRepository: PlateRepository
) {
    @ExperimentalCoroutinesApi
    suspend fun execute(id: String) {
        platesRepository.subtractPlateQuantity(id)
    }
}