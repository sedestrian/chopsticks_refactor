package com.gaboardi.chopsticks.order.usecase

import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.repository.PlateRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class UpdatePlateStateUseCase @Inject constructor(
    private val platesRepository: PlateRepository
) {
    @ExperimentalCoroutinesApi
    suspend fun setState(plateId: String, plateState: PlateState) {
        platesRepository.setPlateState(plateId, plateState.state)
    }
}