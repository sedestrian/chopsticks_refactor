package com.gaboardi.plate.usecase

import com.gaboardi.plate.repository.PlateRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class SetQuantityUseCase @Inject constructor(
    private val platesRepository: PlateRepository
) {
    @ExperimentalCoroutinesApi
    suspend fun execute(id: String, amount: Int) {
        platesRepository.setPlateQuantity(id, amount)
    }
}