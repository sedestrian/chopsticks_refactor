package com.gaboardi.chopsticks.addplate.usecase

import com.gaboardi.plate.repository.PlateRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class AddPlateUseCase @Inject constructor(
    private val platesRepository: PlateRepository
) {
    @ExperimentalCoroutinesApi
    suspend fun addPlate(
        orderId: String,
        name: String,
        code: String,
        quantity: Int,
        state: Int
    ) = platesRepository.addPlate(orderId, name, code, quantity, state)
}