package com.gaboardi.platecreation.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.plate.model.ui.PlateSimple
import com.gaboardi.platecreation.databinding.SuggestionItemBinding

class SuggestionsViewHolder(val binding: SuggestionItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(suggestion: PlateSimple, click: ((suggestion: PlateSimple) -> Unit)?) {
        binding.suggestion.text = suggestion.name
        binding.interaction.setOnClickListener {
            click?.invoke(suggestion)
        }
    }
}