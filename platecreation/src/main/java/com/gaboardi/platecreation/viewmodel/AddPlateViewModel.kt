package com.gaboardi.platecreation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gaboardi.chopsticks.addplate.usecase.AddPlateUseCase
import com.gaboardi.chopsticks.addplate.usecase.GetPlateSuggestionsUseCase
import com.gaboardi.common.model.State
import com.gaboardi.plate.model.ui.AddPlateUI
import com.gaboardi.plate.model.ui.PlateSimple
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddPlateViewModel @Inject constructor(
    private val addPlateUseCase: AddPlateUseCase,
    private val getPlateSuggestionsUseCase: GetPlateSuggestionsUseCase
) : ViewModel() {
    var orderId: String? = null
        private set

    val plate: MutableLiveData<AddPlateUI> = MutableLiveData(AddPlateUI())

    private val _suggestions: MutableLiveData<List<PlateSimple>> = MutableLiveData()
    val suggestions: LiveData<List<PlateSimple>> = _suggestions

    private val _created: MutableLiveData<String> = MutableLiveData()
    val created: LiveData<String> = _created

    private val _loading: MutableLiveData<Boolean> = MutableLiveData()
    val loading: LiveData<Boolean> = _loading

    private val _error: MutableLiveData<Exception> = MutableLiveData()
    val error: LiveData<Exception> = _error

    fun setOrderId(orderId: String) {
        this.orderId = orderId
    }

    fun setAmount(amount: Int) {
        val value = plate.value
        value?.quantity = amount
        plate.postValue(value)
    }

    fun getSuggestions(query: String) {
        viewModelScope.launch {
            val plates = getPlateSuggestionsUseCase.request(query)
            _suggestions.postValue(plates)
        }
    }

    @ExperimentalCoroutinesApi
    fun addPlate() {
        val plateData = plate.value
        val id = orderId

        if (plateData != null && id != null) {
            viewModelScope.launch {
                addPlateUseCase.addPlate(
                    id,
                    plateData.name,
                    plateData.code,
                    plateData.quantity,
                    plateData.state
                ).collect { state ->
                    when (state) {
                        is State.Loading -> _loading.postValue(true)
                        is State.Success -> {
                            _loading.postValue(false)
                            _created.postValue(state.id)
                        }
                        is State.Error -> _loading.postValue(false)
                    }
                }
            }
        }
    }
}