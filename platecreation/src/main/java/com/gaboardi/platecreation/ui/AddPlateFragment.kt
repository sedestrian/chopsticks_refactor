package com.gaboardi.platecreation.ui

import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getDrawable
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.*
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.gaboardi.common.extensions.vibrate
import com.gaboardi.commonui.extensions.setGone
import com.gaboardi.commonui.extensions.setVisible
import com.gaboardi.commonui.inputflilter.PlateCodeInputFilter
import com.gaboardi.platecreation.R
import com.gaboardi.platecreation.adapter.SuggestionsAdapter
import com.gaboardi.platecreation.databinding.FragmentAddPlateBinding
import com.gaboardi.platecreation.viewmodel.AddPlateViewModel
import com.shawnlin.numberpicker.NumberPicker
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
class AddPlateFragment : Fragment() {
    private val viewModel: AddPlateViewModel by viewModels()

    private lateinit var binding: FragmentAddPlateBinding
    private lateinit var adapter: SuggestionsAdapter

    private var isSet: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddPlateBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        arguments?.let {
            val args = AddPlateFragmentArgs.fromBundle(it)
            viewModel.setOrderId(args.orderId)
        }

        setupNumberPicker()
        setupSuggestions()
        setupCodeInput()
        observe()

        viewModel.setAmount(binding.numberPicker.value)
        viewModel.getSuggestions("")

        binding.buttonContainer.doOnLayout {
            binding.scrollView.updatePadding(bottom = it.height)
        }

        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        binding.savePlate.setOnClickListener {
            viewModel.addPlate()
        }

        binding.plateName.addTextChangedListener { text ->
            viewModel.getSuggestions(text.toString())
        }
    }

    private fun observe() {
        viewModel.created.observe(viewLifecycleOwner) {
            findNavController().navigateUp()
        }

        viewModel.suggestions.observe(viewLifecycleOwner) {
//            TransitionManager.beginDelayedTransition(binding.constraintLayout)
            if (it.isEmpty()) {
                binding.recentsCard.setGone()
            } else {
                binding.recentsCard.setVisible()
                adapter.setSuggestions(it)
            }
        }
    }

    private fun setupCodeInput() {
        val default = ContextCompat.getColor(requireContext(), R.color.red_500)
        val attributes =
            requireContext().theme.obtainStyledAttributes(intArrayOf(R.attr.colorPrimary))
        val color = attributes.getColor(0, default)
        attributes.recycle()
        binding.codeInput.filters =
            binding.codeInput.filters + InputFilter.AllCaps() + PlateCodeInputFilter(
                color
            )
    }

    private fun setupSuggestions() {
        adapter = SuggestionsAdapter()
        val layoutManager = LinearLayoutManager(requireContext())
        val dividerDrawable = getDrawable(requireContext(), R.drawable.divider)
        dividerDrawable?.let {
            val divider = DividerItemDecoration(requireContext(), layoutManager.orientation)
            binding.recentsRecycler.addItemDecoration(divider)
        }

        adapter.setOnSuggestionClick {
            binding.plateName.setText(it.name)
        }

        binding.recentsRecycler.layoutManager = layoutManager
        binding.recentsRecycler.adapter = adapter
    }

    private fun setupNumberPicker() {
        binding.numberPicker.setSelectedTypeface(
            ResourcesCompat.getFont(
                requireContext(),
                R.font.comfortaa_bold
            )
        )
        binding.numberPicker.typeface = ResourcesCompat.getFont(
            requireContext(),
            R.font.comfortaa_regular
        )

        binding.numberPicker.setOnValueChangedListener { numberPicker, oldValue, newValue ->
            viewModel.setAmount(newValue)
            if(newValue != oldValue){
                requireContext().vibrate()
            }
        }
    }

    private fun selectName() {
        isSet = true
    }

    private fun deselectName() {
        isSet = false
    }

    private fun showRest() {
        binding.recentsCard.setGone()
        binding.recentsRecycler.setGone()
    }

    private fun hideRest() {
        binding.recentsCard.setVisible()
        binding.recentsRecycler.setVisible()
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val windowOrIme =
            insets.getInsets(WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.ime())
        view.updatePadding(top = windowOrIme.top, bottom = windowOrIme.bottom)
        insets
    }
}