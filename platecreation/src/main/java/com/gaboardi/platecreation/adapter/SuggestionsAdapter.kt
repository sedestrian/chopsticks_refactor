package com.gaboardi.platecreation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.plate.model.ui.PlateSimple
import com.gaboardi.platecreation.databinding.SuggestionItemBinding
import com.gaboardi.platecreation.viewholder.SuggestionsViewHolder

class SuggestionsAdapter : RecyclerView.Adapter<SuggestionsViewHolder>() {
    private val items: MutableList<PlateSimple> = mutableListOf()
    private lateinit var inflater: LayoutInflater

    private var onSuggestionClick: ((suggestion: PlateSimple) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuggestionsViewHolder {
        if (!this::inflater.isInitialized) inflater = LayoutInflater.from(parent.context)
        val binding = SuggestionItemBinding.inflate(inflater, parent, false)
        return SuggestionsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SuggestionsViewHolder, position: Int) {
        holder.bind(items[position]) { suggestion ->
            onSuggestionClick?.invoke(suggestion)
        }
    }

    override fun getItemCount(): Int = items.size

    fun setOnSuggestionClick(listener: ((suggestion: PlateSimple) -> Unit)?) {
        onSuggestionClick = listener
    }

    fun setSuggestions(newSuggestions: List<PlateSimple>) {
        val util = SuggestionsDiffUtil(items, newSuggestions)
        val diff = DiffUtil.calculateDiff(util)
        diff.dispatchUpdatesTo(this)
        items.clear()
        items.addAll(newSuggestions)
    }
}