package com.gaboardi.platecreation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.gaboardi.plate.model.ui.PlateSimple

class SuggestionsDiffUtil(val oldItems: List<PlateSimple>, val newItems: List<PlateSimple>) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]

        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]

        return oldItem.name == newItem.name
    }
}