package com.gaboardi.login.contracts

import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import com.gaboardi.login.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await

class GoogleSignInContract : ActivityResultContract<Int?, Flow<GoogleSignInAccount>>() {
    override fun createIntent(context: Context, webClientId: Int?): Intent {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        val signInClient = GoogleSignIn.getClient(context, gso)
        return signInClient.signInIntent
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Flow<GoogleSignInAccount> {
        return flow { emit(GoogleSignIn.getSignedInAccountFromIntent(intent).await()) }
    }
}