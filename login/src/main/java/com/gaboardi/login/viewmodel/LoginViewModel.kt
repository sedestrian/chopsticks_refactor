package com.gaboardi.login.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.facebook.AccessToken
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.gaboardi.login.uistate.LoginViewState
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val auth: FirebaseAuth
) : ViewModel() {
    private val _loginViewState: MutableLiveData<LoginViewState> =
        MutableLiveData(LoginViewState.Landing)
    val loginViewState: LiveData<LoginViewState> = _loginViewState

    fun facebookResult(flow: Flow<LoginResult?>) {
        viewModelScope.launch {
            try {
                flow.collect { login ->
                    login?.let {
                        loginWithFacebook(it.accessToken)
                    } ?: _loginViewState.postValue(LoginViewState.LoginError)
                }
            } catch (e: FacebookException) {
                _loginViewState.postValue(LoginViewState.LoginError)
            }
        }
    }

    fun googleResult(flow: Flow<GoogleSignInAccount?>) {
        viewModelScope.launch {
            try {
                flow.collect { account ->
                    account?.let {
                        loginWithGoogle(it)
                    } ?: _loginViewState.postValue(LoginViewState.LoginError)
                }
            } catch (e: Exception) {
                _loginViewState.postValue(LoginViewState.LoginError)
            }
        }
    }

    private suspend fun loginWithFacebook(accessToken: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(accessToken.token)
        val authResult = firebaseAccess(credential)
        notifyLogin(authResult)
    }

    private suspend fun loginWithGoogle(account: GoogleSignInAccount) {
        account.idToken?.let { token ->
            val credential = GoogleAuthProvider.getCredential(token, null)
            val authResult = firebaseAccess(credential)
            notifyLogin(authResult)
        }
    }

    private suspend fun loginAnonimously() {
        val result = auth.signInAnonymously().await()
        notifyLogin(result)
    }

    fun loginClicked() {
        _loginViewState.postValue(LoginViewState.LoginChoice)
    }

    fun clickedGoogle() {
        _loginViewState.postValue(LoginViewState.LoggingIn)
    }

    fun clickedFacebook() {
        _loginViewState.postValue(LoginViewState.LoggingIn)
    }

    fun resetState() {
        _loginViewState.postValue(LoginViewState.Landing)
    }

    private fun notifyLogin(authResult: AuthResult) {
        val user = authResult.user
        if (user != null)
            _loginViewState.postValue(LoginViewState.LoggedIn(user))
        else
            _loginViewState.postValue(LoginViewState.LoginError)
    }

    private suspend fun firebaseAccess(credential: AuthCredential): AuthResult {
        return auth.signInWithCredential(credential).await()
    }

    fun anonimousLogin() {
        viewModelScope.launch {
            _loginViewState.postValue(LoginViewState.LoggingIn)
            loginAnonimously()
        }
    }
}