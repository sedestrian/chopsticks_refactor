package com.gaboardi.login.customview

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import androidx.fragment.app.Fragment
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.supervisorScope
import kotlinx.coroutines.suspendCancellableCoroutine

class FacebookLoginButton: MaterialButton {
    private val callbackManager = CallbackManager.Factory.create()

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    @ExperimentalCoroutinesApi
    fun login(fragment: Fragment, permissions: List<String> = listOf()): Flow<LoginResult?>{
        val flow = initFlow()
        LoginManager.getInstance().logInWithReadPermissions(fragment, permissions)
        return flow
    }

    @ExperimentalCoroutinesApi
    fun login(activity: Activity, permissions: List<String> = listOf()): Flow<LoginResult?>{
        val flow = initFlow()
        LoginManager.getInstance().logInWithReadPermissions(activity, permissions)
        return flow
    }

    @ExperimentalCoroutinesApi
    private fun initFlow(): Flow<LoginResult?>{
        return callbackFlow {
            val callback = object: FacebookCallback<LoginResult>{
                override fun onSuccess(result: LoginResult?) {
                    trySend(result)
                }

                override fun onCancel() {
                    close()
                }

                override fun onError(error: FacebookException?) {
                    close(error)
                }
            }
            LoginManager.getInstance()
                .registerCallback(callbackManager, callback)
            awaitClose { LoginManager.getInstance().unregisterCallback(callbackManager) }
        }
    }

    fun activityResult(requestCode: Int, resultCode: Int, data: Intent?){
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }
}