package com.gaboardi.login.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.OnApplyWindowInsetsListener
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.gaboardi.host.viewmodel.HostViewModel
import com.gaboardi.login.contracts.GoogleSignInContract
import com.gaboardi.login.databinding.FragmentLoginBinding
import com.gaboardi.login.uistate.LoginViewState
import com.gaboardi.login.viewmodel.LoginViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.card.MaterialCardView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
class LoginFragment: Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var bottomSheet: BottomSheetBehavior<MaterialCardView>

    private val googleSignInRequest = registerForActivityResult(GoogleSignInContract()) {
        viewModel.googleResult(it)
    }

    private val viewModel: LoginViewModel by viewModels()
    private val firebaseViewModel: HostViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        setupBottomSheet()
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        observe()
        listeners()
    }

    override fun onResume() {
        super.onResume()
        bottomSheet.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val window = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        view.updatePadding(top = window.top, bottom = window.bottom)
        binding.bottomSheetCard.setContentPadding(
            binding.bottomSheetCard.contentPaddingLeft,
            binding.bottomSheetCard.contentPaddingTop,
            binding.bottomSheetCard.contentPaddingRight,
            window.bottom
        )
        insets
    }

    private fun setupBottomSheet() {
        bottomSheet = BottomSheetBehavior.from(binding.bottomSheetCard)
        bottomSheet.isHideable = true
        bottomSheet.state = BottomSheetBehavior.STATE_HIDDEN
    }

    @ExperimentalCoroutinesApi
    private fun observe() {
        viewModel.loginViewState.observe(viewLifecycleOwner) { state ->
            when (state) {
                is LoginViewState.Landing -> {
                    bottomSheet.state = BottomSheetBehavior.STATE_HIDDEN
                }
                is LoginViewState.LoginChoice -> {
                    bottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
                }
                is LoginViewState.LoggingIn -> {
                    bottomSheet.state = BottomSheetBehavior.STATE_HIDDEN
                }
                is LoginViewState.LoggedIn -> {
                    firebaseViewModel.beginSubscription()

                    viewModel.resetState()

                    val action = LoginFragmentDirections.actionLoginFragmentToHomeFlow()
                    findNavController().navigate(action)
                }
                is LoginViewState.LoginError -> {
                    println("Error")
                    Snackbar.make(binding.coordinator, "Error", Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun listeners() {
        binding.googleLogin.setOnClickListener {
            viewModel.clickedGoogle()
            googleSignInRequest.launch(null)
        }

        binding.facebookLogin.setOnClickListener {
            viewModel.clickedFacebook()
            val flow =
                binding.facebookLogin.login(this@LoginFragment, listOf("public_profile", "email"))
            viewModel.facebookResult(flow)
        }

        binding.mailLogin.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragmentToMailLoginFragment()
            findNavController().navigate(action)
        }

        binding.loginButton.setOnClickListener {
            viewModel.loginClicked()
        }

        binding.laterButton.setOnClickListener {
            viewModel.anonimousLogin()
            /*val action = LoginFragmentDirections.actionLoginFragmentToHomeFragment()
            findNavController().navigate(action)*/
        }
    }
}