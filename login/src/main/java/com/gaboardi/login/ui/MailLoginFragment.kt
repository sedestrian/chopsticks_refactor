package com.gaboardi.login.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gaboardi.login.databinding.FragmentMailLoginBinding

class MailLoginFragment: Fragment() {
    private lateinit var binding: FragmentMailLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMailLoginBinding.inflate(inflater, container, false)
        return binding.root
    }
}