package com.gaboardi.login.uistate

import com.google.firebase.auth.FirebaseUser

sealed class LoginViewState{
    object Landing : LoginViewState()
    object LoginChoice : LoginViewState()
    object LoggingIn : LoginViewState()
    data class LoggedIn(val user: FirebaseUser): LoginViewState()
    object LoginError : LoginViewState()
}