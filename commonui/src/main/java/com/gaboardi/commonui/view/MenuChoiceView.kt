package com.gaboardi.commonui.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.gaboardi.commonui.R

class MenuChoiceView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {
    private var listeners: MutableList<((selected: Boolean) -> Unit)> = mutableListOf()
    private val root: ConstraintLayout
    private val icon: ImageView
    private val text: TextView
    private var textString: String? = null
    private var iconDrawable: Drawable? = null

    var isChecked: Boolean = false
        set(value) {
            field = value
            updateUi()
        }

    init {
        inflate(context, R.layout.menu_choice_view, this)
        root = findViewById(R.id.root)
        icon = findViewById(R.id.icon)
        text = findViewById(R.id.text)

        initAttrs(attrs)
        setupUi()
        setListener()
    }

    private fun setListener() {
        root.setOnClickListener {
            listeners.forEach { it.invoke(isChecked) }
            updateUi()
        }
    }

    fun addOnCheckedListener(listener: ((selected: Boolean) -> Unit)) {
        this.listeners.add(listener)
    }

    private fun uncheck() {
        root.setBackgroundResource(R.drawable.dash_line_red_8dp)
    }

    private fun check() {
        root.setBackgroundResource(R.drawable.line_red_8dp)
    }

    private fun initAttrs(attrs: AttributeSet?) {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.ChoiceView,
            0, 0
        ).apply {

            try {
                textString = getString(R.styleable.ChoiceView_text)
                iconDrawable = getDrawable(R.styleable.ChoiceView_src)
            } finally {
                recycle()
            }
        }
    }

    private fun setupUi() {
        icon.setImageDrawable(iconDrawable)
        text.text = textString
    }

    private fun updateUi() {
        if (isChecked) {
            check()
        } else {
            uncheck()
        }
    }
}

@BindingAdapter("menu_chosen")
fun bindChoice(view: MenuChoiceView, boolean: Boolean?) {
    if (boolean != null)
        view.isChecked = boolean
}

@InverseBindingAdapter(attribute = "menu_chosen")
fun onChosen(view: MenuChoiceView): Boolean {
    return !view.isChecked
}

@BindingAdapter("menu_chosenAttrChanged")
fun setListeners(
    view: MenuChoiceView,
    attrChange: InverseBindingListener
) {
    view.addOnCheckedListener {
        attrChange.onChange()
    }
}