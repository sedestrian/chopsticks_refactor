package com.gaboardi.commonui.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.gaboardi.common.extensions.dp
import com.gaboardi.commonui.R
import kotlin.math.roundToInt

class ChopstickProgress(context: Context, attrs: AttributeSet?) : View(context, attrs) {
    private var strokePadding = 0f
    private var diameter = 0.0
    private var stepWidth = 0.0
    private var font = ResourcesCompat.getFont(context, R.font.comfortaa_regular)

    private var onValueChanged: ((Int, Int) -> Unit)? = null

    var value: Int = 0
        set(value) {
            field = value
            invalidate()
        }

    var steps = 10
        set(value) {
            field = value
            invalidate()
        }

    var radius: Float = 8f.dp
        set(value) {
            field = value
            diameter = radius * 2.0
            invalidate()
        }

    var strokeWidth = 8f.dp
        set(value) {
            field = value
            thumbStrokePaint.strokeWidth = value
            trackStrokePaint.strokeWidth = value
            strokePadding = value / 2
            invalidate()
        }

    var thumbTextSize = 24f.dp
        set(value) {
            field = value
            thumbTextPaint.textSize = value
            invalidate()
        }

    var trackTextSize = 24f.dp
        set(value) {
            field = value
            trackTextPaint.textSize = value
            invalidate()
        }

    var trackTextColor = Color.BLUE
        set(value) {
            field = value
            trackTextPaint.color = value
            invalidate()
        }

    var thumbTextColor = Color.BLACK
        set(value) {
            field = value
            thumbTextPaint.color = value
            invalidate()
        }

    var thumbBorderColor = Color.GREEN
        set(value) {
            field = value
            thumbStrokePaint.color = value
            invalidate()
        }

    var thumbColor = Color.BLUE
        set(value) {
            field = value
            thumbPaint.color = value
            invalidate()
        }

    var trackBorderColor = Color.BLUE
        set(value) {
            field = value
            trackStrokePaint.color = value
            invalidate()
        }

    var trackColor = Color.CYAN
        set(value) {
            field = value
            trackPaint.color = value
            invalidate()
        }

    private val thumbPaint = Paint().apply {
        color = thumbColor
        isAntiAlias = true
        isDither = true
    }

    private val thumbStrokePaint = Paint().apply {
        color = thumbBorderColor
        strokeWidth = this@ChopstickProgress.strokeWidth
        style = Paint.Style.STROKE
        isAntiAlias = true
        isDither = true
    }

    private val thumbTextPaint = Paint().apply {
        color = thumbTextColor
        textAlign = Paint.Align.CENTER
        typeface = font
        textSize = thumbTextSize
        isAntiAlias = true
        isDither = true
    }

    private val trackPaint = Paint().apply {
        color = trackColor
        isAntiAlias = true
        isDither = true
    }

    private val trackStrokePaint = Paint().apply {
        color = trackBorderColor
        strokeWidth = this@ChopstickProgress.strokeWidth
        style = Paint.Style.STROKE
        isAntiAlias = true
        isDither = true
    }

    private val trackTextPaint = Paint().apply {
        color = trackTextColor
        textAlign = Paint.Align.CENTER
        typeface = font
        textSize = trackTextSize
        isAntiAlias = true
        isDither = true
    }

    fun setOnValueChangedListener(listener: ((oldValue: Int, newValue: Int) -> Unit)?) {
        onValueChanged = listener
    }

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.ChopstickProgress,
            0, 0
        ).apply {

            try {
                value = getInteger(R.styleable.ChopstickProgress_progressValue, 0)
                radius = getDimension(R.styleable.ChopstickProgress_progressRadius, 16f.dp)
                strokeWidth = getDimension(R.styleable.ChopstickProgress_progressBarStrokeWidth, 16f.dp)
                thumbBorderColor =
                    getColor(R.styleable.ChopstickProgress_progressThumbBorderColor, Color.BLACK)
                trackBorderColor =
                    getColor(R.styleable.ChopstickProgress_progressTrackBorderColor, Color.RED)
                thumbColor = getColor(R.styleable.ChopstickProgress_progressThumbColor, Color.RED)
                trackColor = getColor(R.styleable.ChopstickProgress_progressTrackColor, Color.WHITE)
            } finally {
                recycle()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(widthMeasureSpec, diameter.roundToInt())
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        stepWidth = (width.toDouble() - diameter) / steps.toDouble()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val stepPosition = value * stepWidth + radius

        canvas.drawRoundRect(
            0f,
            0f,
            width.toFloat(),
            height.toFloat(),
            radius,
            radius,
            trackPaint
        )
        canvas.drawRoundRect(
            strokePadding,
            strokePadding,
            width.toFloat() - strokePadding,
            height.toFloat() - strokePadding,
            radius,
            radius,
            trackStrokePaint
        )

        canvas.drawRoundRect(
            0f,
            0f,
            stepPosition.toFloat() + radius,
            height.toFloat(),
            radius,
            radius,
            thumbPaint
        )
        canvas.drawRoundRect(
            strokePadding,
            strokePadding,
            stepPosition.toFloat() + radius - strokePadding,
            height.toFloat() - strokePadding,
            radius,
            radius,
            thumbStrokePaint
        )
    }
}