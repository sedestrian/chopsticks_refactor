package com.gaboardi.commonui.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.gaboardi.commonui.R

class InlineChoiceView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {
    private var listeners: MutableList<((selected: Boolean) -> Unit)> = mutableListOf()
    private val root: ConstraintLayout
    private val icon: ImageView
    private val text: TextView
    private var textString: String? = null
    private var iconDrawable: Drawable? = null

    var isChecked: Boolean = false
        set(value) {
            field = value
            updateUi()
        }

    init {
        inflate(context, R.layout.inline_choice_view, this)
        root = findViewById(R.id.root)
        icon = findViewById(R.id.icon)
        text = findViewById(R.id.text)

        initAttrs(attrs)
        setupUi()
        setListener()
    }

    private fun setListener() {
        root.setOnClickListener {
            if (isEnabled) {
                listeners.forEach { it.invoke(!isChecked) }
            }
        }
    }

    fun addOnCheckedListener(listener: ((selected: Boolean) -> Unit)) {
        this.listeners.add(listener)
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        updateUi()
    }

    private fun uncheck() {
        root.setBackgroundResource(R.drawable.dash_line_red_8dp)
    }

    private fun check() {
        root.setBackgroundResource(R.drawable.line_red_8dp)
    }

    private fun disable() {
        root.setBackgroundResource(R.drawable.dash_line_disabled_8dp)
    }

    private fun initAttrs(attrs: AttributeSet?) {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.ChoiceView,
            0, 0
        ).apply {

            try {
                textString = getString(R.styleable.ChoiceView_text)
                iconDrawable = getDrawable(R.styleable.ChoiceView_src)
            } finally {
                recycle()
            }
        }
    }

    private fun setupUi() {
        updateUi()
        if (iconDrawable != null) {
            icon.visibility = View.VISIBLE
            icon.setImageDrawable(iconDrawable)
        } else {
            icon.visibility = View.GONE
        }
        text.text = textString
    }

    private fun updateUi() {
        if (isEnabled) {
            if (isChecked) {
                check()
            } else {
                uncheck()
            }
        } else {
            disable()
        }
    }
}

@BindingAdapter("enabled")
fun bindEnabled(view: InlineChoiceView, boolean: Boolean) {
    view.isEnabled = boolean
}

@BindingAdapter("chosen")
fun bindChoice(view: InlineChoiceView, boolean: Boolean?) {
    if (boolean != null)
        view.isChecked = boolean
}

@InverseBindingAdapter(attribute = "chosen")
fun onChosen(view: InlineChoiceView): Boolean {
    return !view.isChecked
}

@BindingAdapter("chosenAttrChanged")
fun setListeners(
    view: InlineChoiceView,
    attrChange: InverseBindingListener
) {
    view.addOnCheckedListener {
        attrChange.onChange()
    }
}