package com.gaboardi.commonui.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.gaboardi.common.extensions.dp
import com.gaboardi.commonui.R
import kotlin.math.atan2
import kotlin.math.roundToInt
import kotlin.math.sqrt

class PlateQuantitySeekbar(context: Context, attrs: AttributeSet?) : View(context, attrs) {
    private var thumbX: Float = 0f
    private val touchSlop = 10
    private val horizontalMargin = 30
    private var motionEventStartX = 0.0
    private var motionEventStartY = 0.0
    private var strokePadding = 0f
    private var diameter = 0.0
    private var stepWidth = 0.0
    private var font = ResourcesCompat.getFont(context, R.font.comfortaa_regular)

    private var onValueChanged: ((Int, Int) -> Unit)? = null
    private var onValueSelected: ((Int) -> Unit)? = null

    var value: Int = 0
        set(value) {
            field = value
            invalidate()
        }

    var steps = 10
        set(value) {
            field = value
            invalidate()
        }

    var radius: Float = 16f.dp
        set(value) {
            field = value
            diameter = radius * 2.0
            invalidate()
        }

    var strokeWidth = 8f.dp
        set(value) {
            field = value
            thumbStrokePaint.strokeWidth = value
            trackStrokePaint.strokeWidth = value
            strokePadding = value / 2
            invalidate()
        }

    var thumbTextSize = 24f.dp
        set(value) {
            field = value
            thumbTextPaint.textSize = value
            invalidate()
        }

    var trackTextSize = 24f.dp
        set(value) {
            field = value
            trackTextPaint.textSize = value
            invalidate()
        }

    var trackTextColor = Color.BLUE
        set(value) {
            field = value
            trackTextPaint.color = value
            invalidate()
        }

    var thumbTextColor = Color.BLACK
        set(value) {
            field = value
            thumbTextPaint.color = value
            invalidate()
        }

    var thumbBorderColor = Color.GREEN
        set(value) {
            field = value
            thumbStrokePaint.color = value
            invalidate()
        }

    var thumbColor = Color.BLUE
        set(value) {
            field = value
            thumbPaint.color = value
            invalidate()
        }

    var trackBorderColor = Color.BLUE
        set(value) {
            field = value
            trackStrokePaint.color = value
            invalidate()
        }

    var trackColor = Color.CYAN
        set(value) {
            field = value
            trackPaint.color = value
            invalidate()
        }

    private val thumbPaint = Paint().apply {
        color = thumbColor
        isAntiAlias = true
        isDither = true
    }

    private val thumbStrokePaint = Paint().apply {
        color = thumbBorderColor
        strokeWidth = this@PlateQuantitySeekbar.strokeWidth
        style = Paint.Style.STROKE
        isAntiAlias = true
        isDither = true
    }

    private val thumbTextPaint = Paint().apply {
        color = thumbTextColor
        textAlign = Paint.Align.CENTER
        typeface = font
        textSize = thumbTextSize
        isAntiAlias = true
        isDither = true
    }

    private val trackPaint = Paint().apply {
        color = trackColor
        isAntiAlias = true
        isDither = true
    }

    private val trackStrokePaint = Paint().apply {
        color = trackBorderColor
        strokeWidth = this@PlateQuantitySeekbar.strokeWidth
        style = Paint.Style.STROKE
        isAntiAlias = true
        isDither = true
    }

    private val trackTextPaint = Paint().apply {
        color = trackTextColor
        textAlign = Paint.Align.CENTER
        typeface = font
        textSize = trackTextSize
        isAntiAlias = true
        isDither = true
    }

    fun setOnValueChangedListener(listener: ((oldValue: Int, newValue: Int) -> Unit)?) {
        onValueChanged = listener
    }

    fun setOnValueSelectedListener(listener: ((newValue: Int) -> Unit)?) {
        onValueSelected = listener
    }

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.PlateQuantitySeekbar,
            0, 0
        ).apply {

            try {
                radius = getDimension(R.styleable.PlateQuantitySeekbar_radius, 16f.dp)
                strokeWidth = getDimension(R.styleable.PlateQuantitySeekbar_barStrokeWidth, 16f.dp)
                thumbTextSize = getDimension(R.styleable.PlateQuantitySeekbar_thumbTextSize, 16f.dp)
                trackTextSize = getDimension(R.styleable.PlateQuantitySeekbar_trackTextSize, 16f.dp)
                thumbTextColor =
                    getColor(R.styleable.PlateQuantitySeekbar_thumbTextColor, Color.WHITE)
                trackTextColor =
                    getColor(R.styleable.PlateQuantitySeekbar_trackTextColor, Color.BLACK)
                thumbBorderColor =
                    getColor(R.styleable.PlateQuantitySeekbar_thumbBorderColor, Color.BLACK)
                trackBorderColor =
                    getColor(R.styleable.PlateQuantitySeekbar_trackBorderColor, Color.RED)
                thumbColor = getColor(R.styleable.PlateQuantitySeekbar_thumbColor, Color.RED)
                trackColor = getColor(R.styleable.PlateQuantitySeekbar_trackColor, Color.WHITE)
            } finally {
                recycle()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(widthMeasureSpec, diameter.roundToInt())
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        stepWidth = (width.toDouble() - diameter) / steps.toDouble()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) return false
        return when (event.action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> {
                motionEventStartX = event.x.toDouble()
                motionEventStartY = event.y.toDouble()
                true
            }
            MotionEvent.ACTION_MOVE -> {
                val deltaX = event.x - motionEventStartX
                val deltaY = event.y - motionEventStartY
                val distance = sqrt(Math.pow(deltaX, 2.0) + Math.pow(deltaY, 2.0))
                val angle = Math.toDegrees(atan2(deltaY, deltaX)).toInt()
                if (distance > touchSlop && isHorizontal(angle))
                    parent.requestDisallowInterceptTouchEvent(true)
                thumbX = event.x.coerceIn(radius, width - radius)
                val newValue = ((thumbX - radius) / stepWidth).roundToInt()
                if(newValue != value) {
                    onValueChanged?.invoke(value, newValue)
                    value = newValue
                }
                invalidate()
                true
            }
            MotionEvent.ACTION_UP -> {
                onValueSelected?.invoke(value)
                true
            }
            else -> false
        }
    }

    private fun isHorizontal(angle: Int): Boolean {
        return angle in -horizontalMargin..horizontalMargin ||
                angle > 180 - horizontalMargin ||
                angle < -180 + horizontalMargin
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val stepPosition = value * stepWidth + radius
        val stepString = value.toString()

        canvas.drawRoundRect(
            0f,
            0f,
            width.toFloat(),
            height.toFloat(),
            radius,
            radius,
            trackPaint
        )
        canvas.drawRoundRect(
            strokePadding,
            strokePadding,
            width.toFloat() - strokePadding,
            height.toFloat() - strokePadding,
            radius,
            radius,
            trackStrokePaint
        )
        canvas.drawText(
            steps.toString(),
            width - radius,
            (height - thumbTextPaint.ascent() - thumbTextPaint.descent()) / 2,
            trackTextPaint
        )

        canvas.drawRoundRect(
            0f,
            0f,
            stepPosition.toFloat() + radius,
            height.toFloat(),
            radius,
            radius,
            thumbPaint
        )
        canvas.drawRoundRect(
            strokePadding,
            strokePadding,
            stepPosition.toFloat() + radius - strokePadding,
            height.toFloat() - strokePadding,
            radius,
            radius,
            thumbStrokePaint
        )

        canvas.drawCircle(stepPosition.toFloat(), height / 2f, radius - strokePadding, thumbPaint)
        canvas.drawCircle(
            stepPosition.toFloat(),
            height / 2f,
            radius - strokePadding,
            thumbStrokePaint
        )
        canvas.drawText(
            stepString,
            stepPosition.toFloat(),
            (height.toFloat() - thumbTextPaint.ascent() - thumbTextPaint.descent()) / 2,
            thumbTextPaint
        )
    }
}