package com.gaboardi.commonui.utility

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.common.extensions.dp

class SpacingItemDecoration(private val horizontal: Int = 16.dp, private val vertical: Int = 16.dp): RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
        outRect.right = horizontal
        outRect.left = horizontal

        outRect.top = if (itemPosition == 0)
            vertical
        else
            vertical / 2

        outRect.bottom = parent.adapter?.itemCount?.let {
            if (itemPosition == it - 1)
                vertical
            else
                vertical / 2
        } ?: vertical / 2
    }

}