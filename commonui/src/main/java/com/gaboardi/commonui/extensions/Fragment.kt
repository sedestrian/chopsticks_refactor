package com.gaboardi.commonui.extensions

import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.gaboardi.commonui.R
import com.google.android.material.snackbar.Snackbar

fun Fragment.hideKeyboard() {
    val imm =
        requireContext().applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view?.applicationWindowToken, 0)
}

fun Fragment.showError(message: String, coordinator: CoordinatorLayout? = null){
    val errorColor = ContextCompat.getColor(requireContext(), R.color.error_background)
    val textColor = ContextCompat.getColor(requireContext(), R.color.error_text)
    Snackbar.make(coordinator ?: requireView(), message, Snackbar.LENGTH_LONG)
        .setBackgroundTint(errorColor)
        .setTextColor(textColor)
        .show()
}

fun Fragment.showError(message: String){
    val errorColor = ContextCompat.getColor(requireContext(), R.color.error_background)
    val textColor = ContextCompat.getColor(requireContext(), R.color.error_text)
    Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG)
        .setBackgroundTint(errorColor)
        .setTextColor(textColor)
        .show()
}