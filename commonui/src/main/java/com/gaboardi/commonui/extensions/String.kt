package com.gaboardi.commonui.extensions

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan

fun String.colorCode(letterColor: Int): CharSequence {
    val shouldSpan = any { it.isLetter() }
    val result = if (shouldSpan) {
        val spannable = SpannableStringBuilder(this)
        forEachIndexed { index, c ->
            if (c.isLetter())
                spannable.setSpan(
                    ForegroundColorSpan(letterColor),
                    index,
                    index + 1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
        }
        spannable
    } else this
    return result
}