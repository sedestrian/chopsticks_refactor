package com.gaboardi.commonui.extensions

import android.content.Context
import androidx.annotation.AttrRes
import androidx.core.content.ContextCompat
import com.gaboardi.commonui.R

fun Context.getColorAttribute(@AttrRes attribute: Int): Int {
    val default = ContextCompat.getColor(this, R.color.red_500)
    val attributes = this.theme.obtainStyledAttributes(intArrayOf(attribute))
    val color = attributes.getColor(0, default)
    attributes.recycle()
    return color
}