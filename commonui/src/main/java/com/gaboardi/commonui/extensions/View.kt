package com.gaboardi.commonui.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

fun View.hideKeyboard() {
    val imm =
        context.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(applicationWindowToken, 0)
}

fun View.setGone(){
    visibility = View.GONE
}

fun View.setVisible(){
    visibility = View.VISIBLE
}