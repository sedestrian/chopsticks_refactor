package com.gaboardi.commonui.inputflilter

import android.text.InputFilter
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan

class PlateCodeInputFilter(val red: Int): InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence {
        val noSpecial = source.dropWhile { !it.isLetterOrDigit() }
        val shouldSpan = noSpecial.any { it.isLetter() }
        val result = if(shouldSpan){
            var processed = noSpecial
            val sections: MutableList<LetterSection> = mutableListOf()
            while (processed.any { it.isLetter() }){
                val sectionStart = processed.indexOfFirst { it.isLetter() }
                val sectionEnd = if(processed.any { !it.isLetter() })
                    processed.substring(sectionStart, processed.length).indexOfFirst { !it.isLetter() }
                else
                    processed.length
                sections.add(LetterSection(sectionStart, sectionEnd))
                processed = processed.removeRange(sectionStart, sectionEnd)
            }
            val spannable = SpannableStringBuilder(noSpecial)
            sections.forEach {
                spannable.setSpan(
                    ForegroundColorSpan(red),
                    it.start,
                    it.end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            spannable
        }else noSpecial
        return result
    }

    data class LetterSection(val start: Int, val end: Int)
}