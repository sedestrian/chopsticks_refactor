package com.gaboardi.commonui.model

import androidx.annotation.IntRange
import androidx.annotation.StringRes
import com.gaboardi.commonui.R

enum class RestaurantType(val id: Int, @StringRes val string: Int) {
    ALL_YOU_CAN_EAT(1, R.string.all_you_can_eat),
    A_LA_CARTE(0, R.string.alla_carta);

    companion object {
        fun fromId(@IntRange(from = 0, to = 1) id: Int) = values().first { it.id == id }
    }
}