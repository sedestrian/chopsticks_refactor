package com.gaboardi.order.dao

import androidx.room.*
import com.gaboardi.order.model.db.OrderData
import com.gaboardi.order.model.db.OrderEntity
import kotlinx.coroutines.flow.Flow

@Dao
abstract class OrderDao {
    @Transaction
    @Query("SELECT * FROM orders WHERE id = :orderId LIMIT 1")
    abstract fun observeOrder(orderId: String): Flow<OrderData>

    @Transaction
    @Query("SELECT * FROM orders WHERE id IN(:orderIds)")
    abstract fun observeOrders(orderIds: List<String>): Flow<List<OrderData>>

    @Transaction
    open suspend fun insertOrders(orders: List<OrderEntity>) {
        insertOrdersDB(orders)
    }

    @Transaction
    open suspend fun insertOrder(order: OrderEntity) {
        insertOrderDB(order)
    }

    @Query("DELETE FROM orders WHERE id NOT IN (:orders)")
    abstract suspend fun cleanOrders(orders: List<String>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertOrdersDB(orders: List<OrderEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertOrderDB(order: OrderEntity)
}