package com.gaboardi.order.model.api

import com.google.firebase.Timestamp

data class OrderResult(
    val id: String,
    val userId: String,
    val tableId: String?,
    val restaurantId: String,
    val name: String,
    val date: Timestamp,
    val rating: Double?,
    val price: Double?,
    val priceTime: Int?,
    val currency: String?
)