package com.gaboardi.order.model.ui

import java.time.LocalDateTime

data class Order(
    val id: String,
    val restaurantId: String,
    val tableId: String?,
    val name: String,
    val date: LocalDateTime,
    val rating: Double?,
    val price: Double?,
    val priceTime: Int?,
    val currency: String?
)