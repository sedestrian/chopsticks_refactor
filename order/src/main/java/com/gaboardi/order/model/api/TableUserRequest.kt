package com.gaboardi.order.model.api

data class TableUserRequest(
    val userId: String,
    val orderId: String
)