package com.gaboardi.order.model.ui

data class UserSeparatedOrders(
    val userOrder: OrderUI?,
    val orders: List<OrderUI>
)