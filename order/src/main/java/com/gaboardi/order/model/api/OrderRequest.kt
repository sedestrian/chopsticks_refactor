package com.gaboardi.order.model.api

import com.google.firebase.Timestamp

data class OrderRequest(
    val restaurantId: String,
    val tableId: String?,
    val userId: String,
    val name: String,
    val date: Timestamp,
    val rating: Double? = null,
    val price: Double? = null,
    val currency: String? = null
)