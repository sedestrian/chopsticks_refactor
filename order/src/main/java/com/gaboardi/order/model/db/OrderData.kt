package com.gaboardi.order.model.db

import androidx.room.DatabaseView
import androidx.room.Relation
import com.gaboardi.plate.model.db.PlateEntity
import java.time.LocalDateTime

@DatabaseView(
    """SELECT
        orders.id,
        orders.userId,
        orders.restaurantId,
        orders.tableId,
        orders.name,
        orders.date,
        orders.rating,
        orders.price,
        orders.priceTime,
        orders.currency,
        plates.*
    FROM orders LEFT JOIN plates ON orders.id = plates.orderId GROUP BY orders.id"""
)
data class OrderData(
    val id: String,
    val userId: String,
    val restaurantId: String,
    val tableId: String?,
    val name: String,
    val date: LocalDateTime,
    val rating: Double?,
    val price: Double?,
    val priceTime: Int?,
    val currency: String?,
    @Relation(parentColumn = "id", entityColumn = "orderId")
    val plates: List<PlateEntity>
)