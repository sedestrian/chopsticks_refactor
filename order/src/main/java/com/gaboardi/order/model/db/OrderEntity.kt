package com.gaboardi.order.model.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDateTime

@Entity(tableName = "orders")
data class OrderEntity(
    @PrimaryKey
    val id: String,
    val userId: String,
    @ColumnInfo(index = true)
    val restaurantId: String,
    val tableId: String?,
    val name: String,
    val date: LocalDateTime,
    val rating: Double?,
    val price: Double?,
    val priceTime: Int?,
    val currency: String?
)