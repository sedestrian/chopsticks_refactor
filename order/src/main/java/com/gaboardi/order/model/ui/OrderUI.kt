package com.gaboardi.order.model.ui

import com.gaboardi.plate.model.ui.PlateUI
import java.time.LocalDateTime

data class OrderUI(
    val id: String,
    val restaurantId: String,
    val tableId: String?,
    val userId: String,
    val name: String,
    val date: LocalDateTime,
    val rating: Double?,
    val price: Double?,
    val priceTime: Int?,
    val currency: String?,
    val plates: List<PlateUI>
)