package com.gaboardi.order.converter

import com.gaboardi.order.model.api.OrderResult
import com.gaboardi.order.model.db.OrderEntity
import java.time.LocalDateTime
import java.time.ZoneOffset

fun OrderResult.toEntity(): OrderEntity{
    return OrderEntity(
        id,
        userId,
        restaurantId,
        tableId,
        name,
        LocalDateTime.ofEpochSecond(
            date.seconds,
            date.nanoseconds,
            ZoneOffset.UTC
        ),
        rating,
        price,
        priceTime,
        currency
    )
}