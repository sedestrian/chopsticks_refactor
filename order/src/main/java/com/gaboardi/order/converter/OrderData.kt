package com.gaboardi.order.converter

import com.gaboardi.order.R
import com.gaboardi.order.model.db.OrderData
import com.gaboardi.order.model.ui.OrderUI
import com.gaboardi.plate.converter.toPlateUi
import com.gaboardi.plate.converter.toUi
import com.gaboardi.plate.model.ui.EmptyPlateType
import com.gaboardi.plate.model.ui.Plate
import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.model.ui.PlateUI

fun OrderData.toUi(): OrderUI {
    return OrderUI(
        id,
        restaurantId,
        tableId,
        userId,
        name,
        date,
        rating,
        price,
        priceTime,
        currency,
        getProcessedPlates(plates.map { it.toUi() })
    )
}

fun getProcessedPlates(orderPlates: List<Plate>): List<PlateUI> {
    val plates = orderPlates.map { it.toPlateUi() }
    val mutablePlates: MutableList<PlateUI> = plates.sortedWith(
        compareBy(PlateUI.Plate::state, PlateUI.Plate::name)
    ).toMutableList()

    val indexOfFirstOrdered = mutablePlates.indexOfFirst {
        it is PlateUI.Plate &&
                it.state == PlateState.ORDERED.state
    }

    if (indexOfFirstOrdered != -1) {
        val index = if (indexOfFirstOrdered > 0) indexOfFirstOrdered else 0
        mutablePlates.add(
            index,
            PlateUI.Header(R.string.plate_list_ordered_header, PlateState.ORDERED)
        )
    } else {
        mutablePlates.add(
            0,
            PlateUI.Header(R.string.plate_list_ordered_header, PlateState.ORDERED)
        )
        mutablePlates.add(1, PlateUI.Empty(EmptyPlateType.ORDERED))
    }

    val indexOfFirstForLater = mutablePlates.indexOfFirst {
        it is PlateUI.Plate &&
                it.state == PlateState.LATER.state
    }

    if (indexOfFirstForLater != -1) {
        val index = if (indexOfFirstForLater > 0) indexOfFirstForLater else 0
        mutablePlates.add(
            index,
            PlateUI.Header(R.string.plate_list_forlater_header, PlateState.LATER)
        )
    } else {
        val index = if (indexOfFirstOrdered != -1) mutablePlates.indexOfLast {
            it is PlateUI.Plate && it.state == PlateState.ORDERED.state
        } + 1 else 2
        mutablePlates.add(
            index,
            PlateUI.Header(R.string.plate_list_forlater_header, PlateState.LATER)
        )
        mutablePlates.add(index + 1, PlateUI.Empty(EmptyPlateType.LATER))
    }

    val indexOfFirstReceived = mutablePlates.indexOfFirst {
        it is PlateUI.Plate &&
                it.state == PlateState.RECEIVED.state
    }

    if (indexOfFirstReceived != -1) {
        val index = if (indexOfFirstReceived > 0) indexOfFirstReceived else 0
        mutablePlates.add(
            index,
            PlateUI.Header(R.string.plate_list_received_header, PlateState.RECEIVED)
        )
    }
    return mutablePlates
}