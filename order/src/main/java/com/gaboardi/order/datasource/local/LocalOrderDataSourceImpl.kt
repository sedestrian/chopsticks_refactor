package com.gaboardi.order.datasource.local

import com.gaboardi.order.converter.toUi
import com.gaboardi.order.dao.OrderDao
import com.gaboardi.order.model.db.OrderEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LocalOrderDataSourceImpl @Inject constructor(
    private val orderDao: OrderDao
) : LocalOrderDataSource {
    override fun observeOrder(orderId: String) = orderDao.observeOrder(orderId).map { it.toUi() }

    override fun observeOrders(orderIds: List<String>) =
        orderDao.observeOrders(orderIds).map { list -> list.map { item -> item.toUi() } }

    override suspend fun saveOrders(orders: List<OrderEntity>) = withContext(Dispatchers.IO) {
        orderDao.insertOrders(orders)
    }

    override suspend fun saveOrder(order: OrderEntity) = withContext(Dispatchers.IO) {
        orderDao.insertOrder(order)
    }
}