package com.gaboardi.order.datasource.local

import com.gaboardi.order.model.db.OrderEntity
import com.gaboardi.order.model.ui.OrderUI
import kotlinx.coroutines.flow.Flow

interface LocalOrderDataSource {
    suspend fun saveOrders(orders: List<OrderEntity>)
    fun observeOrder(orderId: String): Flow<OrderUI>
    fun observeOrders(orderIds: List<String>): Flow<List<OrderUI>>
    suspend fun saveOrder(order: OrderEntity)
}