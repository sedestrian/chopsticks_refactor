package com.gaboardi.order.datasource.remote

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.order.model.api.OrderResult
import com.google.firebase.Timestamp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

interface RemoteOrderDataSource {
    @ExperimentalCoroutinesApi
    suspend fun deleteOrder(orderId: String): ApiResponse<Unit>
    @ExperimentalCoroutinesApi
    fun subscribeToFirestore(): Flow<ApiResponse<List<OrderResult>>>
    suspend fun createOrder(restaurantId: String, tableId: String?, name: String, date: Timestamp): ApiResponse<String>
    suspend fun setOrderTable(orderId: String, tableId: String): ApiResponse<Unit>
    fun startOrdersSubscription(orders: List<String>): Flow<ApiResponse<List<OrderResult>>>
    fun startOrderSubscription(order: String): Flow<ApiResponse<OrderResult>>
}