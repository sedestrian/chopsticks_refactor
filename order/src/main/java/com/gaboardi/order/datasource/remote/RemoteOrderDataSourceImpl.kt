package com.gaboardi.order.datasource.remote

import com.gaboardi.common.extensions.fromJson
import com.gaboardi.common.extensions.toJson
import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.Collections
import com.gaboardi.common.model.DatabaseField
import com.gaboardi.common.model.ErrorMessage
import com.gaboardi.order.R
import com.gaboardi.order.model.api.OrderRequest
import com.gaboardi.order.model.api.OrderResult
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RemoteOrderDataSourceImpl @Inject constructor(
    private val firestore: FirebaseFirestore,
    private val firebaseUser: FirebaseUser?
) : RemoteOrderDataSource {
    override suspend fun createOrder(
        restaurantId: String,
        tableId: String?,
        name: String,
        date: Timestamp
    ): ApiResponse<String> {
        return try {
            firebaseUser?.let { user ->
                val order = OrderRequest(restaurantId, tableId, user.uid, name, date)
                val ordersCollection = firestore.collection(Collections.Orders)
                val result = ordersCollection.add(order).await()
                ApiResponse.success(result.id)
            } ?: ApiResponse.error(ErrorMessage.of(R.string.error_missing_user))
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage ?: "Api error")
        }
    }

    override suspend fun setOrderTable(orderId: String, tableId: String): ApiResponse<Unit> {
        return try {
            val orderDocument = firestore.collection(Collections.Orders).document(orderId)
            orderDocument.update(DatabaseField.Order.TableId, tableId).await()
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    @ExperimentalCoroutinesApi
    override suspend fun deleteOrder(orderId: String): ApiResponse<Unit> {
        return try {
            val ordersCollection = firestore.collection(Collections.Orders)
            ordersCollection.document(orderId).delete().await()
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage ?: "Api error")
        }
    }

    @ExperimentalCoroutinesApi
    override fun subscribeToFirestore() = callbackFlow<ApiResponse<List<OrderResult>>> {
        firebaseUser?.let { user ->
            val subscription = firestore.collection(Collections.Orders)
                .whereEqualTo(DatabaseField.Order.UserId, user.uid)
                .addSnapshotListener { value, error ->
                    when {
                        error != null -> sendBlocking(ApiResponse.error(error.localizedMessage))
                        value != null -> {
                            try {
                                val data = value.documents.map {
                                    val order = it.data.toJson().fromJson<OrderResult>()
                                    order.copy(id = it.id)
                                }
                                sendBlocking(ApiResponse.success(data))
                            } catch (e: Exception) {
                                sendBlocking(ApiResponse.error(e.localizedMessage))
                            }
                        }
                    }
                }
            awaitClose { subscription.remove() }
        }
    }

    override fun startOrdersSubscription(orders: List<String>) =
        callbackFlow<ApiResponse<List<OrderResult>>> {
            val collection = firestore.collection(Collections.Orders)
            val query = collection.whereIn(FieldPath.documentId(), orders)
            val callback = query.addSnapshotListener { value, error ->
                when {
                    error != null -> sendBlocking(ApiResponse.error(error.localizedMessage))
                    value != null -> {
                        try {
                            val data = value.documents.map {
                                val order = it.data.toJson().fromJson<OrderResult>()
                                order.copy(id = it.id)
                            }
                            sendBlocking(ApiResponse.success(data))
                        } catch (e: Exception) {
                            sendBlocking(ApiResponse.error(e.localizedMessage))
                        }
                    }
                }
            }
            awaitClose { callback.remove() }
        }

    override fun startOrderSubscription(order: String) =
        callbackFlow<ApiResponse<OrderResult>> {
            val collection = firestore.collection(Collections.Orders)
            val query = collection.whereEqualTo(FieldPath.documentId(), order)
            val callback = query.addSnapshotListener { value, error ->
                when {
                    error != null -> sendBlocking(ApiResponse.error(error.localizedMessage))
                    value != null -> {
                        try {
                            val data = value.documents.map {
                                val order = it.data.toJson().fromJson<OrderResult>()
                                order.copy(id = it.id)
                            }
                            sendBlocking(ApiResponse.success(data.first()))
                        } catch (e: Exception) {
                            sendBlocking(ApiResponse.error(e.localizedMessage))
                        }
                    }
                }
            }
            awaitClose { callback.remove() }
        }
}