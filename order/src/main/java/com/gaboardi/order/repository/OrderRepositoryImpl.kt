package com.gaboardi.order.repository

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.State
import com.gaboardi.order.converter.toEntity
import com.gaboardi.order.datasource.local.LocalOrderDataSource
import com.gaboardi.order.datasource.remote.RemoteOrderDataSource
import com.google.firebase.Timestamp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class OrderRepositoryImpl @Inject constructor(
    private val localOrdersDataSource: LocalOrderDataSource,
    private val remoteOrdersDataSource: RemoteOrderDataSource
) : OrderRepository {
    override fun observeOrder(id: String) = localOrdersDataSource.observeOrder(id)

    override fun observeOrders(orderIds: List<String>) =
        localOrdersDataSource.observeOrders(orderIds)

    override fun createOrder(
        restaurantId: String,
        tableId: String?,
        name: String,
        date: Timestamp
    ) = flow {
        emit(State.loading())
        val result = remoteOrdersDataSource.createOrder(restaurantId, tableId, name, date)
        when (result) {
            is ApiResponse.Success -> emit(State.success(result.body))
            is ApiResponse.Error -> emit(State.error(result.message))
        }
    }

    override suspend fun createOrderSuspend(
        restaurantId: String,
        tableId: String?,
        name: String,
        date: Timestamp
    ) = remoteOrdersDataSource.createOrder(restaurantId, tableId, name, date)

    override fun setOrderTable(orderId: String, tableId: String) = flow {
        emit(State.loading())
        val result = remoteOrdersDataSource.setOrderTable(orderId, tableId)
        when (result) {
            is ApiResponse.Success -> emit(State.success())
            is ApiResponse.Error -> emit(State.error(result.message))
        }
    }

    @ExperimentalCoroutinesApi
    override fun deleteOrder(orderId: String) = flow {
        emit(State.loading())
        val result = remoteOrdersDataSource.deleteOrder(orderId)
        when (result) {
            is ApiResponse.Success -> emit(State.success())
            is ApiResponse.Error -> emit(State.error(result.message))
        }
    }

    @ExperimentalCoroutinesApi
    override suspend fun subscribeToFirestore() = flow {
        remoteOrdersDataSource.subscribeToFirestore().collect { result ->
            when (result) {
                is ApiResponse.Success -> {
                    try {
                        val data = result.body.map { it.toEntity() }
                        localOrdersDataSource.saveOrders(data)
                        emit(State.success())
                    } catch (e: Exception) {
                        emit(State.error(e.localizedMessage))
                    }
                }
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        }
    }

    override fun startOrdersSubscription(orderIds: List<String>) = flow {
        remoteOrdersDataSource.startOrdersSubscription(orderIds).collect { result ->
            when (result) {
                is ApiResponse.Success -> {
                    try {
                        val data = result.body.map { it.toEntity() }
                        localOrdersDataSource.saveOrders(data)
                        emit(State.success())
                    } catch (e: Exception) {
                        emit(State.error(e.localizedMessage))
                    }
                }
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        }
    }

    override fun startOrderSubscription(orderId: String) = flow {
        remoteOrdersDataSource.startOrderSubscription(orderId).collect { result ->
            when (result) {
                is ApiResponse.Success -> {
                    try {
                        val data = result.body.toEntity()
                        localOrdersDataSource.saveOrder(data)
                        emit(State.success())
                    } catch (e: Exception) {
                        emit(State.error(e.localizedMessage))
                    }
                }
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        }
    }
}