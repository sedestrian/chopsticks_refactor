package com.gaboardi.order.repository

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.State
import com.gaboardi.order.model.ui.OrderUI
import com.google.firebase.Timestamp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

interface OrderRepository {
    @ExperimentalCoroutinesApi
    fun deleteOrder(orderId: String): Flow<State>
    @ExperimentalCoroutinesApi
    suspend fun subscribeToFirestore(): Flow<State>
    fun createOrder(restaurantId: String, tableId: String?, name: String, date: Timestamp): Flow<State>
    fun observeOrder(id: String): Flow<OrderUI>
    fun setOrderTable(orderId: String, tableId: String): Flow<State>
    fun observeOrders(orderIds: List<String>): Flow<List<OrderUI>>
    fun startOrdersSubscription(orderIds: List<String>): Flow<State>
    fun startOrderSubscription(orderId: String): Flow<State>
    suspend fun createOrderSuspend(
        restaurantId: String,
        tableId: String?,
        name: String,
        date: Timestamp
    ): ApiResponse<String>
}