package com.gaboardi.order.usecase

import com.gaboardi.order.repository.OrderRepository
import javax.inject.Inject

class SubscribeToOrderUseCase @Inject constructor(
    private val orderRepository: OrderRepository
) {
    fun execute(orderId: String) = orderRepository.startOrderSubscription(orderId)
}