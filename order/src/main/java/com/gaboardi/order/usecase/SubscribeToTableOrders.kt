package com.gaboardi.order.usecase

import com.gaboardi.order.repository.OrderRepository
import javax.inject.Inject

class SubscribeToTableOrders @Inject constructor(
    private val orderRepository: OrderRepository
) {
    suspend fun execute(orders: List<String>) = orderRepository.startOrdersSubscription(orders)
}