package com.gaboardi.order.usecase

import com.gaboardi.order.repository.OrderRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class OrdersSubscriptionUseCase @Inject constructor(
    private val ordersRepository: OrderRepository
) {
    @ExperimentalCoroutinesApi
    suspend fun subscribeToRemote() = ordersRepository.subscribeToFirestore()
}