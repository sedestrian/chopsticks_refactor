package com.gaboardi.order.usecase

import com.gaboardi.common.model.State
import com.gaboardi.order.model.api.OrderRequest
import com.gaboardi.order.repository.OrderRepository
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CreateEmptyOrderUseCase @Inject constructor(
    private val ordersRepository: OrderRepository,
) {
    @ExperimentalCoroutinesApi
    suspend fun create(
        name: String,
        restaurantId: String,
        date: Timestamp
    ): Flow<State> {
        return ordersRepository.createOrder(restaurantId, null, name, date)
    }
}