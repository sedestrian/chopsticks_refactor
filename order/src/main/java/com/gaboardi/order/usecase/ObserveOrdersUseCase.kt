package com.gaboardi.order.usecase

import com.gaboardi.order.model.ui.UserSeparatedOrders
import com.gaboardi.order.repository.OrderRepository
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ObserveOrdersUseCase @Inject constructor(
    private val orderRepository: OrderRepository,
    private val firebaseUser: FirebaseUser?
) {
    suspend fun execute(orders: List<String>) =
        orderRepository.observeOrders(orders).map {
            val userOrder = it.firstOrNull { order -> order.userId == firebaseUser?.uid }
            val others = it.filter { order -> order.userId != firebaseUser?.uid }
            UserSeparatedOrders(userOrder, others)
        }
}