package com.gaboardi.order.usecase

import com.gaboardi.common.model.State
import com.gaboardi.order.repository.OrderRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DeleteOrderUseCase @Inject constructor(
    private val ordersRepository: OrderRepository
) {
    @ExperimentalCoroutinesApi
    fun delete(orderId: String): Flow<State> {
        return ordersRepository.deleteOrder(orderId)
    }
}