package com.gaboardi.order.usecase

import com.gaboardi.order.repository.OrderRepository
import javax.inject.Inject

class ObserveOrderUseCase @Inject constructor(
    private val ordersRepository: OrderRepository
) {
    fun observe(orderId: String) = ordersRepository.observeOrder(orderId)
}