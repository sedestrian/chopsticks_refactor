package com.gaboardi.order.usecase

import com.gaboardi.order.repository.OrderRepository
import javax.inject.Inject

class SetOrderTableUseCase @Inject constructor(
    private val orderRepository: OrderRepository
) {
    fun execute(orderId: String, tableId: String) = orderRepository.setOrderTable(orderId, tableId)
}