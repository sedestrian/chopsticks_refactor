package com.gaboardi.table.model.db

import androidx.room.DatabaseView

@DatabaseView("""
    SELECT 
        plates.id,
        plates.tableId,
        platestates.tableUserId,
        plates.name,
        plates.code,
        platestates.quantity,
        platestates.received,
        platestates.state
    FROM 
        platestates 
    LEFT JOIN 
        plates 
    ON 
        platestates.plateId = plates.id
""")
data class PlateData(
    val id: String,
    val tableId: String,
    val tableUserId: String,
    val name: String,
    val code: String?,
    val quantity: Int,
    val received: Int,
    val state: Int
)