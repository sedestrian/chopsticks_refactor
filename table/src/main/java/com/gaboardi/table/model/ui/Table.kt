package com.gaboardi.table.model.ui

import java.time.LocalDateTime

data class Table(
    val id: String,
    val creatorId: String,
    val isTable: Boolean,
    val name: String,
    val code: String?,
    val dynamicLink: String?,
    val date: LocalDateTime,
    val users: List<TableUser>,
)