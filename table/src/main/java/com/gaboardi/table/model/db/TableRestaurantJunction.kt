package com.gaboardi.table.model.db

import androidx.room.Entity

@Entity(
    primaryKeys = ["tableId", "restaurantId"]
)
data class TableRestaurantJunction(
    val tableId: String,
    val restaurantId: String
)