package com.gaboardi.table.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "tableusers"
)
data class TableUserEntity(
    @PrimaryKey
    val id: String,
    val tableId: String,
    val userId: String,
    val restaurantId: String,
    val rating: Float?,
    val price: Float?
)