package com.gaboardi.table.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDateTime

@Entity(tableName = "tables")
data class TableEntity(
    @PrimaryKey
    val id: String,
    val isTable: Boolean,
    val creatorId: String,
    val name: String,
    val code: String?,
    val date: LocalDateTime,
    val dynamicLink: String?
)