package com.gaboardi.table.model.db

data class TableUserInsert(
    val user: TableUserEntity,
    val plates: List<TablePlateEntity>
)