package com.gaboardi.table.model.api

data class PlateStateResult(
    val plateId: String,
    val state: Int,
    val quantity: Int,
    val received: Int
)