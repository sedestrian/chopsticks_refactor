package com.gaboardi.table.model.db

data class TableInsert(
    val table: TableEntity,
    val users: List<TableUserInsert>
)