package com.gaboardi.table.model.db

import androidx.room.DatabaseView
import androidx.room.Relation

@DatabaseView("""
    SELECT
        tableusers.id,
        tableusers.tableId,
        tableusers.userId,
        tableusers.restaurantId,
        tableusers.rating,
        tableusers.price,
        platedata.*
    FROM
        tableusers
    LEFT JOIN
        platedata
    ON
        tableusers.id = platedata.tableUserId
""")
data class TableUserData(
    val id: String,
    val tableId: String,
    val userId: String,
    val restaurantId: String,
    val rating: Float?,
    val price: Float?,
    @Relation(parentColumn = "id", entityColumn = "tableUserId")
    val plates: List<PlateData>
)