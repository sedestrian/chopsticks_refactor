package com.gaboardi.table.model.ui

import com.gaboardi.plate.model.ui.Plate

data class TableUser(
    val id: String,
    val tableId: String,
    val userId: String,
    val restaurantId: String,
    val vote: Float?,
    val price: Float?,
    val plates: List<Plate>
)