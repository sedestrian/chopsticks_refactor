package com.gaboardi.table.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "platestates"
)
data class TablePlateEntity(
    @PrimaryKey
    val id: String,
    val plateId: String,
    val tableUserId: String,
    val state: Int,
    val quantity: Int,
    val received: Int
)