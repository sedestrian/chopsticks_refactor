package com.gaboardi.table.model.api

data class TableUserRequest(
    val userId: String,
    val restaurantId: String
)