package com.gaboardi.table.model.api

import com.google.firebase.Timestamp

data class TableRequest(
    val isTable: Boolean,
    val date: Timestamp,
    val creatorId: String,
    val name: String,
    val userIds: List<String>,
    val users: List<TableUserRequest>
)