package com.gaboardi.table.model.db

import androidx.room.DatabaseView
import androidx.room.Relation
import java.time.LocalDateTime

@DatabaseView("""
    SELECT 
        tables.id,
        tables.isTable,
        tables.creatorId,
        tables.name,
        tables.code,
        tables.dynamicLink,
        tables.date
    FROM
        tables
    JOIN
        tableuserdata
    ON
        tables.id = tableuserdata.tableId
    GROUP BY
        tables.id
    ORDER BY
        tables.date
""")
data class TableData(
    val id: String,
    val isTable: Boolean,
    val creatorId: String,
    val name: String,
    val code: String?,
    val dynamicLink: String?,
    val date: LocalDateTime,
    @Relation(parentColumn = "id", entityColumn = "tableId")
    val users: List<TableUserData>
)