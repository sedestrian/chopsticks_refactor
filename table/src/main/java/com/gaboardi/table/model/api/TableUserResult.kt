package com.gaboardi.table.model.api

data class TableUserResult(
    val userId: String,
    val restaurantId: String,
    val vote: Float?,
    val price: Float?,
    val plates: List<PlateStateResult>?
)