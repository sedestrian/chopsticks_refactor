package com.gaboardi.table.model.api

import com.gaboardi.plate.model.api.PlateResult
import com.google.firebase.Timestamp

data class TableResult(
    val id: String,
    val isTable: Boolean,
    val creatorId: String,
    val name: String?,
    val code: String?,
    val date: Timestamp,
    val dynamicLink: String?,
    val users: List<TableUserResult>,
    val userIds: List<String>,
    val plates: List<PlateResult>?
)