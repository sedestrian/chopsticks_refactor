package com.gaboardi.table.repository

import com.gaboardi.common.model.State
import com.gaboardi.table.model.ui.Table
import kotlinx.coroutines.flow.Flow

interface TableRepository {
    fun observeTable(tableId: String): Flow<Table?>
    fun observeTableWithCode(tableCode: String): Flow<Table?>
    fun startTableSubscription(tableId: String): Flow<State>
    fun startTablesSubscription(): Flow<State>
    fun subscribeToTableWithCode(tableCode: String): Flow<State>
    fun joinTable(tableCode: String, restaurantId: String): Flow<State>
    fun addTable(name: String, restaurantId: String, isTable: Boolean): Flow<State>
    suspend fun setPlateReceived(tableId: String, plateId: String, amount: Int)
}