package com.gaboardi.table.repository

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.State
import com.gaboardi.plate.model.db.PlateInsert
import com.gaboardi.plate.repository.PlateRepository
import com.gaboardi.table.R
import com.gaboardi.table.datasource.local.LocalTableDataSource
import com.gaboardi.table.datasource.remote.RemoteTableDataSource
import com.gaboardi.table.model.api.TableRequest
import com.gaboardi.table.model.api.TableUserRequest
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class TableRepositoryImpl @Inject constructor(
    private val remoteTableDataSource: RemoteTableDataSource,
    private val localTableDataSource: LocalTableDataSource,
    private val firebaseUser: FirebaseUser?,
    private val plateRepository: PlateRepository
) : TableRepository {
    override fun observeTable(tableId: String) = localTableDataSource.observeTable(tableId)

    override fun observeTableWithCode(tableCode: String) =
        localTableDataSource.observeTableWithCode(tableCode)

    override suspend fun setPlateReceived(tableId: String, plateId: String, amount: Int) {
        firebaseUser?.let {
            remoteTableDataSource.setPlateReceived(tableId, it.uid, plateId, amount)
        }
    }

    override fun addTable(name: String, restaurantId: String, isTable: Boolean) = flow {
        emit(State.loading())
        firebaseUser?.let {
            val request = TableRequest(
                isTable,
                Timestamp.now(),
                it.uid,
                name,
                listOf(it.uid),
                listOf(
                    TableUserRequest(
                        it.uid,
                        restaurantId
                    )
                )
            )
            val result = remoteTableDataSource.addTable(request)
            when (result) {
                is ApiResponse.Success -> emit(State.success(result.body))
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        } ?: emit(State.error(R.string.server_error))
    }

    override fun joinTable(tableCode: String, restaurantId: String) = flow {
        emit(State.loading())
        /*localTableDataSource.getTableFromCode(tableCode)?.let { table ->
            val result =
                orderRepository.createOrderSuspend(
                    restaurantId,
                    table.id,
                    table.name,
                    Timestamp.now()
                )
            when (result) {
                is ApiResponse.Success -> {
                    when (val state = completeJoinTable(table.id, result.body)) {
                        is ApiResponse.Success -> emit(State.success())
                        is ApiResponse.Error -> emit(State.error(state.message))
                    }
                }
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        } ?: emit(State.error("Missing table"))*/
    }

    private suspend fun completeJoinTable(tableId: String, orderId: String) =
        firebaseUser?.let {
            remoteTableDataSource.joinTable(tableId, it.uid, orderId)
        } ?: ApiResponse.error("")

    override fun startTableSubscription(tableId: String) = flow {
        remoteTableDataSource.startTableSubscription(tableId).collect { result ->
            when (result) {
                is ApiResponse.Success -> {
                    try {
                        localTableDataSource.insertTable(result.body)
                        emit(State.success())
                    } catch (e: Exception) {
                        emit(State.error(e.localizedMessage))
                    }
                }
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        }
    }

    override fun startTablesSubscription() = flow {
        remoteTableDataSource.startTablesSubscription().collect { result ->
            when (result) {
                is ApiResponse.Success -> {
                    try {
                        val plates = result.body.map { table ->
                            PlateInsert(
                                table.plates ?: listOf(),
                                table.id
                            )
                        }
                        plateRepository.savePlates(plates)
                        localTableDataSource.insertTables(result.body)
                        emit(State.success())
                    } catch (e: Exception) {
                        emit(State.error(e.localizedMessage))
                    }
                }
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        }
    }

    override fun subscribeToTableWithCode(tableCode: String) = flow {
        remoteTableDataSource.subscribeToTableWithCode(tableCode).collect { result ->
            when (result) {
                is ApiResponse.Success -> {
                    try {
                        result.body?.let {
                            localTableDataSource.insertTable(it)
                            emit(State.success())
                        } ?: emit(State.error("Null result"))
                    } catch (e: Exception) {
                        emit(State.error(e.localizedMessage))
                    }
                }
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        }
    }
}