package com.gaboardi.table.converter

import com.gaboardi.common.extensions.hash
import com.gaboardi.plate.model.ui.Plate
import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.table.model.api.PlateStateResult
import com.gaboardi.table.model.api.TableResult
import com.gaboardi.table.model.api.TableUserResult
import com.gaboardi.table.model.db.*
import com.gaboardi.table.model.ui.Table
import com.gaboardi.table.model.ui.TableUser
import java.time.LocalDateTime
import java.time.ZoneOffset

fun TableResult.toInsertData(): TableInsert {
    val table = TableInsert(
        TableEntity(
            id,
            isTable,
            creatorId,
            name ?: "",
            code,
            LocalDateTime.ofEpochSecond(
                date.seconds,
                date.nanoseconds,
                ZoneOffset.UTC
            ),
            dynamicLink
        ),
        users.map {
            val user = it.toEntity(id)
            val plates = it.plates?.map { it.toEntity(user.id) } ?: listOf()
            TableUserInsert(user, plates)
        }
    )
    return table
}

fun PlateStateResult.toEntity(tableUserId: String): TablePlateEntity {
    val id = (plateId + tableUserId).hash("MD5")
    return TablePlateEntity(
        id,
        plateId,
        tableUserId,
        state,
        quantity,
        received
    )
}

fun TableData.toUi(): Table {
    return Table(
        id,
        creatorId,
        isTable,
        name,
        code,
        dynamicLink,
        date,
        users.map { it.toUi() }
    )
}

fun PlateData.toUi(): Plate {
    return Plate(
        id,
        tableId,
        tableUserId,
        name,
        code ?: "",
        quantity,
        received,
        state
    )
}

fun Plate.toPlate(): PlateUI.Plate {
    return PlateUI.Plate(
        id,
        tableId,
        name,
        code,
        quantity,
        received,
        state
    )
}

fun TableUserData.toUi(): TableUser {
    return TableUser(
        id,
        tableId,
        userId,
        restaurantId,
        rating,
        price,
        plates.map { it.toUi() }
    )
}

fun TableUserResult.toEntity(tableId: String): TableUserEntity {
    val id = (tableId + userId).hash("MD5")
    return TableUserEntity(
        id,
        tableId,
        userId,
        restaurantId,
        vote,
        price
    )
}