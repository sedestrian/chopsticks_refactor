package com.gaboardi.table.di

import com.gaboardi.table.datasource.local.LocalTableDataSource
import com.gaboardi.table.datasource.local.LocalTableDataSourceImpl
import com.gaboardi.table.datasource.remote.RemoteTableDataSource
import com.gaboardi.table.datasource.remote.RemoteTableDataSourceImpl
import com.gaboardi.table.repository.TableRepository
import com.gaboardi.table.repository.TableRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class TableModule {
    @Binds
    abstract fun bindRemoteDataSource(
        implementation: RemoteTableDataSourceImpl
    ): RemoteTableDataSource

    @Binds
    abstract fun bindLocalDataSource(
        implementation: LocalTableDataSourceImpl
    ): LocalTableDataSource

    @Binds
    abstract fun bindRepository(
        implementation: TableRepositoryImpl
    ): TableRepository
}