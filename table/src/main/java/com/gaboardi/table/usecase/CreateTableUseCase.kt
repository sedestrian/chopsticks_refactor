package com.gaboardi.table.usecase

import com.gaboardi.table.repository.TableRepository
import javax.inject.Inject

class CreateTableUseCase @Inject constructor(
    private val tableRepository: TableRepository
) {
    fun execute(name: String, restaurantId: String, isTable: Boolean) = tableRepository.addTable(name, restaurantId, isTable)
}