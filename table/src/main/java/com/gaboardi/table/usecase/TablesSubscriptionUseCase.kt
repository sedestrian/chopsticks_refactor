package com.gaboardi.table.usecase

import com.gaboardi.table.repository.TableRepository
import javax.inject.Inject

class TablesSubscriptionUseCase @Inject constructor(
    private val tableRepository: TableRepository
){
    operator fun invoke() = tableRepository.startTablesSubscription()
}