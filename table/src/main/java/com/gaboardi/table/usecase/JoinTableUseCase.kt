package com.gaboardi.table.usecase

import com.gaboardi.table.repository.TableRepository
import javax.inject.Inject

class JoinTableUseCase @Inject constructor(
    private val tableRepository: TableRepository
) {
    operator fun invoke(tableCode: String, restaurantId: String) = tableRepository.joinTable(tableCode, restaurantId)
}