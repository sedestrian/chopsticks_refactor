package com.gaboardi.table.usecase

import com.gaboardi.table.repository.TableRepository
import javax.inject.Inject

class GetTableFromCodeUseCase @Inject constructor(
    private val tableRepository: TableRepository
) {
    fun execute(code: String) = tableRepository.observeTableWithCode(code)
}