package com.gaboardi.table.usecase

import com.gaboardi.table.repository.TableRepository
import javax.inject.Inject

class ObserveTableUseCase @Inject constructor(
    private val tableRepository: TableRepository
) {
    fun execute(tableId: String) = tableRepository.observeTable(tableId)
}