package com.gaboardi.table.usecase

import com.gaboardi.table.repository.TableRepository
import javax.inject.Inject

class SubscribeToTableWithCodeUseCase @Inject constructor(
    private val tableRepository: TableRepository
) {
    fun execute(tableCode: String) = tableRepository.subscribeToTableWithCode(tableCode)
}