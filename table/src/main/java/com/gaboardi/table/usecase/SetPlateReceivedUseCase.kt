package com.gaboardi.table.usecase

import com.gaboardi.table.repository.TableRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class SetPlateReceivedUseCase @Inject constructor(
    private val tableRepository: TableRepository
) {
    @ExperimentalCoroutinesApi
    suspend fun execute(tableId: String, plateId: String, amount: Int) {
        tableRepository.setPlateReceived(tableId, plateId, amount)
    }
}