package com.gaboardi.table.datasource.local

import com.gaboardi.table.model.api.TableResult
import com.gaboardi.table.model.ui.Table
import kotlinx.coroutines.flow.Flow

interface LocalTableDataSource {
    suspend fun insertTables(data: List<TableResult>)
    suspend fun insertTable(data: TableResult)
    fun observeTable(tableId: String): Flow<Table?>
    fun observeTableWithCode(tableCode: String): Flow<Table?>
    suspend fun getTableFromCode(tableCode: String): Table?
}