package com.gaboardi.table.datasource.remote

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.table.model.api.TableRequest
import com.gaboardi.table.model.api.TableResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface RemoteTableDataSource {
    suspend fun addTable(table: TableRequest): ApiResponse<String>
    fun startTableSubscription(tableId: String): Flow<ApiResponse<TableResult>>
    fun subscribeToTableWithCode(tableCode: String): Flow<ApiResponse<TableResult?>>
    suspend fun joinTable(tableId: String, userId: String, orderId: String): ApiResponse<Unit>
    fun startTablesSubscription(): Flow<ApiResponse<List<TableResult>>>
    suspend fun setPlateReceived(
        tableId: String,
        userId: String,
        plateId: String,
        amount: Int
    ): ApiResponse<Unit>
}