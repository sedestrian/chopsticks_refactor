package com.gaboardi.table.datasource.local

import com.gaboardi.table.converter.toInsertData
import com.gaboardi.table.converter.toUi
import com.gaboardi.table.dao.TableDao
import com.gaboardi.table.model.api.TableResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LocalTableDataSourceImpl @Inject constructor(
    private val tableDao: TableDao
) : LocalTableDataSource {
    override fun observeTable(tableId: String) = tableDao.observeTable(tableId).map { it?.toUi() }

    override fun observeTableWithCode(tableCode: String) =
        tableDao.observeTableWithCode(tableCode).map { it?.toUi() }

    override suspend fun getTableFromCode(tableCode: String) =
        withContext(Dispatchers.IO) { tableDao.getTableFromCode(tableCode)?.toUi() }

    override suspend fun insertTables(data: List<TableResult>) = withContext(Dispatchers.IO) {
        val insertData = data.map { it.toInsertData() }
        tableDao.insertTables(insertData)
    }

    override suspend fun insertTable(data: TableResult) = withContext(Dispatchers.IO) {
        val insertData = data.toInsertData()
        tableDao.insertTable(insertData)
    }
}