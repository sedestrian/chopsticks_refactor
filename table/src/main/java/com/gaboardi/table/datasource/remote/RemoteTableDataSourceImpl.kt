package com.gaboardi.table.datasource.remote

import android.net.Uri
import com.gaboardi.common.extensions.fromJson
import com.gaboardi.common.extensions.toJson
import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.Collections
import com.gaboardi.common.model.DatabaseField
import com.gaboardi.table.model.api.TableRequest
import com.gaboardi.table.model.api.TableResult
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.ShortDynamicLink
import com.google.firebase.dynamiclinks.ktx.androidParameters
import com.google.firebase.dynamiclinks.ktx.iosParameters
import com.google.firebase.dynamiclinks.ktx.shortLinkAsync
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import java.util.*
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

@ExperimentalCoroutinesApi
class RemoteTableDataSourceImpl @Inject constructor(
    private val firestore: FirebaseFirestore,
    private val firebaseUser: FirebaseUser?,
    private val dynamicLinks: FirebaseDynamicLinks
) : RemoteTableDataSource {
    override suspend fun joinTable(
        tableId: String,
        userId: String,
        orderId: String
    ): ApiResponse<Unit> {
        return try {
            /*val table = firestore.collection(Collections.Tables).document(tableId)
            firestore.runBatch {
                it.update(table, DatabaseField.Table.Users, FieldValue.arrayUnion(userId))
                it.update(table, DatabaseField.Table.Orders, FieldValue.arrayUnion(
                    TableOrderRequest(userId, orderId)
                ))
            }.await()*/
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    override suspend fun addTable(table: TableRequest): ApiResponse<String> {
        return try {
            val tableCollection = firestore.collection(Collections.Tables)
            val result = tableCollection.add(table).await()

            insertCode(result.id)
            ApiResponse.success(result.id)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage ?: "Api error")
        }
    }

    private suspend fun insertCode(id: String) {
        val tableCollection = firestore.collection(Collections.Tables)

        var code: String
        do {
            code = generateCode(id)
            val sameCode = tableCollection.whereEqualTo("table.code", code).get().await()
        } while (sameCode.size() > 0)
        val dynamicLink = createDynamicLink(code)

        firestore.runBatch { batch ->
            batch.update(tableCollection.document(id), DatabaseField.Table.Code, code)
            dynamicLink?.let {
                batch.update(
                    tableCollection.document(id),
                    DatabaseField.Table.DynamicLink,
                    it.toString()
                )
            }
        }.await()
    }

    override suspend fun setPlateReceived(
        tableId: String,
        userId: String,
        plateId: String,
        amount: Int
    ): ApiResponse<Unit> {
        return try {
            val transaction = firestore.runTransaction {
                val currentTable = firestore.collection(Collections.Tables).document(tableId)
                val tableResult = it.get(currentTable)
                val table = tableResult.data.toJson().fromJson<TableResult>()
                val result = table?.copy(
                    id = tableResult.id,
                    users = table.users.map { user ->
                        if (user.userId == userId)
                            user.copy(
                                plates = user.plates?.map { plate ->
                                    if (plate.plateId == plateId) plate.copy(received = amount) else plate
                                }
                            )
                        else
                            user
                    }
                )
                if (result != null)
                    it.set(currentTable, result)
                null
            }
            coroutineContext.ensureActive()
            transaction.await()
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    private suspend fun createDynamicLink(code: String): Uri? {
        return dynamicLinks.shortLinkAsync(ShortDynamicLink.Suffix.SHORT) {
            link = Uri.parse("https://chopsticks.com/join/$code")
            domainUriPrefix = "https://chopsticks.page.link"
            androidParameters("com.gaboardi.chopsticks") { }
            iosParameters("com.gaboardi.chopsticks") { }
        }.await().shortLink
    }

    private fun generateCode(id: String): String {
        val uuid = UUID.nameUUIDFromBytes(id.toByteArray())
        val cleanUUID = uuid.toString().replace("-", "", true)
        return cleanUUID
            .toCharArray()
            .toList()
            .windowed(8)
            .reduce { acc, list ->
                acc.zip(list) { total, current ->
                    constrainToAlphanumeric(
                        total.toInt() xor current.toInt()
                    ).toChar()
                }
            }.joinToString("")
            .toUpperCase()
    }

    private fun constrainToAlphanumeric(number: Int): Int {
        val char = number.toChar()
        return if (char.isLetterOrDigit())
            number
        else when {
            number < 48 -> constrainToAlphanumeric(48 + number)
            number > 122 -> constrainToAlphanumeric(48 + (number - 123))
            number in 58..64 -> constrainToAlphanumeric(65 + (number - 58))
            number in 91..96 -> constrainToAlphanumeric(97 + (number - 91))
            else -> number
        }
    }

    override fun startTableSubscription(tableId: String) =
        callbackFlow<ApiResponse<TableResult>> {
            val tablesCollection = firestore.collection(Collections.Tables)
            val tableDocument = tablesCollection.document(tableId)
            val subscription = tableDocument.addSnapshotListener { value, error ->
                when {
                    error != null -> trySendBlocking(ApiResponse.error(error.localizedMessage))
                    value != null -> {
                        val table = value.data.toJson().fromJson<TableResult>()
                        val data = table.copy(id = value.id)
                        trySendBlocking(ApiResponse.success(data))
                    }
                }
            }
            awaitClose { subscription.remove() }
        }

    override fun startTablesSubscription() = callbackFlow<ApiResponse<List<TableResult>>> {
        firebaseUser?.let { user ->
            val tablesCollection = firestore.collection(Collections.Tables)
            val tablesQuery =
                tablesCollection.whereArrayContains(DatabaseField.Table.UserIds, user.uid)
            val subscription = tablesQuery.addSnapshotListener { value, error ->
                when {
                    error != null -> trySendBlocking(ApiResponse.error(error.localizedMessage))
                    value != null -> {
                        val tables = value.documents.map {
                            val table = it.data.toJson().fromJson<TableResult>()
                            table.copy(id = it.id)
                        }
                        trySendBlocking(ApiResponse.success(tables))
                    }
                }
            }
            awaitClose { subscription.remove() }
        }
    }

    override fun subscribeToTableWithCode(tableCode: String) =
        callbackFlow<ApiResponse<TableResult?>> {
            val tablesCollection = firestore.collection(Collections.Tables)
            val tableQuery =
                tablesCollection.whereEqualTo(DatabaseField.Table.Code, tableCode).limit(1)
            val subscription = tableQuery.addSnapshotListener { value, error ->
                when {
                    error != null -> trySendBlocking(ApiResponse.error(error.localizedMessage))
                    value != null -> {
                        val tables = value.documents.map {
                            val table = it.data.toJson().fromJson<TableResult>()
                            table.copy(id = it.id)
                        }
                        trySendBlocking(ApiResponse.success(tables.firstOrNull()))
                    }
                }
            }
            awaitClose { subscription.remove() }
        }
}