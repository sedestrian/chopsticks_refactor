package com.gaboardi.table.dao

import androidx.room.*
import com.gaboardi.table.model.db.*
import kotlinx.coroutines.flow.Flow

@Dao
abstract class TableDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertTable(table: TableEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertTablesDb(tables: List<TableEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertTableUser(user: TableUserEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertTableUsersDb(tableUsers: List<TableUserEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertTablePlates(tablePlates: List<TablePlateEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun createJunction(junctionEntry: TableRestaurantJunction)

    @Transaction
    open suspend fun insertTables(data: List<TableInsert>){
        val tables = data.map { it.table }
        val users = data.flatMap { it.users }

        insertTablesDb(tables)
        users.forEach {
            insertTableUser(it.user)
            val junction = TableRestaurantJunction(it.user.tableId, it.user.restaurantId)
            createJunction(junction)
            insertTablePlates(it.plates)
        }
    }

    @Transaction
    open suspend fun insertTable(data: TableInsert){
        insertTable(data.table)
        data.users.forEach {
            insertTableUser(it.user)
            insertTablePlates(it.plates)
        }
    }

    @Transaction
    @Query("SELECT * FROM tabledata WHERE id = :tableId LIMIT 1")
    abstract fun observeTable(tableId: String): Flow<TableData?>

    @Transaction
    @Query("SELECT * FROM tabledata WHERE code = :tableCode LIMIT 1")
    abstract fun observeTableWithCode(tableCode: String): Flow<TableData?>

    @Transaction
    @Query("SELECT * FROM tabledata WHERE code = :tableCode LIMIT 1")
    abstract suspend fun getTableFromCode(tableCode: String): TableData?
}