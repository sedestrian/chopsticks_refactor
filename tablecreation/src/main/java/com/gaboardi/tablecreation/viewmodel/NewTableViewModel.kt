package com.gaboardi.tablecreation.viewmodel

import androidx.lifecycle.ViewModel
import com.gaboardi.common.model.State
import com.gaboardi.table.usecase.CreateTableUseCase
import com.gaboardi.tablecreation.mvi.TableCreationSideEffect
import com.gaboardi.tablecreation.mvi.TableCreationState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class NewTableViewModel @Inject constructor(
    private val createTableUseCase: CreateTableUseCase,
) : ViewModel(), ContainerHost<TableCreationState, TableCreationSideEffect> {

    override val container: Container<TableCreationState, TableCreationSideEffect> = container(
        TableCreationState()
    )

    fun createTable(restaurantId: String) = intent {
        val name = state.name
        if (name != null)
            createTableUseCase.execute(name, restaurantId, state.isTable).collect {
                when (it) {
                    State.Loading -> reduce { state }
                    is State.Success -> {
                        it.id?.let { tableId ->
                            postSideEffect(TableCreationSideEffect.TableCreated(tableId))
                        }
                    }
                    is State.Error -> reduce { state }
                }
            }
    }

    fun nameChanged(name: String) = intent {
        reduce { state.copy(name = name) }
    }

    fun typeChanged(isTable: Boolean) = intent {
        reduce { state.copy(isTable = isTable) }
    }

    /*private lateinit var restaurantId: String
    var isTable = ObservableBoolean(false)

    private val _loading: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val loading: LiveData<Event<Boolean>> = _loading

    private val _error: MutableLiveData<Event<ErrorMessage>> = MutableLiveData()
    val error: LiveData<Event<ErrorMessage>> = _error

    private val _created: MutableLiveData<Event<String>> = MutableLiveData()
    val created: LiveData<Event<String>> = _created

    private val _tableCreated: MutableLiveData<Event<String>> = MutableLiveData()
    val tableCreated: LiveData<Event<String>> = _tableCreated

    private val _tableError: MutableLiveData<Event<ErrorMessage>> = MutableLiveData()
    val tableError: LiveData<Event<ErrorMessage>> = _tableError

    fun init(restaurantId: String) {
        this.restaurantId = restaurantId
    }

    @ExperimentalCoroutinesApi
    fun create(name: String) {
        viewModelScope.launch {
            createEmptyOrderUseCase.create(name, restaurantId, Timestamp.now()).collect { state ->
                when (state) {
                    is State.Loading -> _loading.postValue(Event(true))
                    is State.Success -> {
                        state.id?.let {
                            if (!isTable.get()) {
                                _loading.postValue(Event(false))
                                _created.postValue(Event(it))
                            } else {
                                createTable(it, name)
                            }
                        } ?: run {
                            _loading.postValue(Event(false))
                            _error.postValue(Event(ErrorMessage.of(R.string.server_error)))
                        }
                    }
                    is State.Error -> {
                        _loading.postValue(Event(false))
                        _error.postValue(Event(state.message))
                    }
                }
            }
        }
    }

    suspend fun createTable(orderId: String, name: String) {
        createTableUseCase.execute(orderId, name).collect { state ->
            when (state) {
                State.Loading -> _loading.postValue(Event(true))
                is State.Success -> {
                    state.id?.let { tableId ->
                        setOrderTable(orderId, tableId)
                    }
                }
                is State.Error -> {
                    _loading.postValue(Event(false))
                    _tableError.postValue(Event(state.message))
                }
            }
        }
    }

    private suspend fun setOrderTable(orderId: String, tableId: String) {
        setOrderTableUseCase.execute(orderId, tableId).collect { state ->
            when (state) {
                State.Loading -> _loading.postValue(Event(true))
                is State.Success -> {
                    _loading.postValue(Event(false))
                    _tableCreated.postValue(Event(tableId))
                }
                is State.Error -> {
                    _loading.postValue(Event(false))
                    _tableError.postValue(Event(state.message))
                }
            }
        }
    }*/
}