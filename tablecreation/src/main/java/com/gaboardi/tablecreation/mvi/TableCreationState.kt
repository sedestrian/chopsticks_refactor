package com.gaboardi.tablecreation.mvi

data class TableCreationState(
    val name: String? = null,
    val isTable: Boolean = false
)