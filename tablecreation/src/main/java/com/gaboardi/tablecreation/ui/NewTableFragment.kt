package com.gaboardi.tablecreation.ui

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.annotation.RequiresApi
import androidx.core.view.*
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gaboardi.ordercreation.databinding.FragmentNewTableBinding
import com.gaboardi.tablecreation.mvi.TableCreationSideEffect
import com.gaboardi.tablecreation.mvi.TableCreationState
import com.gaboardi.tablecreation.viewmodel.NewTableViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class NewTableFragment : Fragment() {
    private var wasVisible = false

    private val viewModel: NewTableViewModel by viewModels()
    private val args: NewTableFragmentArgs by navArgs()

    private lateinit var binding: FragmentNewTableBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewTableBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        lifecycleScope.launchWhenCreated {
            viewModel.container.stateFlow.collect {
                renderState(it)
            }
        }

        lifecycleScope.launchWhenCreated {
            viewModel.container.sideEffectFlow.collect { sideEffect ->
                when (sideEffect) {
                    is TableCreationSideEffect.TableCreated -> toDetail(sideEffect.tableId)
                }
            }
        }

        followUi()
    }

    private fun toDetail(tableId: String) {
        val action =
            NewTableFragmentDirections.actionNewOrderNameFragmentToOrderDetailFlow(
                tableId
            )
        findNavController().navigate(action)
    }

    private suspend fun renderState(state: TableCreationState) {
        binding.singleTable.isChecked = !state.isTable
        binding.groupTable.isChecked = state.isTable
    }

    private fun followUi() {
        binding.orderName.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.motionLayout.transitionToEnd()
            } else {
                binding.motionLayout.transitionToStart()
            }
        }

        binding.orderName.doAfterTextChanged {
            viewModel.nameChanged(it.toString())
        }

        binding.buttonNext.setOnClickListener {
            viewModel.createTable(args.restaurantId)
        }

        binding.singleTable.addOnCheckedListener {
            if (it)
                viewModel.typeChanged(true)
        }

        binding.groupTable.addOnCheckedListener {
            if (it)
                viewModel.typeChanged(false)
        }

        binding.backButton.setOnClickListener {
            findNavController().navigateUp()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            view?.setWindowInsetsAnimationCallback(imeCallback)
        }
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val sysWindow = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        view.updatePadding(top = sysWindow.top, bottom = sysWindow.bottom)
        if (!insets.isVisible(WindowInsetsCompat.Type.ime())) {
            if (wasVisible)
                binding.orderName.clearFocus()
        } else {
            wasVisible = true
        }
        insets
    }

    private val imeCallback = @RequiresApi(Build.VERSION_CODES.R)
    object : WindowInsetsAnimation.Callback(DISPATCH_MODE_CONTINUE_ON_SUBTREE) {
        override fun onProgress(
            insets: WindowInsets,
            runningAnimations: MutableList<WindowInsetsAnimation>
        ): WindowInsets {
            val keyboard = insets.getInsets(WindowInsetsCompat.Type.ime())
            binding.buttonNext.translationY =
                -(keyboard.bottom.toFloat() - binding.buttonNext.marginBottom).coerceAtLeast(0f)
            return insets
        }
    }
}