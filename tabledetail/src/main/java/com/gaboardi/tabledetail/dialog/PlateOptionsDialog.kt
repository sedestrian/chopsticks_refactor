package com.gaboardi.tabledetail.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaboardi.tabledetail.databinding.DialogPlateOptionsBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class PlateOptionsDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogPlateOptionsBinding

    private var onDeletePlate: (() -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogPlateOptionsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.deleteLayout.setOnClickListener {
            onDeletePlate?.invoke()
        }
    }

    fun setOnDeleteListener(listener: (() -> Unit)?) {
        onDeletePlate = listener
    }
}