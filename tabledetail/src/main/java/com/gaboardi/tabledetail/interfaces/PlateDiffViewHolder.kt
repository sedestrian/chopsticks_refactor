package com.gaboardi.tabledetail.interfaces

import com.gaboardi.plate.model.ui.PlateDiff
import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.tabledetail.callback.PlatesHandler

interface PlateDiffViewHolder {
    fun bindForChanges(diff: PlateDiff, plate: PlateUI.Plate, platesHandler: PlatesHandler)
}