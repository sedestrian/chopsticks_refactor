package com.gaboardi.tabledetail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.plate.model.ui.EmptyPlateType
import com.gaboardi.plate.model.ui.PlateDiff
import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.tabledetail.callback.PlatesHandler
import com.gaboardi.tabledetail.databinding.*
import com.gaboardi.tabledetail.interfaces.PlateDiffViewHolder
import com.gaboardi.tabledetail.viewholder.*

class PlatesAdapter(private val handler: PlatesHandler) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var inflater: LayoutInflater
    private val items: MutableList<PlateUI> = mutableListOf()

    private var onEmptyClickListener: ((type: EmptyPlateType) -> Unit)? = null

    override fun getItemViewType(position: Int): Int {
        return when (val item = items[position]) {
            is PlateUI.Header -> PlateState.HEADER.viewType
            is PlateUI.Plate -> PlateState.fromState(item.state).viewType
            is PlateUI.Empty -> PlateState.EMPTY.viewType
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (!this::inflater.isInitialized) inflater = LayoutInflater.from(parent.context)

        return when (PlateState.fromViewType(viewType)) {
            PlateState.LATER -> {
                val binding = TableLaterPlateItemBinding.inflate(inflater, parent, false)
                LaterPlateViewHolder(binding)
            }
            PlateState.ORDERED -> {
                val binding = TableOrderedPlateItemBinding.inflate(inflater, parent, false)
                OrderedPlateViewHolder(binding)
            }
            PlateState.RECEIVED -> {
                val binding = TablePlatesListItemBinding.inflate(inflater, parent, false)
                PlatesViewHolder(binding)
            }
            PlateState.HEADER -> {
                val binding = TablePlatesListHeaderBinding.inflate(inflater, parent, false)
                PlatesHeaderViewHolder(binding)
            }
            PlateState.EMPTY -> {
                val binding = TablePlatesListEmptyBinding.inflate(inflater, parent, false)
                EmptyPlatesViewHolder(binding)
            }
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LaterPlateViewHolder -> {
                val item = items[position] as PlateUI.Plate
                holder.bind(item, handler)
            }
            is OrderedPlateViewHolder -> {
                val item = items[position] as PlateUI.Plate
                holder.bind(item, handler)
            }
            is PlatesViewHolder -> {
                val item = items[position] as PlateUI.Plate
                holder.bind(item, handler)
            }
            is PlatesHeaderViewHolder -> {
                val item = items[position] as PlateUI.Header
                holder.bind(item)
            }
            is EmptyPlatesViewHolder -> {
                val item = items[position] as PlateUI.Empty
                holder.bind(item) {
                    onEmptyClickListener?.invoke(it)
                }
            }
        }
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty() || holder !is PlateDiffViewHolder) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            payloads.forEach {
                holder.bindForChanges(it as PlateDiff, items[position] as PlateUI.Plate, handler)
            }
        }
    }

    fun setItems(plates: List<PlateUI>) {
        val diffUtil = PlateDiffUtil(items, plates)
        val diff = DiffUtil.calculateDiff(diffUtil)
        diff.dispatchUpdatesTo(this)
        items.clear()
        items.addAll(plates)
    }
}