package com.gaboardi.tabledetail.adapter

import androidx.recyclerview.widget.DiffUtil
import com.gaboardi.plate.model.ui.PlateDiff
import com.gaboardi.plate.model.ui.PlateUI

class PlateDiffUtil(private val oldList: List<PlateUI>, private val newList: List<PlateUI>) :
    DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]
        return when (oldItem) {
            is PlateUI.Plate -> newItem is PlateUI.Plate && newItem.id == oldItem.id
            is PlateUI.Header -> newItem is PlateUI.Header && newItem.type == oldItem.type
            is PlateUI.Empty -> newItem is PlateUI.Empty && newItem.type == oldItem.type
        }
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]
        return when (oldItem) {
            is PlateUI.Plate -> {
                newItem is PlateUI.Plate &&
                        newItem.id == oldItem.id &&
                        newItem.code == oldItem.code &&
                        newItem.name == oldItem.name &&
                        newItem.tableId == oldItem.tableId &&
                        newItem.quantity == oldItem.quantity &&
                        newItem.received == oldItem.received &&
                        newItem.state == oldItem.state &&
                        newItem.tableQuantity == oldItem.tableQuantity &&
                        newItem.tableReceived == oldItem.tableReceived
            }
            is PlateUI.Header -> newItem is PlateUI.Header && newItem.type == oldItem.type && newItem.title == oldItem.title
            is PlateUI.Empty -> newItem is PlateUI.Empty && newItem.type == oldItem.type
        }
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        if (oldItem !is PlateUI.Plate || newItem !is PlateUI.Plate) return null

        val changes = PlateDiff(
            if (oldItem.name != newItem.name) newItem.name else null,
            if (oldItem.code != newItem.code) newItem.code else null,
            if (oldItem.quantity != newItem.quantity) newItem.quantity else null,
            if (oldItem.received != newItem.received) newItem.received else null,
            if (oldItem.state != newItem.state) newItem.state else null,
            if (oldItem.tableQuantity != newItem.tableQuantity) newItem.tableQuantity else null,
            if (oldItem.tableReceived != newItem.tableReceived) newItem.tableReceived else null
        )

        return if (changes.isEmpty()) null else changes
    }
}