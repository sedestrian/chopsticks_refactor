package com.gaboardi.tabledetail.viewholder

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.common.extensions.vibrate
import com.gaboardi.commonui.extensions.getColorAttribute
import com.gaboardi.commonui.extensions.setGone
import com.gaboardi.commonui.extensions.setVisible
import com.gaboardi.commonui.inputflilter.PlateCodeInputFilter
import com.gaboardi.plate.model.ui.PlateDiff
import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.tabledetail.R
import com.gaboardi.tabledetail.callback.PlatesHandler
import com.gaboardi.tabledetail.databinding.TableOrderedPlateItemBinding
import com.gaboardi.tabledetail.interfaces.PlateDiffViewHolder

class OrderedPlateViewHolder(val binding: TableOrderedPlateItemBinding) :
    RecyclerView.ViewHolder(binding.root), PlateDiffViewHolder {

    fun bind(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        binding.amountSeekbar.steps = plate.quantity
        binding.amountSeekbar.value = plate.received
        binding.amountSeekbar.setOnValueChangedListener { old, new ->
            binding.amountSeekbar.context.vibrate()
        }

        binding.amountSeekbar.setOnValueSelectedListener {
            platesHandler.onReceivedSet(plate.tableId, plate.id, it)
        }

        val tableQuantity = plate.tableQuantity
        val tableReceived = plate.tableReceived
        if(tableQuantity != null && tableReceived != null) {
            binding.tableProgress.steps = tableQuantity
            binding.tableProgress.value = tableReceived
        }

        binding.plusButton.setOnClickListener {
            binding.amountSeekbar.context.vibrate()
            binding.amountSeekbar.value = binding.amountSeekbar.value + 1
            platesHandler.onReceivedSet(plate.tableId, plate.id, binding.amountSeekbar.value)
        }

        binding.minusButton.setOnClickListener {
            binding.amountSeekbar.context.vibrate()
            binding.amountSeekbar.value = binding.amountSeekbar.value - 1
            platesHandler.onReceivedSet(plate.tableId, plate.id, binding.amountSeekbar.value)
        }
        binding.plateName.text = plate.name
        showCode(plate)
    }

    override fun bindForChanges(
        diff: PlateDiff,
        plate: PlateUI.Plate,
        platesHandler: PlatesHandler
    ) {
        if(diff.name != null) binding.plateName.text = diff.name
        diff.tableQuantity?.let { binding.tableProgress.steps = it }
        diff.tableReceived?.let { binding.tableProgress.value = it }
    }

    private fun showCode(plate: PlateUI.Plate) {
        if (plate.code.isNotEmpty()) {
            binding.codeFlow.setVisible()
            binding.codeDivider.setVisible()
            binding.plateCode.text = colorCodeText(
                plate.code,
                binding.root.context.getColorAttribute(R.attr.colorPrimary)
            )
        } else {
            binding.codeFlow.setGone()
            binding.codeDivider.setGone()
        }
    }

    private fun colorCodeText(code: String, letterColor: Int): CharSequence {
        val shouldSpan = code.any { it.isLetter() }
        val result = if (shouldSpan) {
            var editable = code
            val sections: MutableList<PlateCodeInputFilter.LetterSection> = mutableListOf()
            while (editable.any { it.isLetter() }) {
                val sectionStart = editable.indexOfFirst { it.isLetter() }
                val section = editable.substring(sectionStart, editable.length)
                val sectionEnd = if (section.any { !it.isLetter() })
                    section.indexOfFirst { !it.isLetter() }
                else
                    editable.length
                sections.add(PlateCodeInputFilter.LetterSection(sectionStart, sectionEnd))
                editable = editable.removeRange(sectionStart, sectionEnd)
            }
            val spannable = SpannableStringBuilder(code)
            sections.forEach {
                spannable.setSpan(
                    ForegroundColorSpan(letterColor),
                    it.start,
                    it.end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            spannable
        } else code
        return result
    }
}