package com.gaboardi.tabledetail.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.tabledetail.databinding.TablePlatesListHeaderBinding

class PlatesHeaderViewHolder(val binding: TablePlatesListHeaderBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(header: PlateUI.Header) {
        binding.title.text = binding.root.context.getString(header.title)
    }
}