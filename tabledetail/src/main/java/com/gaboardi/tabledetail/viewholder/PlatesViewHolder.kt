package com.gaboardi.tabledetail.viewholder

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.transition.TransitionManager
import androidx.core.view.isGone
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.commonui.extensions.getColorAttribute
import com.gaboardi.commonui.extensions.setGone
import com.gaboardi.commonui.extensions.setVisible
import com.gaboardi.commonui.inputflilter.PlateCodeInputFilter
import com.gaboardi.plate.model.ui.PlateDiff
import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.tabledetail.R
import com.gaboardi.tabledetail.callback.PlatesHandler
import com.gaboardi.tabledetail.databinding.TablePlatesListItemBinding
import com.gaboardi.tabledetail.interfaces.PlateDiffViewHolder

class PlatesViewHolder(val binding: TablePlatesListItemBinding) :
    RecyclerView.ViewHolder(binding.root), PlateDiffViewHolder {

    fun bind(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        TransitionManager.beginDelayedTransition(binding.constraint)
        val plateState = PlateState.fromState(plate.state)
        binding.plateName.text = plate.name
        if (plate.code.isNotEmpty()) {
            binding.codeFlow.setVisible()
            binding.codeDivider.setVisible()
            binding.plateCode.text = colorCodeText(
                plate.code,
                binding.root.context.getColorAttribute(R.attr.colorPrimary)
            )
        } else {
            binding.codeFlow.setGone()
            binding.codeDivider.setGone()
        }
        handleOptions(plate, platesHandler)
        when (plateState) {
            PlateState.ORDERED -> {
                handleOrdered(plate, platesHandler)
            }
            PlateState.LATER -> {
                handleLater(plate, platesHandler)
            }
            PlateState.RECEIVED -> {
                handleReceived(plate, platesHandler)
            }
        }
    }

    override fun bindForChanges(
        diff: PlateDiff,
        plate: PlateUI.Plate,
        platesHandler: PlatesHandler
    ) {
        if (diff.name != null) binding.plateName.text = diff.name

        val code = diff.code
        if (code != null) binding.plateCode.text =
            colorCodeText(code, binding.root.context.getColorAttribute(R.attr.colorPrimary))
        if (diff.state != null) {
            val state = PlateState.fromState(diff.state!!)
            when (state) {
                PlateState.ORDERED -> {
                    setOrderedUi()
                    binding.plateReceived.text = plate.received.toString()
                    setPlusMinusOrdered(plate, platesHandler)
                    stateToLater(plate, platesHandler)
                }
                PlateState.LATER -> {
                    setLaterUi()
                    setPlusMinusLater(plate, platesHandler)
                    stateToOrdered(plate, platesHandler)
                }
                else -> {
                    setReceivedUi()
                    binding.plateReceived.text = plate.received.toString()
                    setPlusMinusReceived(plate, platesHandler)
                }
            }
        }
        if (diff.quantity != null) {
            binding.plateQuantity.text = diff.quantity.toString()
            setPlusMinusForState(plate, platesHandler)
        }
        if (diff.received != null) {
            binding.plateReceived.text = diff.received.toString()
            setPlusMinusForState(plate, platesHandler)
        }
    }

    private fun setPlusMinusForState(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        val state = PlateState.fromState(plate.state)
        when (state) {
            PlateState.ORDERED -> {
                setPlusMinusOrdered(plate, platesHandler)
            }
            PlateState.LATER -> {
                setPlusMinusLater(plate, platesHandler)
            }
            else -> {
                setPlusMinusReceived(plate, platesHandler)
            }
        }
    }

    private fun handleOptions(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        binding.root.setOnLongClickListener {
            platesHandler.onLongClick(plate)
            true
        }
    }

    private fun setOrderedLaterUi() {
        binding.plusButton.setImageResource(R.drawable.plus)
        binding.minusButton.setImageResource(R.drawable.minus)
        binding.stateIcon.setVisible()
        binding.amountLayout.setBackgroundResource(R.drawable.outline_red)
    }

    private fun handleOrdered(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        setOrderedUi()
        setPlusMinusOrdered(plate, platesHandler)
        setPlateQuantityAndReceived(plate)
        stateToLater(plate, platesHandler)
    }

    private fun handleLater(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        setLaterUi()
        setPlusMinusLater(plate, platesHandler)
        setPlateQuantity(plate)
        stateToOrdered(plate, platesHandler)
    }

    private fun handleReceived(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        setReceivedUi()
        setPlusMinusReceived(plate, platesHandler)
        setPlateQuantityAndReceived(plate)
    }

    private fun setOrderedUi() {
        setOrderedLaterUi()
        binding.stateIcon.setImageResource(R.drawable.downgrade)
        binding.amountDivider.setVisible()
        binding.plateReceived.setVisible()
    }

    private fun setLaterUi() {
        setOrderedLaterUi()
        binding.stateIcon.setImageResource(R.drawable.upgrade)
        binding.amountDivider.setGone()
        binding.plateReceived.setGone()
    }

    private fun setReceivedUi() {
        binding.plusButton.setImageResource(R.drawable.low_emphasys_plus)
        binding.minusButton.setImageResource(R.drawable.low_emphasys_minus)
        binding.amountLayout.setBackgroundResource(R.drawable.outline_black)
        binding.amountDivider.setVisible()
        binding.plateReceived.setVisible()
        binding.stateIcon.isGone = true
    }

    private fun stateToLater(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        binding.stateIcon.setOnClickListener {
            platesHandler.changeGroup(plate.id, PlateState.LATER)
        }
    }

    private fun stateToOrdered(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        binding.stateIcon.setOnClickListener {
            platesHandler.changeGroup(plate.id, PlateState.ORDERED)
        }
    }

    private fun setPlusMinusOrdered(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        binding.plusButton.setOnClickListener {
            if (plate.received + 1 <= plate.quantity)
                platesHandler.onAddReceived(plate.id)
        }
        binding.minusButton.setOnClickListener {
            if (plate.received - 1 >= 0)
                platesHandler.onRemoveReceived(plate.id)
        }
    }

    private fun setPlusMinusLater(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        binding.plusButton.setOnClickListener {
            platesHandler.onAddQuantity(plate.id)
        }
        binding.minusButton.setOnClickListener {
            if (plate.quantity - 1 > 0)
                platesHandler.onRemoveQuantity(plate.id)
        }
    }

    private fun setPlusMinusReceived(plate: PlateUI.Plate, platesHandler: PlatesHandler) {
        binding.plusButton.setOnClickListener {
            platesHandler.onAddReceived(plate.id)
        }
        binding.minusButton.setOnClickListener {
            if (plate.received - 1 >= 0)
                platesHandler.onRemoveReceived(plate.id)
        }
    }

    private fun setPlateQuantity(plate: PlateUI.Plate) {
        binding.plateQuantity.text = plate.quantity.toString()
    }

    private fun setPlateQuantityAndReceived(plate: PlateUI.Plate) {
        binding.plateReceived.text = plate.received.toString()
        binding.plateQuantity.text = plate.quantity.toString()
    }

    private fun colorCodeText(code: String, letterColor: Int): CharSequence {
        val shouldSpan = code.any { it.isLetter() }
        val result = if (shouldSpan) {
            var editable = code
            val sections: MutableList<PlateCodeInputFilter.LetterSection> = mutableListOf()
            while (editable.any { it.isLetter() }) {
                val sectionStart = editable.indexOfFirst { it.isLetter() }
                val section = editable.substring(sectionStart, editable.length)
                val sectionEnd = if (section.any { !it.isLetter() })
                    section.indexOfFirst { !it.isLetter() }
                else
                    editable.length
                sections.add(PlateCodeInputFilter.LetterSection(sectionStart, sectionEnd))
                editable = editable.removeRange(sectionStart, sectionEnd)
            }
            val spannable = SpannableStringBuilder(code)
            sections.forEach {
                spannable.setSpan(
                    ForegroundColorSpan(letterColor),
                    it.start,
                    it.end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            spannable
        } else code
        return result
    }
}