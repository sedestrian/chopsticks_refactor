package com.gaboardi.tabledetail.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.plate.model.ui.EmptyPlateType
import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.tabledetail.databinding.TablePlatesListEmptyBinding

class EmptyPlatesViewHolder(val binding: TablePlatesListEmptyBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(empty: PlateUI.Empty, onClick: ((type: EmptyPlateType) -> Unit)?) {
        binding.constraintLayout.setOnClickListener {
            onClick?.invoke(empty.type)
        }
    }
}