package com.gaboardi.tabledetail.mvi

import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.table.model.ui.Table
import com.gaboardi.table.model.ui.TableUser

data class TableDetailState(
    val table: Table? = null,
    val people: List<TableUser> = listOf(),
    val userPlates: List<PlateUI> = listOf()
)