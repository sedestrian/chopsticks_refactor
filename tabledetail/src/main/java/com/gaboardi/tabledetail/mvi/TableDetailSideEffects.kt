package com.gaboardi.tabledetail.mvi

sealed class TableDetailSideEffects {
    data class ShowQrCode(val tableId: String): TableDetailSideEffects()
    data class ShowRecap(val tableId: String): TableDetailSideEffects()
    data class AddPlate(val orderId: String): TableDetailSideEffects()
}