package com.gaboardi.tabledetail.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gaboardi.common.extensions.dp
import com.gaboardi.commonui.utility.SpacingItemDecoration
import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.model.ui.PlateUI
import com.gaboardi.table.model.ui.Table
import com.gaboardi.tabledetail.R
import com.gaboardi.tabledetail.adapter.PlatesAdapter
import com.gaboardi.tabledetail.callback.PlatesHandler
import com.gaboardi.tabledetail.databinding.FragmentTableBinding
import com.gaboardi.tabledetail.dialog.PlateOptionsDialog
import com.gaboardi.tabledetail.mvi.TableDetailSideEffects
import com.gaboardi.tabledetail.mvi.TableDetailState
import com.gaboardi.tabledetail.viewmodel.TableViewModel
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import javax.inject.Inject

@AndroidEntryPoint
class TableFragment : Fragment() {
    private val args: TableFragmentArgs by navArgs()
    private val viewModel: TableViewModel by viewModels()
    private lateinit var binding: FragmentTableBinding
    private lateinit var adapter: PlatesAdapter

    var firebaseUser: FirebaseUser? = null
        @Inject set

    @ExperimentalCoroutinesApi
    val plateHandler: PlatesHandler = object : PlatesHandler {
        override fun onAddQuantity(id: String) {
            viewModel.addPlateQuantity(id)
        }

        override fun onAddReceived(id: String) {
            viewModel.addPlateReceived(id)
        }

        override fun onRemoveQuantity(id: String) {
            viewModel.removePlateQuantity(id)
        }

        override fun onRemoveReceived(id: String) {
            viewModel.removePlateReceived(id)
        }

        override fun onQuantitySet(id: String, amount: Int) {
            viewModel.setPlateQuantity(id, amount)
        }

        override fun onReceivedSet(tableId: String, id: String, amount: Int) {
            viewModel.setPlateReceived(tableId, id, amount)
        }

        override fun changeGroup(id: String, state: PlateState) {
            viewModel.changePlateGroup(id, state)
        }

        override fun onLongClick(plate: PlateUI.Plate) {
            val dialog = PlateOptionsDialog()
            dialog.setOnDeleteListener {
                viewModel.deletePlate(plate.id)
            }
            dialog.show(childFragmentManager, "")
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTableBinding.inflate(inflater, container, false)
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        setupToolbar()
        setupPlatesRecycler()

        listen()

        lifecycleScope.launchWhenStarted {
            viewModel.container.stateFlow.collect {
                renderToolbar(it)
                renderUserPlates(it)
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.container.sideEffectFlow.collect { sideEffect ->
                when (sideEffect) {
                    is TableDetailSideEffects.ShowQrCode -> showQrCode(sideEffect.tableId)
                    is TableDetailSideEffects.ShowRecap -> TODO()
                    is TableDetailSideEffects.AddPlate -> addPlate(sideEffect.orderId)
                }
            }
        }

        viewModel.loadTable(args.tableId)

        binding.root.doOnLayout {
            binding.bottomAppBar.performHide()
        }
    }

    private suspend fun renderToolbar(state: TableDetailState) {
        state.table?.let { table ->
            binding.toolbar.title = table.name
            if (table.isTable)
                subtitleTable(table)
            else
                subtitleDate(table)
        }
    }

    private fun subtitleDate(table: Table){
        binding.toolbar.subtitle = table.date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM))
    }

    private fun subtitleTable(table: Table) {
        val people = table.users.size
        val text =
            if (people > 1) R.string.table_people_subtitle else R.string.table_person_subtitle
        binding.toolbar.subtitle = getString(text, people)
    }

    private suspend fun renderUserPlates(state: TableDetailState) {
        adapter.setItems(state.userPlates)
    }

    @ExperimentalCoroutinesApi
    private fun setupPlatesRecycler() {
        adapter = PlatesAdapter(plateHandler)
        binding.platesRecycler.layoutManager = LinearLayoutManager(requireContext())
        binding.platesRecycler.addItemDecoration(SpacingItemDecoration(0.dp, 1.dp))
        binding.platesRecycler.adapter = adapter
    }

    private fun setupToolbar() {
        binding.toolbar.inflateMenu(R.menu.order_options_menu)
        binding.toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.recap -> {
                    viewModel.recapClicked()
                }
                R.id.qr_code -> {
                    viewModel.qrClicked()
                }
            }
            true
        }
    }

    private fun showQrCode(tableId: String) {
        val action = TableFragmentDirections.actionTableFragmentToTableInviteFlow(tableId, false)
        findNavController().navigate(action)
    }

    private fun addPlate(orderId: String) {
        val action = TableFragmentDirections.actionTableFragmentToPlateChoiceCreationFlow(orderId)
        findNavController().navigate(action)
    }

    private fun listen() {
        binding.addPlateButton.setOnClickListener {
            viewModel.addPlateClicked()
        }

        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val window = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        view.updatePadding(top = window.top)
        binding.addPlateButton.doOnLayout {
            binding.platesRecycler.updatePadding(bottom = window.bottom + it.height + 16.dp)
        }
        insets
    }
}