package com.gaboardi.tabledetail.viewmodel

import androidx.lifecycle.ViewModel
import com.gaboardi.chopsticks.order.usecase.*
import com.gaboardi.plate.model.ui.PlateState
import com.gaboardi.plate.usecase.AddQuantityUseCase
import com.gaboardi.plate.usecase.SetQuantityUseCase
import com.gaboardi.table.extensions.processPlates
import com.gaboardi.table.usecase.DownloadTableDataUseCase
import com.gaboardi.table.usecase.ObserveTableUseCase
import com.gaboardi.table.usecase.SetPlateReceivedUseCase
import com.gaboardi.tabledetail.mvi.TableDetailSideEffects
import com.gaboardi.tabledetail.mvi.TableDetailState
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.coroutines.transformFlow
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.syntax.strict.orbit
import org.orbitmvi.orbit.syntax.strict.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class TableViewModel @Inject constructor(
    private val observeTableUseCase: ObserveTableUseCase,
    private val downloadTableDataUseCase: DownloadTableDataUseCase,
    private val addQuantityUseCase: AddQuantityUseCase,
    private val subtractQuantityUseCase: SubtractQuantityUseCase,
    private val addReceivedUseCase: AddReceivedUseCase,
    private val subtractReceivedUseCase: SubtractReceivedUseCase,
    private val updatePlateStateUseCase: UpdatePlateStateUseCase,
    private val deletePlateUseCase: DeletePlateUseCase,
    private val setReceivedUseCase: SetPlateReceivedUseCase,
    private val setQuantityUseCase: SetQuantityUseCase,
    private val firebaseUser: FirebaseUser?
) : ViewModel(), ContainerHost<TableDetailState, TableDetailSideEffects> {
    private var receivedJob: Job? = null

    override val container: Container<TableDetailState, TableDetailSideEffects> = container(
        TableDetailState()
    )

    fun loadTable(tableId: String) = intent {
        observeTableUseCase.execute(tableId).collect {
            reduce {
                val plates = it?.users?.firstOrNull { it.userId == firebaseUser?.uid }?.plates
                    ?: listOf()
                val allPlates = it?.users?.flatMap { it.plates } ?: listOf()
                state.copy(
                    table = it,
                    people = it?.users ?: listOf(),
                    userPlates = processPlates(plates, allPlates)
                )
            }
        }
    }

    fun addPlateQuantity(plateId: String) = intent {
        addQuantityUseCase.execute(plateId)
    }

    fun addPlateReceived(plateId: String) = intent {
        addReceivedUseCase.execute(plateId)
    }

    fun removePlateQuantity(plateId: String) = intent {
        subtractQuantityUseCase.execute(plateId)
    }

    fun removePlateReceived(plateId: String) = intent {
        subtractReceivedUseCase.execute(plateId)
    }

    fun setPlateQuantity(plateId: String, amount: Int) = intent {
        setQuantityUseCase.execute(plateId, amount)
    }

    fun setPlateReceived(tableId: String, plateId: String, amount: Int) = intent {
        setReceivedUseCase.execute(tableId, plateId, amount)
    }

    fun changePlateGroup(plateId: String, state: PlateState) = intent {
        updatePlateStateUseCase.setState(plateId, state)
    }

    fun deletePlate(plateId: String) = orbit {
        transformFlow {
            deletePlateUseCase.delete(plateId)
        }.reduce {
            state.copy()
        }
    }

    fun qrClicked() = intent {
        state.table?.id?.let { tableId ->
            postSideEffect(TableDetailSideEffects.ShowQrCode(tableId))
        }
    }

    fun recapClicked() = intent {
        state.table?.id?.let { tableId ->
            postSideEffect(TableDetailSideEffects.ShowRecap(tableId))
        }
    }

    fun addPlateClicked() = intent {
        /*state.userOrder?.id?.let { orderId ->
            postSideEffect(TableDetailSideEffects.AddPlate(orderId))
        }*/
    }
}