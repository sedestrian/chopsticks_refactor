package com.gaboardi.restaurantcreation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.restaurantcreation.viewholder.AddPriceVH
import com.gaboardi.restaurantcreation.viewholder.PriceVH
import com.gaboardi.restaurant.model.ui.PriceUI
import com.gaboardi.restaurantcreation.databinding.AddPriceItemBinding
import com.gaboardi.restaurantcreation.databinding.PriceItemBinding

class AddPriceAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var inflater: LayoutInflater
    private val items: MutableList<PriceUI> = mutableListOf()
    private var onAddItemClicked: (() -> Unit)? = null
    private var onPriceClick: ((price: PriceUI) -> Unit)? = null

    override fun getItemViewType(position: Int): Int {
        return if (items.size < 4)
            if (position == 0)
                ViewType.AddPriceItem.id
            else
                ViewType.PriceItem.id
        else
            ViewType.PriceItem.id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (!this::inflater.isInitialized) inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ViewType.AddPriceItem.id -> {
                val binding = AddPriceItemBinding.inflate(inflater, parent, false)
                AddPriceVH(binding)
            }
            ViewType.PriceItem.id -> {
                val binding = PriceItemBinding.inflate(inflater, parent, false)
                PriceVH(binding)
            }
            else -> {
                val binding = AddPriceItemBinding.inflate(inflater, parent, false)
                AddPriceVH(binding)
            }
        }
    }

    private fun getItemPosition(position: Int): Int {
        return if (items.size < 4) position - 1 else position
    }

    override fun getItemCount(): Int = if (items.size < 4) items.size + 1 else items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val newPosition = getItemPosition(position)
        when (holder) {
            is PriceVH -> {
                holder.bind(items[newPosition], onPriceClick)
            }
            is AddPriceVH -> {
                holder.bind {
                    onAddItemClicked?.invoke()
                }
            }
        }
    }

    fun addPriceClicked(listener: (() -> Unit)?) {
        onAddItemClicked = listener
    }

    fun priceClicked(listener: ((price: PriceUI) -> Unit)?) {
        onPriceClick = listener
    }

    fun setPrices(prices: List<PriceUI>) {
        /*val diffUtil = AddPriceDiffUtil(items, prices)
        val diff = DiffUtil.calculateDiff(diffUtil)
        diff.dispatchUpdatesTo(this)*/
        items.clear()
        items.addAll(prices)
        notifyDataSetChanged()
    }

    enum class ViewType(val id: Int) {
        AddPriceItem(0),
        PriceItem(1)
    }
}