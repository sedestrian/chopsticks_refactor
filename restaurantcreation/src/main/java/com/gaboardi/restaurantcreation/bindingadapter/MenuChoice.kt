package com.gaboardi.restaurantcreation.bindingadapter

import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.gaboardi.commonui.view.MenuChoiceView

@BindingAdapter("menu_chosen")
fun bindChoice(view: MenuChoiceView, boolean: Boolean?) {
    if (boolean != null)
        view.isChecked = boolean
}

@InverseBindingAdapter(attribute = "menu_chosen")
fun onChosen(view: MenuChoiceView): Boolean {
    return !view.isChecked
}

@BindingAdapter("menu_chosenAttrChanged")
fun setListeners(
    view: MenuChoiceView,
    attrChange: InverseBindingListener
) {
    view.addOnCheckedListener {
        attrChange.onChange()
    }
}