package com.gaboardi.restaurantcreation.bindingadapter

import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.gaboardi.commonui.view.InlineChoiceView

@BindingAdapter("enabled")
fun bindEnabled(view: InlineChoiceView, boolean: Boolean) {
    view.isEnabled = boolean
}

@BindingAdapter("chosen")
fun bindChoice(view: InlineChoiceView, boolean: Boolean?) {
    if (boolean != null)
        view.isChecked = boolean
}

@InverseBindingAdapter(attribute = "chosen")
fun onChosen(view: InlineChoiceView): Boolean {
    return !view.isChecked
}

@BindingAdapter("chosenAttrChanged")
fun setListeners(
    view: InlineChoiceView,
    attrChange: InverseBindingListener
) {
    view.addOnCheckedListener {
        attrChange.onChange()
    }
}