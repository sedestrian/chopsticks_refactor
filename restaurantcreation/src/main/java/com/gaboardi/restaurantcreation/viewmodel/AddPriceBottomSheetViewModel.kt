package com.gaboardi.restaurantcreation.viewmodel

import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.gaboardi.restaurant.model.ui.PriceUI
import com.gaboardi.restaurantcreation.BR
import com.gaboardi.restaurantcreation.dialog.AddPriceBottomSheetDialog.AvailableDays
import com.gaboardi.restaurantcreation.dialog.AddPriceBottomSheetDialog.AvailableTime

class AddPriceBottomSheetViewModel : ViewModel() {
    var price: PriceUI = PriceUI()
    var lunchAvailable: ObservableField<Boolean> = ObservableField(true)
    var dinnerAvailable: ObservableField<Boolean> = ObservableField(true)
    var weekAvailable: ObservableField<Boolean> = ObservableField(true)
    var weekendAvailable: ObservableField<Boolean> = ObservableField(true)

    var currentAvailability: List<Pair<AvailableTime, AvailableDays>> = listOf(
        AvailableTime.LUNCH to AvailableDays.WEEK,
        AvailableTime.LUNCH to AvailableDays.WEEKEND,
        AvailableTime.DINNER to AvailableDays.WEEK,
        AvailableTime.DINNER to AvailableDays.WEEKEND
    )

    init {
        price.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (propertyId == BR.lunch || propertyId == BR.dinner || propertyId == BR.week || propertyId == BR.weekend) {
                    setEnabledViews(currentAvailability)
                }
            }
        })
    }

    fun setAvailability(list: List<Pair<AvailableTime, AvailableDays>>) {
        currentAvailability = list
        setEnabledViews(currentAvailability)
    }

    fun setEnabledViews(list: List<Pair<AvailableTime, AvailableDays>>) {
        val viewState = ViewState()
        val lunch = price.isLunch
        val dinner = price.isDinner
        val week = price.isWeek
        val weekend = price.isWeekend

        if ((lunch && dinner) || (week && weekend)) throw Exception()

        checkAvailableWithSomeCombination(list, viewState)

        if (lunch) {
            checkLunchCombinations(list, viewState)
        } else if (dinner) {
            checkDinnerCombinations(list, viewState)
        }

        if (week) {
            checkWeekCombinations(list, viewState)
        } else if (weekend) {
            checkWeekendCombinations(list, viewState)
        }

        dinnerAvailable.set(viewState.dinnerAvailable)
        lunchAvailable.set(viewState.lunchAvailable)
        weekAvailable.set(viewState.weekAvailable)
        weekendAvailable.set(viewState.weekendAvailable)
    }

    private fun checkAvailableWithSomeCombination(
        list: List<Pair<AvailableTime, AvailableDays>>,
        state: ViewState
    ) {
        if (list.none { it.first == AvailableTime.LUNCH }) state.lunchAvailable = false
        if (list.none { it.first == AvailableTime.DINNER }) state.dinnerAvailable = false
        if (list.none { it.second == AvailableDays.WEEK }) state.weekAvailable = false
        if (list.none { it.second == AvailableDays.WEEKEND }) state.weekendAvailable = false
    }

    private fun checkLunchCombinations(
        list: List<Pair<AvailableTime, AvailableDays>>,
        state: ViewState
    ) {
        if (!state.lunchAvailable) return
        val lunchOptions = list.filter { it.first == AvailableTime.LUNCH }
        if (lunchOptions.none { it.second == AvailableDays.WEEK }) state.weekAvailable = false
        if (lunchOptions.none { it.second == AvailableDays.WEEKEND }) state.weekendAvailable = false
    }

    private fun checkDinnerCombinations(
        list: List<Pair<AvailableTime, AvailableDays>>,
        state: ViewState
    ) {
        if (!state.dinnerAvailable) return
        val dinnerOptions = list.filter { it.first == AvailableTime.DINNER }
        if (dinnerOptions.none { it.second == AvailableDays.WEEK }) state.weekAvailable = false
        if (dinnerOptions.none { it.second == AvailableDays.WEEKEND }) state.weekendAvailable =
            false
    }

    private fun checkWeekCombinations(
        list: List<Pair<AvailableTime, AvailableDays>>,
        state: ViewState
    ) {
        if (!state.weekAvailable) return
        val weekOptions = list.filter { it.second == AvailableDays.WEEK }
        if (weekOptions.none { it.first == AvailableTime.LUNCH }) state.lunchAvailable = false
        if (weekOptions.none { it.first == AvailableTime.DINNER }) state.dinnerAvailable = false
    }

    private fun checkWeekendCombinations(
        list: List<Pair<AvailableTime, AvailableDays>>,
        state: ViewState
    ) {
        if (!state.weekendAvailable) return
        val weekendOptions = list.filter { it.second == AvailableDays.WEEKEND }
        if (weekendOptions.none { it.first == AvailableTime.LUNCH }) state.lunchAvailable = false
        if (weekendOptions.none { it.first == AvailableTime.DINNER }) state.dinnerAvailable = false
    }

    private data class ViewState(
        var lunchAvailable: Boolean = true,
        var dinnerAvailable: Boolean = true,
        var weekAvailable: Boolean = true,
        var weekendAvailable: Boolean = true
    )
}
