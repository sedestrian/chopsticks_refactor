package com.gaboardi.restaurantcreation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gaboardi.common.model.ErrorMessage
import com.gaboardi.common.model.Event
import com.gaboardi.common.model.State
import com.gaboardi.restaurant.converter.toRequest
import com.gaboardi.restaurant.model.ui.PriceUI
import com.gaboardi.restaurant.usecase.SaveRestaurantUseCase
import com.gaboardi.restaurantcreation.R
import com.gaboardi.restaurantcreation.dialog.AddPriceBottomSheetDialog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddRestaurantPricesViewModel @Inject constructor(
    private val saveRestaurantUseCase: SaveRestaurantUseCase
) : ViewModel() {
    private val _allYouCanEat: MutableLiveData<Boolean> = MutableLiveData(true)
    val allYouCanEat: LiveData<Boolean> = _allYouCanEat

    private val _prices: MutableLiveData<List<PriceUI>> = MutableLiveData(listOf())
    val prices: LiveData<List<PriceUI>> = _prices

    private val _restaurantSavedLoading: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val restaurantSavedLoading: LiveData<Event<Boolean>> = _restaurantSavedLoading

    private val _restaurantSaved: MutableLiveData<Event<Unit>> = MutableLiveData()
    val restaurantSaved: LiveData<Event<Unit>> = _restaurantSaved

    private val _restaurantSaveError: MutableLiveData<Event<ErrorMessage>> = MutableLiveData()
    val restaurantSaveError: LiveData<Event<ErrorMessage>> = _restaurantSaveError

    fun setAllYouCanEat(boolean: Boolean) {
        _allYouCanEat.postValue(boolean)
    }

    fun savePrice(price: PriceUI) {
        if (price.id != null) {
            updatePrice(price)
        } else {
            createPrice(price)
        }
    }

    fun getPriceAvailability(price: PriceUI? = null): List<Pair<AddPriceBottomSheetDialog.AvailableTime, AddPriceBottomSheetDialog.AvailableDays>> {
        val availability =
            mutableListOf<Pair<AddPriceBottomSheetDialog.AvailableTime, AddPriceBottomSheetDialog.AvailableDays>>()
        prices.value?.toMutableList()?.let { priceList ->
            if (price != null) {
                priceList.removeAll { it.id == price.id }
            }
            if (priceList.none { it.isLunch && it.isWeek }) availability.add(
                AddPriceBottomSheetDialog.AvailableTime.LUNCH to AddPriceBottomSheetDialog.AvailableDays.WEEK
            )
            if (priceList.none { it.isLunch && it.isWeekend }) availability.add(
                AddPriceBottomSheetDialog.AvailableTime.LUNCH to AddPriceBottomSheetDialog.AvailableDays.WEEKEND
            )
            if (priceList.none { it.isDinner && it.isWeek }) availability.add(
                AddPriceBottomSheetDialog.AvailableTime.DINNER to AddPriceBottomSheetDialog.AvailableDays.WEEK
            )
            if (priceList.none { it.isDinner && it.isWeekend }) availability.add(
                AddPriceBottomSheetDialog.AvailableTime.DINNER to AddPriceBottomSheetDialog.AvailableDays.WEEKEND
            )
        } ?: availability.addAll(
            listOf(
                AddPriceBottomSheetDialog.AvailableTime.LUNCH to AddPriceBottomSheetDialog.AvailableDays.WEEK,
                AddPriceBottomSheetDialog.AvailableTime.LUNCH to AddPriceBottomSheetDialog.AvailableDays.WEEKEND,
                AddPriceBottomSheetDialog.AvailableTime.DINNER to AddPriceBottomSheetDialog.AvailableDays.WEEK,
                AddPriceBottomSheetDialog.AvailableTime.DINNER to AddPriceBottomSheetDialog.AvailableDays.WEEKEND
            )
        )
        return availability
    }

    fun deletePrice(price: PriceUI) {
        val priceList = prices.value?.toMutableList()
        priceList?.let { list ->
            list.removeAll { it.id == price.id }
            _prices.postValue(list)
        }
    }

    private fun updatePrice(price: PriceUI) {
        val priceList = prices.value?.toMutableList()
        val priceUpdated = priceList?.firstOrNull { it.id == price.id }?.apply {
            time = price.time
            days = price.days
            cost = price.cost
        }
        priceUpdated?.let { updated ->
            priceList.removeAll { it.id == price.id }
            priceList.add(updated)
        }
        _prices.postValue(priceList)
    }

    private fun createPrice(price: PriceUI) {
        val priceList = prices.value?.toMutableList()
        val newId = priceList?.mapNotNull { it.id }?.maxOrNull()?.plus(1) ?: 0
        price.id = newId
        priceList?.add(price)
        _prices.postValue(priceList)
    }

    @ExperimentalCoroutinesApi
    fun saveRestaurant(name: String, address: String, telephone: String?, website: String?) {
        viewModelScope.launch {
            if (name.isNotEmpty() && address.isNotEmpty()) {
                val priceList = prices.value?.mapNotNull { it.toRequest() } ?: listOf()

                saveRestaurantUseCase.saveRestaurant(
                    name,
                    address,
                    telephone,
                    website,
                    0,
                    priceList
                ).collect { state ->
                    when (state) {
                        is State.Success -> {
                            _restaurantSavedLoading.postValue(Event(false))
                            _restaurantSaved.postValue(Event(Unit))
                        }
                        is State.Loading -> {
                            _restaurantSavedLoading.postValue(Event(true))
                        }
                        is State.Error -> {
                            _restaurantSavedLoading.postValue(Event(false))
                            _restaurantSaveError.postValue(Event(state.message))
                        }
                    }
                }
            } else {
                _restaurantSaveError.postValue(Event(ErrorMessage.of(R.string.server_error)))
            }
        }
    }
}