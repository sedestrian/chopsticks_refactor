package com.gaboardi.restaurantcreation.viewmodel

import androidx.lifecycle.ViewModel

class AddRestaurantViewModel : ViewModel() {
    var restaurantName: String = ""
    var restaurantAddress: String = ""
    var restaurantTelefone: String = ""
    var restaurantWebsite: String = ""
}