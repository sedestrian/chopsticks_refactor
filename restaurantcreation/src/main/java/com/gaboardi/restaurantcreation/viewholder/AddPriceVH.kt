package com.gaboardi.restaurantcreation.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.restaurantcreation.databinding.AddPriceItemBinding

class AddPriceVH(val binding: AddPriceItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(addPriceClicked: (() -> Unit)?) {
        binding.root.setOnClickListener {
            addPriceClicked?.invoke()
        }
    }
}