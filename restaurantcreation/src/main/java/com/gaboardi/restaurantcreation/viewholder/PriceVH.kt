package com.gaboardi.restaurantcreation.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.restaurant.model.ui.PriceUI
import com.gaboardi.restaurantcreation.R
import com.gaboardi.restaurantcreation.databinding.PriceItemBinding

class PriceVH(val binding: PriceItemBinding) : RecyclerView.ViewHolder(binding.root) {
    val context = binding.root.context

    fun bind(price: PriceUI, listener: ((price: PriceUI) -> Unit)?) {
        binding.price.text = context.getString(R.string.price_value, price.costDouble)
        price.days?.let {
            binding.day.text = context.getString(it.string)
        }
        price.time?.let {
            binding.icon.setImageResource(it.icon)
        }

        binding.root.setOnClickListener {
            listener?.invoke(price)
        }
    }
}
