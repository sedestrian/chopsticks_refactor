package com.gaboardi.restaurantcreation.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.gaboardi.restaurant.model.ui.PriceUI
import com.gaboardi.restaurantcreation.databinding.AddPriceBottomSheetBinding
import com.gaboardi.restaurantcreation.viewmodel.AddPriceBottomSheetViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class AddPriceBottomSheetDialog : BottomSheetDialogFragment {
    private val viewModel: AddPriceBottomSheetViewModel by viewModels()

    private lateinit var binding: AddPriceBottomSheetBinding

    private var availabilities: List<Pair<AvailableTime, AvailableDays>> = listOf()

    private var onPriceSelected: ((price: PriceUI) -> Unit)? = null
    private var onDeleteSelected: ((price: PriceUI) -> Unit)? = null
    private var hasPrice: PriceUI? = null

    constructor() : super()
    constructor(price: PriceUI) : super() {
        hasPrice = price
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = AddPriceBottomSheetBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        viewModel.setAvailability(availabilities)

        hasPrice?.let {
            viewModel.price = it
            binding.deleteButton.visibility = View.VISIBLE
            hasPrice = null
        }

        return binding.root
    }

    fun setAvailability(available: List<Pair<AvailableTime, AvailableDays>>) {
        availabilities = available
        try {
            viewModel.setAvailability(availabilities)
        } catch (e: Exception) {
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listen()

        val d = dialog as BottomSheetDialog
        d.behavior.skipCollapsed = true
        d.behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    fun setPriceSelectedListener(listener: ((price: PriceUI) -> Unit)?) {
        onPriceSelected = listener
    }

    fun setDeleteSelectedListener(listener: ((price: PriceUI) -> Unit)?) {
        onDeleteSelected = listener
    }

    private fun listen() {
        binding.saveButton.setOnClickListener {
            val price = viewModel.price
            price.let {
                onPriceSelected?.invoke(it)
            }
            dismiss()
        }

        binding.deleteButton.setOnClickListener {
            val price = viewModel.price
            onDeleteSelected?.invoke(price)
            dismiss()
        }
    }

    enum class AvailableTime {
        LUNCH,
        DINNER,
    }

    enum class AvailableDays {
        WEEK,
        WEEKEND
    }
}
