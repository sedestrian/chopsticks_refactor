package com.gaboardi.restaurantcreation.ui

import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.core.view.OnApplyWindowInsetsListener
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.gaboardi.restaurantcreation.viewmodel.AddRestaurantPricesViewModel
import com.gaboardi.restaurantcreation.viewmodel.AddRestaurantViewModel
import com.gaboardi.common.extensions.dp
import com.gaboardi.commonui.extensions.hideKeyboard
import com.gaboardi.commonui.utility.SpacingItemDecoration
import com.gaboardi.restaurantcreation.R
import com.gaboardi.restaurantcreation.adapter.AddPriceAdapter
import com.gaboardi.restaurantcreation.databinding.FragmentAddRestaurantPricesBinding
import com.gaboardi.restaurantcreation.dialog.AddPriceBottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddRestaurantPricesFragment : Fragment() {
    private lateinit var binding: FragmentAddRestaurantPricesBinding
    private lateinit var priceAdapter: AddPriceAdapter

    private val viewModel: AddRestaurantPricesViewModel by viewModels()
    private val sharedViewModel: AddRestaurantViewModel by navGraphViewModels(R.id.restaurantCreationFlow)

    private val PRICE_DIALOG_TAG = "Price dialog"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddRestaurantPricesBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        observeViewModel()

        binding.buttonContinue.setOnClickListener {
            viewModel.saveRestaurant(
                sharedViewModel.restaurantName,
                sharedViewModel.restaurantAddress,
                sharedViewModel.restaurantTelefone,
                sharedViewModel.restaurantWebsite
            )
        }

        binding.backButton.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.allYouCanEat.addOnCheckedListener {
            if (!it) viewModel.setAllYouCanEat(true)
        }

        binding.fromMenu.addOnCheckedListener {
            if (!it) viewModel.setAllYouCanEat(false)
        }

        setupPriceRecycler()
    }

    private fun observeViewModel() {
        viewModel.allYouCanEat.observe(viewLifecycleOwner) {
            if (it)
                binding.motionLayout.transitionToEnd()
            else
                binding.motionLayout.transitionToStart()
        }
        viewModel.prices.observe(viewLifecycleOwner) {
            priceAdapter.setPrices(it)
        }
        viewModel.restaurantSavedLoading.observe(viewLifecycleOwner) { event ->
            event.consume { loading ->
                if (loading) {
                    val color = ContextCompat.getColor(requireContext(), R.color.white)
                    val drawable = CircularProgressDrawable(requireContext()).apply {
                        setColorSchemeColors(color)
                        strokeWidth = 3.dp.toFloat()
                    }
                    drawable.start()
                    binding.buttonContinue.icon = drawable
                } else {
                    binding.buttonContinue.icon = null
                }
            }
        }

        viewModel.restaurantSaved.observe(viewLifecycleOwner) {
            findNavController().popBackStack(R.id.restaurantCreationFlow, true)
        }
    }

    private fun setupPriceRecycler() {
        priceAdapter = AddPriceAdapter()
        priceAdapter.priceClicked { price ->
            val dialog = AddPriceBottomSheetDialog(price)
            dialog.setAvailability(viewModel.getPriceAvailability(price))
            dialog.setPriceSelectedListener {
                viewModel.savePrice(it)
                hideKeyboard()
            }
            dialog.setDeleteSelectedListener {
                viewModel.deletePrice(it)
                hideKeyboard()
            }
            dialog.show(childFragmentManager, PRICE_DIALOG_TAG)
        }
        priceAdapter.addPriceClicked {
            val dialog = AddPriceBottomSheetDialog()
            dialog.setAvailability(viewModel.getPriceAvailability())
            dialog.setPriceSelectedListener {
                viewModel.savePrice(it)
                hideKeyboard()
            }
            dialog.show(childFragmentManager, PRICE_DIALOG_TAG)
        }
        binding.priceList.addItemDecoration(SpacingItemDecoration())
        binding.priceList.layoutManager = LinearLayoutManager(requireContext())
        binding.priceList.adapter = priceAdapter
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val window = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        view.updatePadding(top = window.top, bottom = window.bottom)
        insets
    }
}