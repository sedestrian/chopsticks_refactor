package com.gaboardi.restaurantcreation.ui

import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.gaboardi.restaurantcreation.viewmodel.AddRestaurantViewModel
import com.gaboardi.restaurantcreation.R
import com.gaboardi.restaurantcreation.databinding.FragmentAddRestaurantNameBinding


class AddRestaurantNameFragment : Fragment() {
    private var wasVisible = false
    private lateinit var binding: FragmentAddRestaurantNameBinding

    private val sharedViewModel: AddRestaurantViewModel by navGraphViewModels(R.id.restaurantCreationFlow)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddRestaurantNameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        binding.restaurantName.setOnFocusChangeListener { v, hasFocus ->
            println("Is focused: $hasFocus")
            if (hasFocus) {
                binding.motionLayout.transitionToEnd()
            } else {
                binding.motionLayout.transitionToStart()
            }
        }

        binding.backButton.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.buttonNext.setOnClickListener {
            sharedViewModel.restaurantName = binding.restaurantName.text.toString()
            val action =
                AddRestaurantNameFragmentDirections.actionAddRestaurantNameFragmentToAddRestaurantLocationFragment()
            findNavController().navigate(action)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            view.setWindowInsetsAnimationCallback(imeCallback)
        }
    }

    override fun onResume() {
        super.onResume()

        val insets = ViewCompat.getRootWindowInsets(requireView())
        val keyboardInset =
            insets?.getInsets(WindowInsetsCompat.Type.ime() or WindowInsetsCompat.Type.systemBars())

        keyboardInset?.let { keyboard ->
            binding.buttonNext.translationY =
                -(keyboard.bottom.toFloat() - binding.buttonNext.marginBottom).coerceAtLeast(0f)
        }

        binding.restaurantName.requestFocus()
        val imm = getSystemService(requireContext(), InputMethodManager::class.java)
        imm?.showSoftInput(binding.restaurantName, InputMethodManager.SHOW_IMPLICIT)
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val window = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        println("Restaurant insets bottom: ${window.bottom}")
        view.updatePadding(top = window.top, bottom = window.bottom)
        if (!insets.isVisible(WindowInsetsCompat.Type.ime())) {
            if (wasVisible)
                binding.restaurantName.clearFocus()
        } else {
            wasVisible = true
        }
        insets
    }

    private val imeCallback = @RequiresApi(Build.VERSION_CODES.R)
    object : WindowInsetsAnimation.Callback(DISPATCH_MODE_CONTINUE_ON_SUBTREE) {
        override fun onProgress(
            insets: WindowInsets,
            runningAnimations: MutableList<WindowInsetsAnimation>
        ): WindowInsets {
            val keyboard = insets.getInsets(WindowInsetsCompat.Type.ime())
            binding.buttonNext.translationY =
                -(keyboard.bottom.toFloat() - binding.buttonNext.marginBottom).coerceAtLeast(0f)
            return insets
        }
    }
}