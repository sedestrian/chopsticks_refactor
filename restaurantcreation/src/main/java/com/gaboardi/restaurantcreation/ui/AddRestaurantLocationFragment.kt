package com.gaboardi.restaurantcreation.ui

import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.gaboardi.restaurantcreation.viewmodel.AddRestaurantViewModel
import com.gaboardi.restaurantcreation.R
import com.gaboardi.restaurantcreation.databinding.FragmentAddRestaurantLocationBinding

class AddRestaurantLocationFragment : Fragment() {
    private var wasVisible = false
    private var keyboardOnOpen = false
    private lateinit var binding: FragmentAddRestaurantLocationBinding

    private val sharedViewModel: AddRestaurantViewModel by navGraphViewModels(R.id.restaurantCreationFlow)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddRestaurantLocationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        val insets = ViewCompat.getRootWindowInsets(requireView())
        keyboardOnOpen = insets?.isVisible(WindowInsetsCompat.Type.ime()) ?: false

        binding.motionLayout.progress = 1f

        binding.backButton.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.buttonNext.setOnClickListener {
            sharedViewModel.restaurantAddress = binding.restaurantLocation.text.toString()
            val action =
                AddRestaurantLocationFragmentDirections.actionAddRestaurantLocationFragmentToAddRestaurantDetailsFragment()
            findNavController().navigate(action)
        }

        binding.restaurantLocation.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.motionLayout.transitionToEnd()
            } else {
                binding.motionLayout.transitionToStart()
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            view.setWindowInsetsAnimationCallback(imeCallback)
        }
    }

    override fun onResume() {
        super.onResume()

        val insets = ViewCompat.getRootWindowInsets(requireView())
        val keyboardInset =
            insets?.getInsets(WindowInsetsCompat.Type.ime() or WindowInsetsCompat.Type.systemBars())

        keyboardInset?.let { keyboard ->
            binding.buttonNext.translationY =
                -(keyboard.bottom.toFloat() - binding.buttonNext.marginBottom).coerceAtLeast(0f)
        }

        binding.restaurantLocation.requestFocus()
        val imm = getSystemService(requireContext(), InputMethodManager::class.java)
        imm?.showSoftInput(binding.restaurantLocation, InputMethodManager.SHOW_IMPLICIT)
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val window = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        view.updatePadding(top = window.top, bottom = window.bottom)

        if (!insets.isVisible(WindowInsetsCompat.Type.ime())) {
            if (wasVisible) {
                binding.restaurantLocation.clearFocus()
                wasVisible = false
            }
        } else {
            wasVisible = true
        }
        insets
    }

    private val imeCallback = @RequiresApi(Build.VERSION_CODES.R)
    object : WindowInsetsAnimation.Callback(DISPATCH_MODE_CONTINUE_ON_SUBTREE) {
        override fun onProgress(
            insets: WindowInsets,
            runningAnimations: MutableList<WindowInsetsAnimation>
        ): WindowInsets {
            val keyboard = insets.getInsets(WindowInsetsCompat.Type.ime())
            binding.buttonNext.translationY =
                -(keyboard.bottom.toFloat() - binding.buttonNext.marginBottom).coerceAtLeast(0f)
            return insets
        }
    }
}