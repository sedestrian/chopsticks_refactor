import AppDependencies.implementApp
import AppDependencies.implementHilt
import AppDependencies.implementNavigation

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
    id("androidx.navigation.safeargs.kotlin")
}

android {
    compileSdk = AppConfig.compileSdk

    defaultConfig {
        minSdk = AppConfig.minSdk
        targetSdk = AppConfig.targetSdk

        testInstrumentationRunner = AppConfig.androidTestInstrumentation
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementBom(AppDependencies.firebaseBom)

    implementApp()
    implementNavigation()
    implementHilt()

    implementation(AppDependencies.material)
    implementation(AppDependencies.constraintLayout)
    implementation(AppDependencies.firebaseCommon)
    implementation(AppDependencies.firestore)
    implementation(AppDependencies.firebaseAuth)
    implementation(AppDependencies.swipeRefresh)

    coreLibraryDesugaring(AppDependencies.coreDesugaring)

    implementModule(AppDependencies.Modules.navigation)
    implementModule(AppDependencies.Modules.commonui)
    implementModule(AppDependencies.Modules.common)
    implementModule(AppDependencies.Modules.restaurant)
}