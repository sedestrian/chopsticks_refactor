package com.gaboardi.splash.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gaboardi.common.model.Event
import com.gaboardi.user.usecase.UserAuthenticatedUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val checkLoginUseCase: UserAuthenticatedUseCase
) : ViewModel() {
    private val _userLogged: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val userLogged: LiveData<Event<Boolean>> = _userLogged

    fun checkLogin() {
        viewModelScope.launch {
            _userLogged.postValue(Event(checkLoginUseCase.isUserAuthenticated()))
        }
    }
}