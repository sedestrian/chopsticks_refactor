package com.gaboardi.splash.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.gaboardi.host.viewmodel.HostViewModel
import com.gaboardi.splash.databinding.FragmentSplashBinding
import com.gaboardi.splash.viewmodel.SplashViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment: Fragment() {
    private val viewModel: SplashViewModel by viewModels()
    private val firebaseViewModel: HostViewModel by activityViewModels()

    private lateinit var binding: FragmentSplashBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSplashBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.userLogged.observe(viewLifecycleOwner) { event ->
            event.consume {
                val direction = if (it) {
                    firebaseViewModel.beginSubscription()
                    SplashFragmentDirections.actionSplashFragmentToHomeFlow()
                } else
                    SplashFragmentDirections.actionSplashFragmentToLoginFlow()
                findNavController().navigate(direction)
            }
        }

        viewModel.checkLogin()
    }
}