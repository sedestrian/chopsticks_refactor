package com.gaboardi.host.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gaboardi.restaurant.usecase.RestaurantsSubscriptionUseCase
import com.gaboardi.table.usecase.TablesSubscriptionUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HostViewModel @Inject constructor(
    private val restaurantsSubscriptionUseCase: RestaurantsSubscriptionUseCase,
    private val tablesSubscriptionUseCase: TablesSubscriptionUseCase
) : ViewModel() {
    @ExperimentalCoroutinesApi
    fun beginSubscription() {
        viewModelScope.launch {
            launch { subscribeToRestaurants() }
            launch { subscribeToTables() }
            /*launch { subscribeToOrders() }
            launch { subscribeToPlates() }*/
        }
    }

    @ExperimentalCoroutinesApi
    private suspend fun subscribeToRestaurants() {
        restaurantsSubscriptionUseCase.startDatabase().collect { }
    }

    private suspend fun subscribeToTables(){
        tablesSubscriptionUseCase().collect {  }
    }

    /*@ExperimentalCoroutinesApi
    private suspend fun subscribeToOrders() {
        ordersSubscriptionUseCase.subscribeToRemote().collect { }
    }

    @ExperimentalCoroutinesApi
    private suspend fun subscribeToPlates() {
        platesSubscriptionUseCase.startDatabase().collect { }
    }*/
}