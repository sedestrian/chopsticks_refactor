package com.gaboardi.user.dao

import androidx.room.Dao
import androidx.room.Query

@Dao
abstract class DatabaseDao {
    @Query("DELETE FROM restaurants")
    abstract suspend fun clearRestaurants()

    @Query("DELETE FROM prices")
    abstract suspend fun clearPrices()

    suspend fun clearDatabase(){
        clearRestaurants()
        clearPrices()
    }
}