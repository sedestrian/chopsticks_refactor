package com.gaboardi.user.usecase

import com.gaboardi.user.repository.UserRepository
import javax.inject.Inject

class UserAuthenticatedUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend fun isUserAuthenticated() = userRepository.isUserAuthenticated()
}