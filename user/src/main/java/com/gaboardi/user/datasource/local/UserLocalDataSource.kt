package com.gaboardi.user.datasource.local

import com.gaboardi.common.model.ApiResponse

interface UserLocalDataSource {
    suspend fun logout(): ApiResponse<Unit>
}