package com.gaboardi.user.datasource.remote

interface UserRemoteDataSource {
    suspend fun isUserAuthenticated(): Boolean
}