package com.gaboardi.user.datasource.remote

import com.google.firebase.auth.FirebaseAuth
import javax.inject.Inject

class UserRemoteDataSourceImpl @Inject constructor(
    private val firebaseAuth: FirebaseAuth
): UserRemoteDataSource {
    override suspend fun isUserAuthenticated(): Boolean {
        return firebaseAuth.currentUser != null
    }
}