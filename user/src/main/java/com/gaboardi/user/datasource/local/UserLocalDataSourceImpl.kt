package com.gaboardi.user.datasource.local

import android.content.Context
import com.gaboardi.common.model.ApiResponse
import com.gaboardi.user.dao.DatabaseDao
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class UserLocalDataSourceImpl @Inject constructor(
    private val auth: FirebaseAuth,
    private val database: DatabaseDao,
    @ApplicationContext private val context: Context
) : UserLocalDataSource {
    override suspend fun logout(): ApiResponse<Unit> {
        auth.signOut()
        database.clearDatabase()
        return logoutGoogle()
    }

    private suspend fun logoutGoogle(): ApiResponse<Unit> {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()

        val signInClient = GoogleSignIn.getClient(context, gso)
        return try {
            signInClient.signOut().await()
            ApiResponse.success(Unit)
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }
}