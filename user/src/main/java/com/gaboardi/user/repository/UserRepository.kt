package com.gaboardi.user.repository

import com.gaboardi.common.model.State
import kotlinx.coroutines.flow.Flow

interface UserRepository {
    suspend fun isUserAuthenticated(): Boolean
    suspend fun logout(): Flow<State>
}