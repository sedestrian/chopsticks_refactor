package com.gaboardi.user.repository

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.State
import com.gaboardi.user.datasource.local.UserLocalDataSource
import com.gaboardi.user.datasource.remote.UserRemoteDataSource
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val remoteDataSource: UserRemoteDataSource,
    private val localDataSource: UserLocalDataSource
): UserRepository {
    override suspend fun isUserAuthenticated(): Boolean {
        return remoteDataSource.isUserAuthenticated()
    }

    override suspend fun logout() = flow {
        emit(State.loading())
        try {
            when(val result = localDataSource.logout()){
                is ApiResponse.Success -> emit(State.success())
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        }catch (e: Exception){
            emit(State.error(e.localizedMessage))
        }
    }
}