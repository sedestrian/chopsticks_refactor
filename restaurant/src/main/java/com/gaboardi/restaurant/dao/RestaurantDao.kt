package com.gaboardi.restaurant.dao

import androidx.room.*
import com.gaboardi.restaurant.model.db.PriceEntity
import com.gaboardi.restaurant.model.db.RestaurantData
import com.gaboardi.restaurant.model.db.RestaurantEntity
import com.gaboardi.restaurant.model.db.RestaurantInsertEntity
import kotlinx.coroutines.flow.Flow

@Dao
abstract class RestaurantDao {
    @Transaction
    @Query("SELECT * FROM restaurants WHERE userId = :userId")
    abstract fun observeRestaurants(userId: String?): Flow<List<RestaurantData>>

    @Transaction
    @Query("SELECT * FROM restaurants WHERE id = :restaurantId LIMIT 1")
    abstract fun observeRestaurant(restaurantId: String): Flow<RestaurantData?>

    @Query("DELETE FROM restaurants")
    abstract fun deleteRestaurants()

    @Query("DELETE FROM restaurants WHERE id = :restaurantId")
    abstract fun deleteRestaurantDB(restaurantId: String)

    @Transaction
    open suspend fun deleteRestaurant(restaurantId: String) {
//        cleanRestaurantPlates(restaurantId)
//        cleanRestaurantOrders(restaurantId)
        deleteRestaurantDB(restaurantId)
    }

    open suspend fun saveRestaurant(restaurantEntity: RestaurantInsertEntity){
        val restaurant = restaurantEntity.restaurantEntity
        val prices = restaurantEntity.prices

        saveRestaurant(restaurant)
        savePrices(prices)
    }

    @Transaction
    open suspend fun saveRestaurants(restaurants: List<RestaurantInsertEntity>, userId: String?) {
        val restaurantBundle = restaurants.map { it.restaurantEntity }
        val priceBundle = restaurants.flatMap { it.prices }

//        val restaurantIds = restaurants.map { it.restaurant.id }

//        cleanPlatesForRestaurants(restaurantIds)
//        cleanOrdersForRestaurants(restaurantIds)
//        cleanPrices(priceBundle.map { it.id })
        cleanRestaurants(restaurantBundle.map { it.id }, userId)
        saveRestaurantEntities(restaurantBundle)
        savePrices(priceBundle)
    }

    @Query("DELETE FROM restaurants WHERE userId = :userId AND id NOT IN(:restaurants)")
    abstract fun cleanRestaurants(restaurants: List<String>, userId: String?)

    /*@Query("DELETE FROM plates WHERE orderId IN (SELECT id FROM orders WHERE restaurantId = :restaurantId)")
    abstract suspend fun cleanRestaurantPlates(restaurantId: String)

    @Query("DELETE FROM orders WHERE restaurantId = :restaurantId")
    abstract suspend fun cleanRestaurantOrders(restaurantId: String)*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun saveRestaurantEntities(restaurants: List<RestaurantEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun saveRestaurant(restaurant: RestaurantEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun savePrices(prices: List<PriceEntity>)
}