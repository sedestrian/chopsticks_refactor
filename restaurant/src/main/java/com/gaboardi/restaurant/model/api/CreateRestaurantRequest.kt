package com.gaboardi.restaurant.model.api

class CreateRestaurantRequest(
    val userId: String,
    val name: String,
    val address: String,
    val phoneNumber: String?,
    val website: String?,
    val category: Int,
    val prices: List<PriceRequest> = listOf()
)