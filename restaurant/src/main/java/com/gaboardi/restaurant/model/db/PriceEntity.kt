package com.gaboardi.restaurant.model.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "prices",
    foreignKeys = [
        ForeignKey(
            entity = RestaurantEntity::class,
            parentColumns = ["id"],
            childColumns = ["restaurantId"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class PriceEntity(
    @PrimaryKey
    val id: Int,
    @ColumnInfo(index = true)
    val restaurantId: String,
    val cost: Double,
    val time: Int,
    val days: Int
)