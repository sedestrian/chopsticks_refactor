package com.gaboardi.restaurant.model.api

data class RestaurantResult(
    val id: String,
    val userId: String,
    val name: String,
    val address: String,
    val phoneNumber: String?,
    val url: String?,
    val category: Int,
    val averageRating: Double?,
    val prices: List<PriceResult>?
)