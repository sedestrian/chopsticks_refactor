package com.gaboardi.restaurant.model.ui

import com.gaboardi.table.model.ui.Table

data class RestaurantUI(
    val id: String,
    val name: String,
    val address: String,
    val phone: String?,
    val website: String?,
    val category: Int,
    val averageRating: Double?,
    val prices: List<Price>,
    val tables: List<Table>
) {
    override fun equals(other: Any?): Boolean {
        return if (other is RestaurantUI) {
            var equals = true

            if (
                id != other.id ||
                name != other.name ||
                address != other.address ||
                phone != other.phone ||
                website != other.website ||
                category != other.category ||
                averageRating != other.averageRating
            ) equals = false
            if (prices.size != other.prices.size) equals = false
            else
                prices.forEachIndexed { index, price ->
                    if (index < other.prices.size) {
                        if (price != other.prices[index])
                            equals = false
                    } else equals = false
                }
            /*if (orders.size != other.orders.size) equals = false
            else
                orders.forEachIndexed { index, order ->
                    if (index < other.orders.size) {
                        if (order != other.orders[index])
                            equals = false
                    } else equals = false
                }*/
            equals
        } else super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}