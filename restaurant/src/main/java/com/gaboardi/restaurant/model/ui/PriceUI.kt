package com.gaboardi.restaurant.model.ui

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.gaboardi.restaurant.BR
import com.gaboardi.restaurant.R

class PriceUI() : BaseObservable() {
    var id: Int? = null
    var costDouble: Double = 0.0
        private set
        get() {
            val number = cost.toDoubleOrNull()
            return number ?: 0.0
        }

    var days: Days? = null
    var time: Time? = null

    @get:Bindable
    var cost: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.cost)
            notifyPropertyChanged(BR.compiled)
        }

    @get:Bindable
    var isWeek: Boolean
        set(value) {
            if (value)
                days = Days.WEEK
            notifyPropertyChanged(BR.week)
            notifyPropertyChanged(BR.weekend)
            notifyPropertyChanged(BR.compiled)
        }
        get() {
            return if (days != null)
                days == Days.WEEK
            else
                false
        }

    @get:Bindable
    var isWeekend: Boolean
        set(value) {
            if (value)
                days = Days.WEEKEND
            notifyPropertyChanged(BR.weekend)
            notifyPropertyChanged(BR.week)
            notifyPropertyChanged(BR.compiled)
        }
        get() {
            return if (days != null)
                days == Days.WEEKEND
            else
                false
        }

    @get:Bindable
    var isLunch: Boolean
        set(value) {
            if (value)
                time = Time.LUNCH
            notifyPropertyChanged(BR.lunch)
            notifyPropertyChanged(BR.dinner)
            notifyPropertyChanged(BR.compiled)
        }
        get() {
            return if (time != null)
                time == Time.LUNCH
            else
                false
        }

    @get:Bindable
    var isDinner: Boolean
        set(value) {
            if (value)
                time = Time.DINNER
            notifyPropertyChanged(BR.dinner)
            notifyPropertyChanged(BR.lunch)
            notifyPropertyChanged(BR.compiled)
        }
        get() {
            return if (time != null)
                time == Time.DINNER
            else
                false
        }

    @get:Bindable
    val isCompiled: Boolean
        get() {
            val costNum = cost.toDoubleOrNull()
            return costNum != null && !costNum.isNaN() && time != null && days != null
        }
}

enum class Time(val id: Int, @StringRes val string: Int, @DrawableRes val icon: Int) {
    LUNCH(0, R.string.lunch_name, R.drawable.ic_sunlight),
    DINNER(1, R.string.dinner_name, R.drawable.ic_moona);

    companion object {
        fun from(id: Int) = values().first { it.id == id }
    }
}

enum class Days(val id: Int, @StringRes val string: Int) {
    WEEK(0, R.string.weekdays_name),
    WEEKEND(1, R.string.weekend_name);

    companion object {
        fun from(id: Int) = values().first { it.id == id }
    }
}