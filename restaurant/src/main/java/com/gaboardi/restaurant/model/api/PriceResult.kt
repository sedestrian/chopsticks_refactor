package com.gaboardi.restaurant.model.api

data class PriceResult(
    val priceId: Int,
    val cost: Double,
    val time: Int,
    val days: Int
)