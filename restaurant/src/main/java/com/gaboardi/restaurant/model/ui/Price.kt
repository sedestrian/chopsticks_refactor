package com.gaboardi.restaurant.model.ui

data class Price(
    val id: Int,
    val restaurantId: String,
    val cost: Double,
    val time: Int,
    val days: Int
)