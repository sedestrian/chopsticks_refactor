package com.gaboardi.restaurant.model.db

import com.gaboardi.restaurant.model.db.PriceEntity
import com.gaboardi.restaurant.model.db.RestaurantEntity

data class RestaurantInsertEntity(
    val restaurantEntity: RestaurantEntity,
    val prices: List<PriceEntity>
)