package com.gaboardi.restaurant.model.ui

import com.gaboardi.restaurant.R

enum class SortMenuItem(val id: Int, val icon: Int, val text: Int){
    RATING_ASC(0, R.drawable.star_outline, R.string.rating_asc),
    RATING_DESC(1, R.drawable.star, R.string.rating_desc),
    DATE_ASC(2, R.drawable.update, R.string.date_asc),
    DATE_DESC(3, R.drawable.restore, R.string.date_desc),
    NAME_ASC(4, R.drawable.text_rotation_down, R.string.name_asc),
    NAME_DESC(5, R.drawable.text_rotate_up, R.string.name_desc);
}