package com.gaboardi.restaurant.model.ui

data class Restaurant(
    val id: String,
    val name: String,
    val address: String,
    val phone: String?,
    val website: String?,
    val category: Int,
    val averageRating: Double?
)