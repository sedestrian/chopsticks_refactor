package com.gaboardi.restaurant.model.db

import androidx.room.DatabaseView
import androidx.room.Junction
import androidx.room.Relation
import com.gaboardi.table.model.db.TableData
import com.gaboardi.table.model.db.TableRestaurantJunction

@DatabaseView(
    """
    SELECT 
        restaurants.id,
        restaurants.userId,
        restaurants.name,
        restaurants.address,
        restaurants.phone,
        restaurants.website,
        restaurants.category,
        restaurants.averageRating,
        prices.*,
        tablerestaurantjunction.*,
        tabledata.*
    FROM 
        restaurants 
    LEFT JOIN 
        prices
    ON 
        restaurants.id = prices.restaurantId
    JOIN 
        tablerestaurantjunction
    ON 
        restaurants.id = tablerestaurantjunction.restaurantId
    JOIN
        tabledata
    ON
        tablerestaurantjunction.tableId = tabledata.id
"""
)
data class RestaurantData(
    val id: String,
    val userId: String,
    val name: String,
    val address: String,
    val phone: String?,
    val website: String?,
    val category: Int,
    val averageRating: Double?,
    @Relation(
        parentColumn = "id",
        entityColumn = "restaurantId"
    )
    val prices: List<PriceEntity>,
    @Relation(
        parentColumn = "id",
        entityColumn = "id",
        associateBy = Junction(
            value = TableRestaurantJunction::class,
            parentColumn = "restaurantId",
            entityColumn = "tableId"
        )
    )
    val tables: List<TableData>
) {
    override fun equals(other: Any?): Boolean {
        return if (other is RestaurantData) {
            var equals = true

            if (
                id != other.id ||
                userId != other.userId ||
                name != other.name ||
                address != other.address ||
                phone != other.phone ||
                website != other.website ||
                category != other.category ||
                averageRating != other.averageRating
            ) equals = false
            if (prices.size != other.prices.size) equals = false
            else
                prices.forEachIndexed { index, price ->
                    if (index < other.prices.size) {
                        if (price != other.prices[index])
                            equals = false
                    } else equals = false
                }
            /*if (orders.size != other.orders.size) equals = false
            else
                orders.forEachIndexed { index, order ->
                    if (index < other.orders.size) {
                        if (order != other.orders[index])
                            equals = false
                    } else equals = false
                }*/
            equals
        } else super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}