package com.gaboardi.restaurant.model.api

data class PriceRequest(
    val priceId: Int,
    val cost: Double,
    val time: Int,
    val days: Int
)