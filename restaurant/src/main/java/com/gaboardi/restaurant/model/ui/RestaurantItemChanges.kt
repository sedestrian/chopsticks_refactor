package com.gaboardi.restaurant.model.ui

data class RestaurantItemChanges(
    val name: String? = null,
    val address: String? = null,
    val category: Int? = null,
    val rating: Double? = null,
    val prices: List<Price>? = null
) {
    fun isEmpty(): Boolean =
        name == null &&
                address == null &&
                category == null &&
                rating == null &&
                prices == null
}