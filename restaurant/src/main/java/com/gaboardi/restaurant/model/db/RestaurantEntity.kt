package com.gaboardi.restaurant.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "restaurants")
data class RestaurantEntity(
    @PrimaryKey
    val id: String,
    val userId: String,
    val name: String,
    val address: String,
    val phone: String?,
    val website: String?,
    val category: Int,
    val averageRating: Double?
)