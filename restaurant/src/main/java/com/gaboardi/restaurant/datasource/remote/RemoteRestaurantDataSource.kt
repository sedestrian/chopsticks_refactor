package com.gaboardi.restaurant.datasource.remote

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.restaurant.model.api.PriceRequest
import com.gaboardi.restaurant.model.api.RestaurantResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

interface RemoteRestaurantDataSource {
    fun subscribeToFirestore(): Flow<ApiResponse<List<RestaurantResult>>>
    @ExperimentalCoroutinesApi
    suspend fun deleteRestaurant(restaurantId: String): ApiResponse<Unit>
    suspend fun createRestaurant(
        name: String,
        address: String,
        telephone: String?,
        website: String?,
        type: Int,
        prices: List<PriceRequest>
    ): ApiResponse<Unit>

    @ExperimentalCoroutinesApi
    fun subscribeToRestaurant(restaurantId: String): Flow<ApiResponse<RestaurantResult>>
}