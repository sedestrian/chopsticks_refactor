package com.gaboardi.restaurant.datasource.local

import com.gaboardi.restaurant.model.api.RestaurantResult
import com.gaboardi.restaurant.model.ui.RestaurantUI
import kotlinx.coroutines.flow.Flow

interface LocalRestaurantDataSource {
    suspend fun saveRestaurants(restaurants: List<RestaurantResult>)
    fun observeRestaurants(): Flow<List<RestaurantUI>>
    suspend fun deleteRestaurant(id: String)
    fun observeRestaurant(restaurantId: String): Flow<RestaurantUI?>
    suspend fun saveRestaurant(restaurant: RestaurantResult)
}