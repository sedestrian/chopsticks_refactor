package com.gaboardi.restaurant.datasource.remote

import com.gaboardi.common.extensions.fromJson
import com.gaboardi.common.extensions.toJson
import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.Collections
import com.gaboardi.common.model.DatabaseField
import com.gaboardi.restaurant.model.api.CreateRestaurantRequest
import com.gaboardi.restaurant.model.api.PriceRequest
import com.gaboardi.restaurant.model.api.RestaurantResult
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class RemoteRestaurantDataSourceImpl @Inject constructor(
    private val firestore: FirebaseFirestore,
    private val firebaseUser: FirebaseUser?
) : RemoteRestaurantDataSource {
    override suspend fun createRestaurant(
        name: String,
        address: String,
        telephone: String?,
        website: String?,
        type: Int,
        prices: List<PriceRequest>
    ): ApiResponse<Unit> {
        return try {
            firebaseUser?.let { user ->
                val data = CreateRestaurantRequest(
                    user.uid,
                    name,
                    address,
                    telephone,
                    website,
                    type,
                    prices
                )
                val restaurantsRef = firestore.collection(Collections.Restaurants)
                restaurantsRef.add(data).await()
                ApiResponse.success(Unit)
            } ?: ApiResponse.error("User credentials missing")
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage ?: "Api error")
        }
    }

    override suspend fun deleteRestaurant(restaurantId: String): ApiResponse<Unit> {
        return try {
            firebaseUser?.let { user ->
                val batch = firestore.batch()

                val restaurantsCollection = firestore.collection(Collections.Restaurants)
                val ordersCollection = firestore.collection(Collections.Orders)
                val platesCollection = firestore.collection(Collections.Plates)

                batch.delete(restaurantsCollection.document(restaurantId))

                val ordersQuery =
                    ordersCollection.whereEqualTo(DatabaseField.Order.RestaurantId, restaurantId)
                val orders = ordersQuery.get().await()

                orders.documents.forEach { batch.delete(it.reference) }

                if (orders.documents.isNotEmpty()) {
                    val orderIds = orders.map { it.id }

                    val platesQuery =
                        platesCollection.whereIn(DatabaseField.Plate.OrderId, orderIds)
                    val plates = platesQuery.get().await()

                    plates.documents.forEach { batch.delete(it.reference) }
                }

                batch.commit().await()
                ApiResponse.success(Unit)
            } ?: ApiResponse.error("User credentials missing")
        } catch (e: Exception) {
            ApiResponse.error(e.localizedMessage)
        }
    }

    override fun subscribeToFirestore() = callbackFlow<ApiResponse<List<RestaurantResult>>> {
        firebaseUser?.let { user ->
            val subscription = firestore.collection(Collections.Restaurants)
                .whereEqualTo(DatabaseField.Restaurant.UserId, user.uid)
                .addSnapshotListener { value, error ->
                    if (error != null) trySendBlocking(ApiResponse.error(error.localizedMessage))
                    else {
                        val data = value?.documents?.map {
                            val restaurant = it.data.toJson().fromJson<RestaurantResult>()
                            restaurant.copy(id = it.id)
                        }
                        trySendBlocking(ApiResponse.success(data ?: listOf()))
                    }
                }
            awaitClose { subscription.remove() }
        }
    }

    @ExperimentalCoroutinesApi
    override fun subscribeToRestaurant(restaurantId: String) =
        callbackFlow<ApiResponse<RestaurantResult>> {
            val subscription = firestore
                .collection(Collections.Restaurants)
                .document(restaurantId)
                .addSnapshotListener { value, error ->
                    when {
                        error != null -> trySendBlocking(ApiResponse.error(error.localizedMessage))
                        value != null -> {
                            val restaurant = value.data.toJson().fromJson<RestaurantResult>()
                            trySendBlocking(ApiResponse.success(restaurant.copy(id = value.id)))
                        }
                    }
                }
            awaitClose { subscription.remove() }
        }

}