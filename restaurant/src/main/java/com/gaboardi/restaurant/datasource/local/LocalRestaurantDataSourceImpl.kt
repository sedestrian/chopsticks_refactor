package com.gaboardi.restaurant.datasource.local

import com.gaboardi.restaurant.converter.toInsertEntity
import com.gaboardi.restaurant.converter.toUi
import com.gaboardi.restaurant.dao.RestaurantDao
import com.gaboardi.restaurant.model.api.RestaurantResult
import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LocalRestaurantDataSourceImpl @Inject constructor(
    private val restaurantDao: RestaurantDao,
    private val firebaseUser: FirebaseUser?
) : LocalRestaurantDataSource {
    override suspend fun saveRestaurants(restaurants: List<RestaurantResult>) =
        withContext(Dispatchers.IO) {
            restaurantDao.saveRestaurants(
                restaurants.map { it.toInsertEntity() },
                firebaseUser?.uid
            )
        }

    override suspend fun saveRestaurant(restaurant: RestaurantResult) =
        withContext(Dispatchers.IO) {
            restaurantDao.saveRestaurant(restaurant.toInsertEntity())
        }

    override fun observeRestaurants(): Flow<List<RestaurantUI>> {
        return restaurantDao.observeRestaurants(firebaseUser?.uid)
            .map { list -> list.map { restaurantAndPrices -> restaurantAndPrices.toUi() } }
    }

    override fun observeRestaurant(restaurantId: String): Flow<RestaurantUI?> {
        return restaurantDao.observeRestaurant(restaurantId).map { it?.toUi() }
    }

    override suspend fun deleteRestaurant(id: String) =
        withContext(Dispatchers.IO) { restaurantDao.deleteRestaurant(id) }
}