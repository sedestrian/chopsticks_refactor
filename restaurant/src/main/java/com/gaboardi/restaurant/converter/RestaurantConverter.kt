package com.gaboardi.restaurant.converter

import com.gaboardi.restaurant.model.api.PriceRequest
import com.gaboardi.restaurant.model.api.PriceResult
import com.gaboardi.restaurant.model.api.RestaurantResult
import com.gaboardi.restaurant.model.db.PriceEntity
import com.gaboardi.restaurant.model.db.RestaurantData
import com.gaboardi.restaurant.model.db.RestaurantEntity
import com.gaboardi.restaurant.model.db.RestaurantInsertEntity
import com.gaboardi.restaurant.model.ui.Price
import com.gaboardi.restaurant.model.ui.PriceUI
import com.gaboardi.restaurant.model.ui.Restaurant
import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.gaboardi.table.converter.toUi


fun PriceEntity.toUi(): Price {
    return Price(
        id,
        restaurantId,
        cost,
        time,
        days
    )
}

fun PriceUI.toRequest(): PriceRequest? {
    return if (id != null && time != null && days != null) {
        PriceRequest(
            id!!,
            costDouble,
            time!!.id,
            days!!.id
        )
    } else
        null
}

fun RestaurantData.toUi(): RestaurantUI {
    return RestaurantUI(
        id,
        name,
        address,
        phone,
        website,
        category,
        averageRating,
        prices.map { it.toUi() },
        tables.map { it.toUi() }
    )
}

fun RestaurantEntity.toUi(): Restaurant {
    return Restaurant(
        id,
        name,
        address,
        phone,
        website,
        category,
        averageRating
    )
}

fun RestaurantResult.toInsertEntity(): RestaurantInsertEntity {
    return RestaurantInsertEntity(
        this.toEntity(),
        prices?.map { it.toPriceEntity(id) } ?: listOf()
    )
}

fun RestaurantResult.toEntity(): RestaurantEntity {
    return RestaurantEntity(
        id,
        userId,
        name,
        address,
        phoneNumber,
        url,
        category,
        averageRating
    )
}

fun PriceResult.toPriceEntity(restaurantId: String): PriceEntity {
    return PriceEntity(
        priceId,
        restaurantId,
        cost,
        time,
        days
    )
}