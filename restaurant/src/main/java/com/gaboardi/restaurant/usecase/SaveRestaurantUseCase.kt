package com.gaboardi.restaurant.usecase

import com.gaboardi.restaurant.model.api.PriceRequest
import com.gaboardi.restaurant.repository.RestaurantRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class SaveRestaurantUseCase @Inject constructor(
    private val repository: RestaurantRepository
) {
    @ExperimentalCoroutinesApi
    fun saveRestaurant(
        name: String,
        address: String,
        telephone: String?,
        website: String?,
        type: Int,
        prices: List<PriceRequest>
    ) =
        repository.createRestaurant(
            name,
            address,
            telephone,
            website,
            type,
            prices
        )
}