package com.gaboardi.restaurant.usecase

import com.gaboardi.restaurant.repository.RestaurantRepository
import javax.inject.Inject

class SubscribeToRestaurantUseCase @Inject constructor(
    private val restaurantRepository: RestaurantRepository
) {
    fun execute(restaurantId: String) = restaurantRepository.subscribeToRestaurant(restaurantId)
}