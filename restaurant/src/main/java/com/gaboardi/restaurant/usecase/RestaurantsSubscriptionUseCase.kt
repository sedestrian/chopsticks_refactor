package com.gaboardi.restaurant.usecase

import com.gaboardi.restaurant.repository.RestaurantRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class RestaurantsSubscriptionUseCase @Inject constructor(
    private val restaurantsRepository: RestaurantRepository
) {
    @ExperimentalCoroutinesApi
    fun startDatabase() = restaurantsRepository.subscribeToFirestore()
}