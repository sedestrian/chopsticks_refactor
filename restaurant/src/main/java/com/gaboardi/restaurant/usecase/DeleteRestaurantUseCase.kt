package com.gaboardi.restaurant.usecase

import com.gaboardi.common.model.State
import com.gaboardi.restaurant.repository.RestaurantRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DeleteRestaurantUseCase @Inject constructor(
    private val restaurantsRepository: RestaurantRepository
) {
    suspend fun delete(restaurantId: String): Flow<State> {
        return restaurantsRepository.deleteRestaurant(restaurantId)
    }
}