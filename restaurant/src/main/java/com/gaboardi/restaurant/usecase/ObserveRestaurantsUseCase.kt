package com.gaboardi.restaurant.usecase

import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.gaboardi.restaurant.model.ui.SortMenuItem
import com.gaboardi.restaurant.repository.RestaurantRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ObserveRestaurantsUseCase @Inject constructor(
    private val restaurantsRepository: RestaurantRepository
) {
    fun observeRestaurants(sortItem: SortMenuItem): Flow<List<RestaurantUI>> {
        return restaurantsRepository.observeRestaurants().map { restaurants ->
            when (sortItem) {
                SortMenuItem.RATING_ASC -> {
                    restaurants.sortedBy { it.averageRating }
                }
                SortMenuItem.RATING_DESC -> {
                    restaurants.sortedByDescending { it.averageRating }
                }
                SortMenuItem.DATE_ASC -> {
                    restaurants
                }
                SortMenuItem.DATE_DESC -> {
                    restaurants
                }
                SortMenuItem.NAME_ASC -> {
                    restaurants.sortedWith(
                        compareBy(
                            String.CASE_INSENSITIVE_ORDER,
                            { it.name })
                    )
                }
                SortMenuItem.NAME_DESC -> {
                    restaurants.sortedWith(
                        compareByDescending(
                            String.CASE_INSENSITIVE_ORDER,
                            { it.name })
                    )
                }
            }
        }
    }
}