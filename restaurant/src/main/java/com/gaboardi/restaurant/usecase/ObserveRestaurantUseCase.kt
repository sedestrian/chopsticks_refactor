package com.gaboardi.restaurant.usecase

import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.gaboardi.restaurant.repository.RestaurantRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ObserveRestaurantUseCase @Inject constructor(
    private val repository: RestaurantRepository
) {
    fun observeRestaurant(restaurantId: String): Flow<RestaurantUI?> {
        return repository.observeRestaurant(restaurantId)
    }
}