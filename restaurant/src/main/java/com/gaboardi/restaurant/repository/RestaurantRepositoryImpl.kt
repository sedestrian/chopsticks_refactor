package com.gaboardi.restaurant.repository

import com.gaboardi.common.model.ApiResponse
import com.gaboardi.common.model.ErrorMessage
import com.gaboardi.common.model.State
import com.gaboardi.restaurant.datasource.local.LocalRestaurantDataSource
import com.gaboardi.restaurant.datasource.remote.RemoteRestaurantDataSource
import com.gaboardi.restaurant.model.api.PriceRequest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RestaurantRepositoryImpl @Inject constructor(
    private val remoteRestaurantDataSource: RemoteRestaurantDataSource,
    private val localRestaurantDataSource: LocalRestaurantDataSource
) : RestaurantRepository {
    override fun observeRestaurants() = localRestaurantDataSource.observeRestaurants()

    override fun observeRestaurant(restaurantId: String) =
        localRestaurantDataSource.observeRestaurant(restaurantId)

    override fun createRestaurant(
        name: String,
        address: String,
        telephone: String?,
        website: String?,
        type: Int,
        prices: List<PriceRequest>
    ) = flow {
        emit(State.loading())
        val result = remoteRestaurantDataSource.createRestaurant(
            name,
            address,
            telephone,
            website,
            type,
            prices
        )
        when (result) {
            is ApiResponse.Success -> emit(State.success())
            is ApiResponse.Error -> emit(State.error(result.message))
        }
    }

    @ExperimentalCoroutinesApi
    override suspend fun deleteRestaurant(restaurantId: String) = flow {
        emit(State.loading())
        val result = remoteRestaurantDataSource.deleteRestaurant(restaurantId)
        when (result) {
            is ApiResponse.Success -> emit(State.success())
            is ApiResponse.Error -> emit(State.error(result.message))
        }
    }

    @ExperimentalCoroutinesApi
    override fun subscribeToFirestore() = flow {
        remoteRestaurantDataSource.subscribeToFirestore().collect { result ->
            when (result) {
                is ApiResponse.Success -> {
                    try {
                        val data = result.body
                        localRestaurantDataSource.saveRestaurants(data)
                        emit(State.success())
                    } catch (e: Exception) {
                        emit(State.error(ErrorMessage.of(e.localizedMessage)))
                    }
                }
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        }
    }

    @ExperimentalCoroutinesApi
    override fun subscribeToRestaurant(restaurantId: String) = flow {
        remoteRestaurantDataSource.subscribeToRestaurant(restaurantId).collect { result ->
            when (result) {
                is ApiResponse.Success -> {
                    try {
                        val data = result.body
                        localRestaurantDataSource.saveRestaurant(data)
                        emit(State.success())
                    } catch (e: Exception) {
                        emit(State.error(ErrorMessage.of(e.localizedMessage)))
                    }
                }
                is ApiResponse.Error -> emit(State.error(result.message))
            }
        }
    }
}