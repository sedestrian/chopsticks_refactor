package com.gaboardi.restaurant.repository

import com.gaboardi.common.model.State
import com.gaboardi.restaurant.model.api.PriceRequest
import com.gaboardi.restaurant.model.ui.RestaurantUI
import kotlinx.coroutines.flow.Flow

interface RestaurantRepository {
    fun subscribeToFirestore(): Flow<State>
    fun subscribeToRestaurant(restaurantId: String): Flow<State>
    fun observeRestaurants(): Flow<List<RestaurantUI>>
    suspend fun deleteRestaurant(id: String): Flow<State>
    fun observeRestaurant(restaurantId: String): Flow<RestaurantUI?>
    fun createRestaurant(
        name: String,
        address: String,
        telephone: String?,
        website: String?,
        type: Int,
        prices: List<PriceRequest>
    ): Flow<State>
}