package com.gaboardi.chopsticks.di.modules

import com.gaboardi.restaurant.datasource.local.LocalRestaurantDataSource
import com.gaboardi.restaurant.datasource.local.LocalRestaurantDataSourceImpl
import com.gaboardi.restaurant.datasource.remote.RemoteRestaurantDataSource
import com.gaboardi.restaurant.datasource.remote.RemoteRestaurantDataSourceImpl
import com.gaboardi.restaurant.repository.RestaurantRepository
import com.gaboardi.restaurant.repository.RestaurantRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RestaurantModule {
    @Binds
    abstract fun bindRemoteDataSource(
        implementation: RemoteRestaurantDataSourceImpl
    ): RemoteRestaurantDataSource

    @Binds
    abstract fun bindLocalDataSource(
        implementation: LocalRestaurantDataSourceImpl
    ): LocalRestaurantDataSource

    @Binds
    abstract fun bindRepository(
        implementation: RestaurantRepositoryImpl
    ): RestaurantRepository
}