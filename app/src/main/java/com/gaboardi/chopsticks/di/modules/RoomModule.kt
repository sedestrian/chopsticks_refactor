package com.gaboardi.chopsticks.di.modules

import android.content.Context
import androidx.room.Room
import com.gaboardi.chopsticks.database.AppDatabase
import com.gaboardi.plate.dao.PlateDao
import com.gaboardi.restaurant.dao.RestaurantDao
import com.gaboardi.table.dao.TableDao
import com.gaboardi.user.dao.DatabaseDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Singleton
    @Provides
    fun provideRoom(
        @ApplicationContext context: Context
    ): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, context.packageName)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideRestaurantsDao(
        database: AppDatabase
    ): RestaurantDao {
        return database.restaurantDao()
    }

    @Singleton
    @Provides
    fun providePlatesDao(
        database: AppDatabase
    ): PlateDao {
        return database.plateDao()
    }

    @Singleton
    @Provides
    fun provideDatabaseDao(
        database: AppDatabase
    ): DatabaseDao {
        return database.databaseDao()
    }

    @Singleton
    @Provides
    fun provideTablesDao(
        database: AppDatabase
    ): TableDao {
        return database.tablesDao()
    }
}