package com.gaboardi.chopsticks.di.modules

import com.gaboardi.plate.datasource.local.LocalPlateDataSource
import com.gaboardi.plate.datasource.local.LocalPlateDataSourceImpl
import com.gaboardi.plate.datasource.remote.RemotePlateDataSource
import com.gaboardi.plate.datasource.remote.RemotePlateDataSourceImpl
import com.gaboardi.plate.repository.PlateRepository
import com.gaboardi.plate.repository.PlateRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class PlateModule {
    @Binds
    abstract fun bindRemoteDataSource(
        implementation: RemotePlateDataSourceImpl
    ): RemotePlateDataSource

    @Binds
    abstract fun bindLocalDataSource(
        implementation: LocalPlateDataSourceImpl
    ): LocalPlateDataSource

    @Binds
    abstract fun bindRepository(
        implementation: PlateRepositoryImpl
    ): PlateRepository
}