package com.gaboardi.chopsticks.di.modules

import com.gaboardi.user.datasource.local.UserLocalDataSource
import com.gaboardi.user.datasource.local.UserLocalDataSourceImpl
import com.gaboardi.user.datasource.remote.UserRemoteDataSource
import com.gaboardi.user.datasource.remote.UserRemoteDataSourceImpl
import com.gaboardi.user.repository.UserRepository
import com.gaboardi.user.repository.UserRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class UserModule {
    @Binds
    abstract fun bindRemoteDataSource(
        implementation: UserRemoteDataSourceImpl
    ): UserRemoteDataSource

    @Binds
    abstract fun bindLocalDataSource(
        implementation: UserLocalDataSourceImpl
    ): UserLocalDataSource

    @Binds
    abstract fun bindRepository(
        implementation: UserRepositoryImpl
    ): UserRepository
}