package com.gaboardi.chopsticks

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ChopsticksApp: Application()