package com.gaboardi.chopsticks.database

import androidx.room.TypeConverter
import java.time.LocalDateTime
import java.time.ZoneOffset

class TimeConverters {
    @TypeConverter
    fun localDateTimeToLong(date: LocalDateTime): Long = date.toEpochSecond(ZoneOffset.UTC)

    @TypeConverter
    fun longToLocalDateTime(date: Long): LocalDateTime =
        LocalDateTime.ofEpochSecond(date, 0, ZoneOffset.UTC)
}