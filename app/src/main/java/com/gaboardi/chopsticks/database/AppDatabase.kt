package com.gaboardi.chopsticks.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.gaboardi.plate.dao.PlateDao
import com.gaboardi.plate.model.db.PlateEntity
import com.gaboardi.restaurant.dao.RestaurantDao
import com.gaboardi.restaurant.model.db.PriceEntity
import com.gaboardi.restaurant.model.db.RestaurantData
import com.gaboardi.restaurant.model.db.RestaurantEntity
import com.gaboardi.table.dao.TableDao
import com.gaboardi.table.model.db.*
import com.gaboardi.user.dao.DatabaseDao

@TypeConverters(TimeConverters::class)
@Database(
    entities = [
        RestaurantEntity::class,
        PriceEntity::class,
        PlateEntity::class,
        TableEntity::class,
        TablePlateEntity::class,
        TableUserEntity::class,
        TableRestaurantJunction::class
    ],
    views = [RestaurantData::class, TableData::class, TableUserData::class, PlateData::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun restaurantDao(): RestaurantDao
    abstract fun plateDao(): PlateDao
    abstract fun databaseDao(): DatabaseDao
    abstract fun tablesDao(): TableDao
}
