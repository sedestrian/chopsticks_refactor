
import AppDependencies.implementApp
import AppDependencies.implementCameraX
import AppDependencies.implementCoroutines
import AppDependencies.implementHilt
import AppDependencies.implementNavigation
import AppDependencies.implementOrbit

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-android")
}

android {
    compileSdk = AppConfig.compileSdk

    defaultConfig {
        minSdk = AppConfig.minSdk
        targetSdk = AppConfig.targetSdk

        testInstrumentationRunner = AppConfig.androidTestInstrumentation
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementBom(AppDependencies.firebaseBom)

    implementApp()
    implementHilt()
    implementNavigation()
    implementOrbit()
    implementCameraX()
    implementCoroutines()

    implementation(AppDependencies.constraintLayout)
    implementation(AppDependencies.material)
    implementation(AppDependencies.swipeRefresh)
    implementation(AppDependencies.qrScanner)
    implementation(AppDependencies.dynamicLinks)

    coreLibraryDesugaring(AppDependencies.coreDesugaring)

    implementModule(AppDependencies.Modules.navigation)
    implementModule(AppDependencies.Modules.common)
    implementModule(AppDependencies.Modules.commonui)
    implementModule(AppDependencies.Modules.qrcode)
    implementModule(AppDependencies.Modules.table)
    implementModule(AppDependencies.Modules.restaurant)
}