package com.gaboardi.tablejoin.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gaboardi.tablejoin.R
import com.gaboardi.tablejoin.databinding.FragmentJoinTableBinding
import com.gaboardi.tablejoin.mvi.JoinTableSideEffect
import com.gaboardi.tablejoin.processing.QrImageAnalyzer
import com.gaboardi.tablejoin.viewmodel.JoinTableViewModel
import com.google.mlkit.vision.barcode.Barcode
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@AndroidEntryPoint
class JoinTableFragment : Fragment() {
    private val args: JoinTableFragmentArgs by navArgs()
    private val viewModel: JoinTableViewModel by viewModels()
    private val cameraExecutor: ExecutorService by lazy { Executors.newSingleThreadExecutor() }
    private val imageAnalyzer by lazy {
        ImageAnalysis.Builder()
            .build()
            .also {
                it.setAnalyzer(cameraExecutor, QrImageAnalyzer(::onQrFound))
            }
    }

    private lateinit var binding: FragmentJoinTableBinding

    private val permissionsRequestLauncher =
        registerForActivityResult(RequestPermission()) { isGranted ->
            if (isGranted)
                startCamera()
            else
                disableCamera()
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentJoinTableBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launchWhenCreated {
            viewModel.container.sideEffectFlow.collect { sideEffect ->
                when (sideEffect) {
                    is JoinTableSideEffect.JoinTable -> joinTable(sideEffect.code)
                }
            }
        }

        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED -> {
                startCamera()
            }
            shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) -> {
            }
            else -> {
                permissionsRequestLauncher.launch(
                    Manifest.permission.CAMERA
                )
            }
        }
    }

    private fun disableCamera() {

    }

    private fun onQrFound(codes: List<Barcode>) {
        viewModel.gotQrCodes(codes)
    }

    private fun joinTable(code: String) {
        val navController = findNavController()
        val action = JoinTableFragmentDirections.actionJoinTableFragmentToTableJoinedFragment(
            code,
            args.restaurantId
        )
        if (navController.currentDestination?.id == R.id.joinTableFragment)
            navController.navigate(action)
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener({
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(binding.previewView.surfaceProvider)
                }

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(
                    viewLifecycleOwner,
                    cameraSelector,
                    preview,
                    imageAnalyzer
                )
            } catch (exc: Exception) {
                println("Use case binding failed: $exc")
            }
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }
}