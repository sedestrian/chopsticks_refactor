package com.gaboardi.tablejoin.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.gaboardi.tablejoin.databinding.FragmentTableJoinedBinding
import com.gaboardi.tablejoin.viewmodel.TableJoinedViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TableJoinedFragment : Fragment() {
    private lateinit var binding: FragmentTableJoinedBinding

    private val args: TableJoinedFragmentArgs by navArgs()
    private val viewModel: TableJoinedViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTableJoinedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.joinTable(args.code, args.restaurantId)
    }
}