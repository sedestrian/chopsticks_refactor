package com.gaboardi.tablejoin.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.OnApplyWindowInsetsListener
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gaboardi.tablejoin.databinding.FragmentChooseTableRestaurantBinding
import com.gaboardi.tablejoin.viewmodel.ChooseRestaurantViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ChooseTableRestaurantFragment : Fragment() {
    private lateinit var binding: FragmentChooseTableRestaurantBinding

    private val args: ChooseTableRestaurantFragmentArgs by navArgs()
    private val viewModel: ChooseRestaurantViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChooseTableRestaurantBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        viewModel.loadCode(args.code)

        lifecycleScope.launchWhenCreated {
            viewModel.container.stateFlow.collect {
                binding.textView2.text = it.originalRestaurant?.name
                if(it.code != null && it.originalRestaurant?.id != null) {
                    val action =
                        ChooseTableRestaurantFragmentDirections.actionChooseTableRestaurantFragmentToTableJoinedFragment(
                            it.code, it.originalRestaurant.id
                        )
                    findNavController().navigate(action)
                }
            }
        }
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val window = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        view.updatePadding(top = window.top, bottom = window.bottom)
        insets
    }
}