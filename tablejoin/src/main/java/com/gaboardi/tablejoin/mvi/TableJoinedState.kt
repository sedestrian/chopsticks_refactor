package com.gaboardi.tablejoin.mvi

data class TableJoinedState(
    val nothing: Boolean = true
)