package com.gaboardi.tablejoin.mvi

sealed class JoinTableSideEffect {
    data class JoinTable(val code: String): JoinTableSideEffect()
}