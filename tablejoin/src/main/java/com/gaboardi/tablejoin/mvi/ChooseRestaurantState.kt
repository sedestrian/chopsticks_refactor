package com.gaboardi.tablejoin.mvi

import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.gaboardi.table.model.ui.Table

data class ChooseRestaurantState(
    val code: String? = null,
    val table: Table? = null,
    val originalRestaurant: RestaurantUI? = null,
)