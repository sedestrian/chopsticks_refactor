package com.gaboardi.tablejoin.mvi

data class JoinTableState(
    val code: String? = null
)