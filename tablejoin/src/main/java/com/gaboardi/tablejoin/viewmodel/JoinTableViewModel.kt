package com.gaboardi.tablejoin.viewmodel

import android.net.Uri
import androidx.lifecycle.ViewModel
import com.gaboardi.tablejoin.mvi.JoinTableSideEffect
import com.gaboardi.tablejoin.mvi.JoinTableState
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.mlkit.vision.barcode.Barcode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.tasks.await
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class JoinTableViewModel @Inject constructor(
    private val dynamicLinks: FirebaseDynamicLinks
) : ViewModel(), ContainerHost<JoinTableState, JoinTableSideEffect> {
    override val container: Container<JoinTableState, JoinTableSideEffect> = container(
        JoinTableState()
    )

    fun gotQrCodes(codes: List<Barcode>) = intent {
        val dyn = codes.firstOrNull { it.rawValue?.contains("chopsticks.page.link") == true }
        dyn?.rawValue?.let { link ->
            val dynamicLinkUri = Uri.parse(link)
            val dynamicLink = dynamicLinks.getDynamicLink(dynamicLinkUri).await()
            val code = dynamicLink.link?.lastPathSegment
            code?.let {
                postSideEffect(JoinTableSideEffect.JoinTable(it))
            }
        }
    }
}