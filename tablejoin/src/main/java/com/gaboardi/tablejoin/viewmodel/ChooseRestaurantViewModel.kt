package com.gaboardi.tablejoin.viewmodel

import android.net.Uri
import androidx.lifecycle.ViewModel
import com.gaboardi.restaurant.usecase.ObserveRestaurantUseCase
import com.gaboardi.restaurant.usecase.SubscribeToRestaurantUseCase
import com.gaboardi.table.usecase.GetTableFromCodeUseCase
import com.gaboardi.table.usecase.SubscribeToTableWithCodeUseCase
import com.gaboardi.tablejoin.mvi.ChooseRestaurantSideEffect
import com.gaboardi.tablejoin.mvi.ChooseRestaurantState
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class ChooseRestaurantViewModel @Inject constructor(
    private val dynamicLinks: FirebaseDynamicLinks,
    private val getTableFromCode: GetTableFromCodeUseCase,
    private val subscribeToTable: SubscribeToTableWithCodeUseCase,
    private val subscribeToRestaurantUseCase: SubscribeToRestaurantUseCase,
    private val observeRestaurantUseCase: ObserveRestaurantUseCase
) : ViewModel(), ContainerHost<ChooseRestaurantState, ChooseRestaurantSideEffect> {
    override val container: Container<ChooseRestaurantState, ChooseRestaurantSideEffect> =
        container(
            ChooseRestaurantState()
        )

    fun loadCode(link: String) = intent {
        val dynamicLinkUri = Uri.parse(link)
        val dynamicLink = dynamicLinks.getDynamicLink(dynamicLinkUri).await()
        val code = dynamicLink.link?.lastPathSegment
        if (code != null) {
            getTable(code)
        }
    }

    private suspend fun getTable(code: String) {
        coroutineScope {
            launch { observeTable(code) }
            launch { subscribeToTable(code) }
        }
    }

    private suspend fun subscribeToTable(tableCode: String) {
        subscribeToTable.execute(tableCode).collect { }
    }

    private suspend fun observeTable(tableCode: String) {
        getTableFromCode.execute(tableCode).collect { result ->
            result?.let { table ->
                /*val creator = table.creatorId
                val order = table.users.firstOrNull { user -> user.userId == creator }?.orderId
                order?.let { orderId ->
                    getOrder(orderId)
                }*/
            }
        }
    }

    private suspend fun getOrder(orderId: String) {
        coroutineScope {
            launch { observeOrder(orderId) }
            launch { subscribeToOrder(orderId) }
        }
    }

    private suspend fun observeOrder(orderId: String) {
        /*observeOrderUseCase.observe(orderId).collect {
            getRestaurant(it.restaurantId)
        }*/
    }

    private suspend fun subscribeToOrder(orderId: String) {
//        subscribeToOrder.execute(orderId).collect { }
    }

    private suspend fun getRestaurant(restaurantId: String) {
        coroutineScope {
            launch { observeRestaurant(restaurantId) }
            launch { subscribeToRestaurant(restaurantId) }
        }
    }

    private suspend fun observeRestaurant(restaurantId: String) = intent {
        observeRestaurantUseCase.observeRestaurant(restaurantId).collect {
            reduce { state.copy(originalRestaurant = it) }
        }
    }

    private suspend fun subscribeToRestaurant(restaurantId: String) {
        subscribeToRestaurantUseCase.execute(restaurantId).collect { }
    }
}