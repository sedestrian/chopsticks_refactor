package com.gaboardi.tablejoin.viewmodel

import androidx.lifecycle.ViewModel
import com.gaboardi.table.usecase.JoinTableUseCase
import com.gaboardi.tablejoin.mvi.TableJoinedSideEffect
import com.gaboardi.tablejoin.mvi.TableJoinedState
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.coroutines.transformFlow
import org.orbitmvi.orbit.syntax.strict.orbit
import org.orbitmvi.orbit.syntax.strict.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class TableJoinedViewModel @Inject constructor(
    private val joinTableUseCase: JoinTableUseCase,
) : ViewModel(), ContainerHost<TableJoinedState, TableJoinedSideEffect> {
    override val container: Container<TableJoinedState, TableJoinedSideEffect> = container(
        TableJoinedState()
    )

    fun joinTable(code: String, restaurantId: String) = orbit {
        transformFlow {
            joinTableUseCase(code, restaurantId)
        }.reduce {
            state
        }
    }
}