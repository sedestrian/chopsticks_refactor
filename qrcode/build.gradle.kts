import AppDependencies.implementRoom
import AppDependencies.implementHilt
import AppDependencies.implementCoroutines
import AppDependencies.implementRetrofit

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdk = AppConfig.compileSdk

    defaultConfig {
        minSdk = AppConfig.minSdk
        targetSdk = AppConfig.targetSdk

        testInstrumentationRunner = AppConfig.androidTestInstrumentation
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementBom(AppDependencies.firebaseBom)

    implementHilt()
    implementCoroutines()
    implementRetrofit()
//    implementation 'androidx.core:core-ktx:1.5.0-beta01'

    implementation(AppDependencies.firebaseCommon)
    implementation(AppDependencies.firebaseAuth)
    implementation(AppDependencies.firestore)
    implementation(AppDependencies.dynamicLinks)
    implementation(AppDependencies.zxing)

    coreLibraryDesugaring(AppDependencies.coreDesugaring)

    implementModule(AppDependencies.Modules.common)
    implementModule(AppDependencies.Modules.commonui)
}