package com.gaboardi.qrcode

import android.graphics.*
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import com.google.zxing.qrcode.encoder.ByteMatrix
import com.google.zxing.qrcode.encoder.Encoder
import com.google.zxing.qrcode.encoder.QRCode
import java.util.*

object AwesomeQrRenderer {
    /**
     * For more information about QR code, refer to: https://en.wikipedia.org/wiki/QR_code
     * BYTE_EPT: Empty block
     * BYTE_DTA: Data block
     * BYTE_POS: Position block
     * BYTE_AGN: Align block
     * BYTE_TMG: Timing block
     * BYTE_PTC: Protector block, translucent layer (custom block, this is not included in QR code's standards)
     */
    private const val BYTE_EPT = 0x0.toByte()
    private const val BYTE_DTA = 0x1.toByte()
    private const val BYTE_POS = 0x2.toByte()
    private const val BYTE_AGN = 0x3.toByte()
    private const val BYTE_TMG = 0x4.toByte()
    private const val BYTE_PTC = 0x5.toByte()

    @JvmStatic
    @Throws(Exception::class)
    suspend fun render(renderOptions: RenderOption): Bitmap {
        return renderFrame(renderOptions)
    }

    /*@JvmStatic
    suspend fun renderAsync(
        renderOptions: RenderOption,
        resultCallback: ((Bitmap) -> Unit)?,
        errorCallback: ((Exception) -> Unit)?
    ) {
        Thread {
            try {
                val renderResult = render(renderOptions)
                resultCallback?.invoke(renderResult)
            } catch (e: Exception) {
                errorCallback?.invoke(e)
            }
        }.start()
    }*/

    /**
     * The general render function.
     *
     * @return a Bitmap if success when under still background image mode
     * otherwise will return null if success
     * @throws Exception ONLY thrown when an error occurred
     */
    @Throws(Exception::class)
    private fun renderFrame(renderOptions: RenderOption): Bitmap {
        if (renderOptions.content.isEmpty()) {
            throw IllegalArgumentException("Error: content is empty. (content.isEmpty())")
        }
        if (renderOptions.size < 0) {
            throw IllegalArgumentException("Error: a negative size is given. (size < 0)")
        }
        if (renderOptions.borderWidth < 0) {
            throw IllegalArgumentException("Error: a negative borderWidth is given. (borderWidth < 0)")
        }
        if (renderOptions.size - 2 * renderOptions.borderWidth <= 0) {
            throw IllegalArgumentException("Error: there is no space left for the QRCode. (size - 2 * borderWidth <= 0)")
        }
        val byteMatrix = getByteMatrix(renderOptions.content, renderOptions.ecl)
            ?: throw NullPointerException("Error: ByteMatrix based on content is null. (getByteMatrix(content, ecl) == null)")
        val innerRenderedSize = renderOptions.size - 2 * renderOptions.borderWidth
        val nCount = byteMatrix.width
        val nSize = Math.round(innerRenderedSize.toFloat() / nCount) // Avoid non-integer values
        val unscaledInnerRenderSize = nSize * nCount // Draw on unscaled Bitmap first
        val unscaledFullRenderSize =
            unscaledInnerRenderSize + 2 * renderOptions.borderWidth // Draw on unscaled Bitmap first

        if (renderOptions.size - 2 * renderOptions.borderWidth < byteMatrix.width) {
            throw IllegalArgumentException("Error: there is no space left for the QRCode. (size - 2 * borderWidth < " + byteMatrix.width + ")")
        }

        val unscaledFullRenderedBitmap = Bitmap.createBitmap(
            unscaledFullRenderSize,
            unscaledFullRenderSize,
            Bitmap.Config.ARGB_8888
        )

        val paintDark = Paint().apply {
            color = renderOptions.color
            isAntiAlias = true
            style = Paint.Style.FILL
        }

        val paintPosition = Paint().apply {
            color = renderOptions.color
            style = Paint.Style.STROKE
            strokeWidth = nSize.toFloat()
            isAntiAlias = true
            isDither = true
        }

        val unscaledCanvas = Canvas(unscaledFullRenderedBitmap)

        drawPositionals(unscaledCanvas, paintPosition, paintDark, nSize, renderOptions)

        for (row in 0 until byteMatrix.height) {
            for (col in 0 until byteMatrix.width) {
                when (byteMatrix.get(col, row)) {
                    BYTE_AGN -> drawFancyBit(
                        unscaledCanvas,
                        renderOptions,
                        col,
                        row,
                        nSize,
                        byteMatrix,
                        paintDark
                    )
                    BYTE_TMG -> drawFancyBit(
                        unscaledCanvas,
                        renderOptions,
                        col,
                        row,
                        nSize,
                        byteMatrix,
                        paintDark
                    )
                    BYTE_DTA -> drawFancyBit(
                        unscaledCanvas,
                        renderOptions,
                        col,
                        row,
                        nSize,
                        byteMatrix,
                        paintDark
                    )
                }
            }
        }

        val renderedScaledBitmap = Bitmap.createBitmap(
            renderOptions.size,
            renderOptions.size,
            Bitmap.Config.ARGB_8888
        )

        val scaledCanvas = Canvas(renderedScaledBitmap)
        scaledCanvas.drawBitmap(
            unscaledFullRenderedBitmap,
            null,
            Rect(0, 0, renderedScaledBitmap.width, renderedScaledBitmap.height),
            paintDark
        )

        unscaledFullRenderedBitmap.recycle()

        return renderedScaledBitmap
    }

    private fun drawPositionals(
        unscaledCanvas: Canvas,
        paintPosition: Paint,
        paintDark: Paint,
        nSize: Int,
        renderOptions: RenderOption
    ) {
        unscaledCanvas.drawRoundRect(
            renderOptions.borderWidth.toFloat() + (nSize / 2),
            renderOptions.borderWidth.toFloat() + (nSize / 2),
            (renderOptions.borderWidth + 7f * nSize) - nSize / 2,
            (renderOptions.borderWidth + 7f * nSize) - nSize / 2,
            2f * nSize,
            2f * nSize,
            paintPosition
        )
        unscaledCanvas.drawCircle(
            (renderOptions.borderWidth + 3.5 * nSize).toFloat(),
            (renderOptions.borderWidth + 3.5 * nSize).toFloat(),
            1.5f * nSize,
            paintDark
        )

        unscaledCanvas.drawRoundRect(
            unscaledCanvas.width - renderOptions.borderWidth - 7f * nSize + nSize / 2,
            renderOptions.borderWidth + nSize / 2f,
            unscaledCanvas.width - renderOptions.borderWidth - nSize / 2f,
            (renderOptions.borderWidth + 7f * nSize) - nSize / 2,
            2f * nSize,
            2f * nSize,
            paintPosition
        )

        unscaledCanvas.drawCircle(
            (unscaledCanvas.width - 3.5 * nSize - renderOptions.borderWidth).toFloat(),
            (renderOptions.borderWidth + 3.5 * nSize).toFloat(),
            1.5f * nSize,
            paintDark
        )

        unscaledCanvas.drawRoundRect(
            renderOptions.borderWidth.toFloat() + (nSize / 2),
            unscaledCanvas.height - renderOptions.borderWidth - 7f * nSize + nSize / 2,
            (renderOptions.borderWidth + 7f * nSize) - nSize / 2,
            unscaledCanvas.height - renderOptions.borderWidth - nSize / 2f,
            2f * nSize,
            2f * nSize,
            paintPosition
        )

        unscaledCanvas.drawCircle(
            (renderOptions.borderWidth + 3.5 * nSize).toFloat(),
            (unscaledCanvas.width - 3.5 * nSize - renderOptions.borderWidth).toFloat(),
            1.5f * nSize,
            paintDark
        )
    }

    private fun drawFancyBit(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        byteMatrix: ByteMatrix,
        paint: Paint
    ) {
        val left = if (col == 0) BYTE_EPT else byteMatrix.get(col - 1, row)
        val top = if (row == 0) BYTE_EPT else byteMatrix.get(col, row - 1)
        val right = if (col == byteMatrix.width - 1) BYTE_EPT else byteMatrix.get(col + 1, row)
        val bottom = if (row == byteMatrix.height - 1) BYTE_EPT else byteMatrix.get(col, row + 1)
        when {
            !left.occupied && !top.occupied && !right.occupied && !bottom.occupied -> {
                drawSingleBit(canvas, renderOptions, col, row, nSize, paint)
            }
            left.occupied && top.occupied && right.occupied && bottom.occupied -> {
                drawFullBit(canvas, renderOptions, col, row, nSize, paint)
            }
            threeSides(left, top, right, bottom) -> {
                drawFullBit(canvas, renderOptions, col, row, nSize, paint)
            }
            oppositeSides(left, top, right, bottom) -> {
                drawFullBit(canvas, renderOptions, col, row, nSize, paint)
            }
            !left.occupied && !top.occupied && !right.occupied && bottom.occupied -> {
                drawEndBitUpright(canvas, renderOptions, col, row, nSize, paint)
            }
            !left.occupied && top.occupied && !right.occupied && !bottom.occupied -> {
                drawEndBitUpsideDown(canvas, renderOptions, col, row, nSize, paint)
            }
            left.occupied && !top.occupied && !right.occupied && !bottom.occupied -> {
                drawEndBitToTheRight(canvas, renderOptions, col, row, nSize, paint)
            }
            !left.occupied && !top.occupied && right.occupied && !bottom.occupied -> {
                drawEndBitToTheLeft(canvas, renderOptions, col, row, nSize, paint)
            }
            !left.occupied && !top.occupied && right.occupied && bottom.occupied -> {
                drawAngledBottomRight(canvas, renderOptions, col, row, nSize, paint)
            }
            !left.occupied && top.occupied && right.occupied && !bottom.occupied -> {
                drawAngledTopRight(canvas, renderOptions, col, row, nSize, paint)
            }
            left.occupied && top.occupied && !right.occupied && !bottom.occupied -> {
                drawAngledTopLeft(canvas, renderOptions, col, row, nSize, paint)
            }
            left.occupied && !top.occupied && !right.occupied && bottom.occupied -> {
                drawAngledBottomLeft(canvas, renderOptions, col, row, nSize, paint)
            }
            else -> {
                drawFullBit(canvas, renderOptions, col, row, nSize, paint)
            }
        }
    }

    private val Byte.occupied: Boolean
        get() {
            return this != BYTE_EPT && this != BYTE_POS && this != BYTE_PTC
        }

    private fun oppositeSides(
        left: Byte,
        top: Byte,
        right: Byte,
        bottom: Byte
    ): Boolean {
        return !left.occupied && top.occupied && !right.occupied && bottom.occupied ||
                left.occupied && !top.occupied && right.occupied && !bottom.occupied
    }

    private fun threeSides(
        left: Byte,
        top: Byte,
        right: Byte,
        bottom: Byte
    ): Boolean {
        return !left.occupied && top.occupied && right.occupied && bottom.occupied ||
                left.occupied && !top.occupied && right.occupied && bottom.occupied ||
                left.occupied && top.occupied && !right.occupied && bottom.occupied ||
                left.occupied && top.occupied && right.occupied && !bottom.occupied
    }

    private fun drawSingleBit(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        canvas.save()
        canvas.rotate(
            -45f,
            renderOptions.borderWidth + (col + 0.5f) * nSize,
            renderOptions.borderWidth + (row + 0.5f) * nSize
        )
        canvas.drawOval(
            renderOptions.borderWidth + (col + 0.15f) * nSize,
            renderOptions.borderWidth + (row - 0.1f) * nSize.toFloat(),
            renderOptions.borderWidth + (col + 0.85f) * nSize,
            renderOptions.borderWidth + (row + 1.1f) * nSize,
            paint
        )
        canvas.restore()
    }

    private fun drawFullBit(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        canvas.drawRect(
            renderOptions.borderWidth + col * nSize.toFloat(),
            renderOptions.borderWidth + row * nSize.toFloat(),
            renderOptions.borderWidth + (col + 1f) * nSize,
            renderOptions.borderWidth + (row + 1f) * nSize,
            paint
        )
    }

    private fun drawAngledBottomRight(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        val zeroX = renderOptions.borderWidth + col * nSize.toFloat()
        val zeroY = renderOptions.borderWidth + row * nSize.toFloat()
        val path = Path()
        path.moveTo(zeroX, zeroY + nSize)
        path.lineTo(zeroX, zeroY + nSize * 0.3f)
        path.arcTo(
            zeroX,
            zeroY,
            zeroX + nSize * 0.6f,
            zeroY + nSize * 0.6f,
            180f, 90f, true
        )
        path.lineTo(zeroX + nSize, zeroY)
        path.lineTo(zeroX + nSize, zeroY + nSize)
        path.lineTo(zeroX, zeroY + nSize)
        path.close()

        canvas.drawPath(
            path,
            paint
        )
    }

    private fun drawAngledBottomLeft(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        canvas.save()
        canvas.rotate(
            90f,
            renderOptions.borderWidth + (col + 0.5f) * nSize,
            renderOptions.borderWidth + (row + 0.5f) * nSize
        )
        drawAngledBottomRight(canvas, renderOptions, col, row, nSize, paint)
        canvas.restore()
    }

    private fun drawAngledTopRight(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        canvas.save()
        canvas.rotate(
            -90f,
            renderOptions.borderWidth + (col + 0.5f) * nSize,
            renderOptions.borderWidth + (row + 0.5f) * nSize
        )
        drawAngledBottomRight(canvas, renderOptions, col, row, nSize, paint)
        canvas.restore()
    }

    private fun drawAngledTopLeft(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        canvas.save()
        canvas.rotate(
            180f,
            renderOptions.borderWidth + (col + 0.5f) * nSize,
            renderOptions.borderWidth + (row + 0.5f) * nSize
        )
        drawAngledBottomRight(canvas, renderOptions, col, row, nSize, paint)
        canvas.restore()
    }

    private fun drawEndBitUpright(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        val zeroX = renderOptions.borderWidth + col * nSize.toFloat()
        val zeroY = renderOptions.borderWidth + row * nSize.toFloat()
        val path = Path()
        path.moveTo(zeroX, zeroY + nSize)
        path.quadTo(zeroX, zeroY, zeroX + (nSize / 6), zeroY)
        val midpointX = zeroX + (nSize / 6) + ((zeroX + nSize) - (zeroX + (nSize / 6))) / 2
        val midpointY = zeroY + ((zeroY + 2 * (nSize / 3)) - zeroY) / 2
        path.quadTo(
            midpointX + (nSize / 6),
            midpointY - (nSize / 6),
            zeroX + nSize,
            zeroY + 2 * (nSize / 3)
        )
        path.lineTo(zeroX + nSize, zeroY + nSize)
        path.lineTo(zeroX, zeroY + nSize)
        path.close()
        canvas.drawPath(
            path,
            paint
        )
    }

    private fun drawEndBitUpsideDown(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        canvas.save()
        canvas.rotate(
            180f,
            renderOptions.borderWidth + (col + 0.5f) * nSize,
            renderOptions.borderWidth + (row + 0.5f) * nSize
        )
        drawEndBitUpright(canvas, renderOptions, col, row, nSize, paint)
        canvas.restore()
    }

    private fun drawEndBitToTheRight(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        canvas.save()
        canvas.rotate(
            90f,
            renderOptions.borderWidth + (col + 0.5f) * nSize,
            renderOptions.borderWidth + (row + 0.5f) * nSize
        )
        drawEndBitUpright(canvas, renderOptions, col, row, nSize, paint)
        canvas.restore()
    }

    private fun drawEndBitToTheLeft(
        canvas: Canvas,
        renderOptions: RenderOption,
        col: Int,
        row: Int,
        nSize: Int,
        paint: Paint
    ) {
        canvas.save()
        canvas.rotate(
            -90f,
            renderOptions.borderWidth + (col + 0.5f) * nSize,
            renderOptions.borderWidth + (row + 0.5f) * nSize
        )
        drawEndBitUpright(canvas, renderOptions, col, row, nSize, paint)
        canvas.restore()
    }

    private fun getByteMatrix(
        contents: String,
        errorCorrectionLevel: ErrorCorrectionLevel
    ): ByteMatrix? {
        try {
            val qrCode = getProtoQrCode(contents, errorCorrectionLevel)
            val agnCenter = qrCode.version.alignmentPatternCenters
            val byteMatrix = qrCode.matrix
            val matSize = byteMatrix.width
            for (row in 0 until matSize) {
                for (col in 0 until matSize) {
                    if (isTypeAGN(col, row, agnCenter, true)) {
                        if (byteMatrix.get(col, row) != BYTE_EPT) {
                            byteMatrix.set(col, row, BYTE_AGN)
                        } else {
                            byteMatrix.set(col, row, BYTE_PTC)
                        }
                    } else if (isTypePOS(col, row, matSize, true)) {
                        if (byteMatrix.get(col, row) != BYTE_EPT) {
                            byteMatrix.set(col, row, BYTE_POS)
                        } else {
                            byteMatrix.set(col, row, BYTE_PTC)
                        }
                    } else if (isTypeTMG(col, row, matSize)) {
                        if (byteMatrix.get(col, row) != BYTE_EPT) {
                            byteMatrix.set(col, row, BYTE_TMG)
                        } else {
                            byteMatrix.set(col, row, BYTE_PTC)
                        }
                    }

                    if (isTypePOS(col, row, matSize, false)) {
                        if (byteMatrix.get(col, row) == BYTE_EPT) {
                            byteMatrix.set(col, row, BYTE_PTC)
                        }
                    }
                }
            }
            return byteMatrix
        } catch (e: WriterException) {
            e.printStackTrace()
        }

        return null
    }

    /**
     * @param contents             Contents to encode.
     * @param errorCorrectionLevel ErrorCorrectionLevel
     * @return QR code object.
     * @throws WriterException Refer to the messages below.
     */
    @Throws(WriterException::class)
    private fun getProtoQrCode(
        contents: String,
        errorCorrectionLevel: ErrorCorrectionLevel
    ): QRCode {
        if (contents.isEmpty()) {
            throw IllegalArgumentException("Found empty content.")
        }
        val hintMap = Hashtable<EncodeHintType, Any>()
        hintMap[EncodeHintType.CHARACTER_SET] = "UTF-8"
        hintMap[EncodeHintType.ERROR_CORRECTION] = errorCorrectionLevel
        return Encoder.encode(contents, errorCorrectionLevel, hintMap)
    }

    private fun isTypeAGN(x: Int, y: Int, agnCenter: IntArray, edgeOnly: Boolean): Boolean {
        if (agnCenter.isEmpty()) return false
        val edgeCenter = agnCenter[agnCenter.size - 1]
        for (agnY in agnCenter) {
            for (agnX in agnCenter) {
                if (edgeOnly && agnX != 6 && agnY != 6 && agnX != edgeCenter && agnY != edgeCenter)
                    continue
                if (agnX == 6 && agnY == 6 || agnX == 6 && agnY == edgeCenter || agnY == 6 && agnX == edgeCenter)
                    continue
                if (x >= agnX - 2 && x <= agnX + 2 && y >= agnY - 2 && y <= agnY + 2)
                    return true
            }
        }
        return false
    }

    private fun isTypePOS(x: Int, y: Int, size: Int, inner: Boolean): Boolean {
        return if (inner) {
            x < 7 && (y < 7 || y >= size - 7) || x >= size - 7 && y < 7
        } else {
            x <= 7 && (y <= 7 || y >= size - 8) || x >= size - 8 && y <= 7
        }
    }

    private fun isTypeTMG(x: Int, y: Int, size: Int): Boolean {
        return y == 6 && x >= 8 && x < size - 8 || x == 6 && y >= 8 && y < size - 8
    }
}