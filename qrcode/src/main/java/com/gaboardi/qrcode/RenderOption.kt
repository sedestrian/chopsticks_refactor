package com.gaboardi.qrcode

import android.graphics.Color
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel

class RenderOption {
    var content = "Makes QR Codes Great Again."
    var size = 600
    var borderWidth = 30
    var clearBorder = true
    var color: Int = Color.BLACK
    var ecl = ErrorCorrectionLevel.M

    fun duplicate(): RenderOption {
        val renderConfig = RenderOption()
        renderConfig.content = content
        renderConfig.size = size
        renderConfig.borderWidth = borderWidth
        renderConfig.clearBorder = clearBorder
        renderConfig.color = color
        renderConfig.ecl = ecl
        return renderConfig
    }
}