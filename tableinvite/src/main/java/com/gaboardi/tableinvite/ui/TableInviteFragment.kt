package com.gaboardi.tableinvite.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gaboardi.commonui.extensions.colorCode
import com.gaboardi.commonui.extensions.getColorAttribute
import com.gaboardi.tableinvite.R
import com.gaboardi.tableinvite.databinding.FragmentGroupInvitationBinding
import com.gaboardi.tableinvite.mvi.TableInviteState
import com.gaboardi.tableinvite.viewmodel.TableInviteViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class TableInviteFragment: Fragment() {
    private lateinit var binding: FragmentGroupInvitationBinding

    private val args: TableInviteFragmentArgs by navArgs()
    private val viewModel: TableInviteViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGroupInvitationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view, insetsListener)

        binding.buttonNext.isVisible = args.showCta
        binding.backButton.setOnClickListener {
            findNavController().navigateUp()
        }

        lifecycleScope.launchWhenCreated {
            viewModel.container.stateFlow.collect {
                render(it)
            }
        }

        viewModel.loadTable(args.tableId)
    }

    private fun render(state: TableInviteState){
        state.table?.code?.let { code ->
            renderCode(code)
        }
        state.tableQr?.let { qr ->
            binding.qrCode.setImageBitmap(qr)
        }
    }

    private fun renderCode(code: String){
        val dashed = code.chunked(4).joinToString(" - ")
        val styled = dashed.colorCode(requireContext().getColorAttribute(R.attr.colorPrimary))
        binding.code.text = styled
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val window = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        view.updatePadding(top = window.top, bottom = window.bottom)
        insets
    }
}