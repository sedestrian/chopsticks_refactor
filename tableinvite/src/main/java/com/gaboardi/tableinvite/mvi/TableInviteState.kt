package com.gaboardi.tableinvite.mvi

import android.graphics.Bitmap
import com.gaboardi.table.model.ui.Table

data class TableInviteState(
    val table: Table? = null,
    val tableQr: Bitmap? = null,
)