package com.gaboardi.tableinvite.viewmodel

import androidx.lifecycle.ViewModel
import com.gaboardi.qrcode.AwesomeQrRenderer
import com.gaboardi.qrcode.RenderOption
import com.gaboardi.table.repository.TableRepository
import com.gaboardi.tableinvite.mvi.TableInviteSideEffects
import com.gaboardi.tableinvite.mvi.TableInviteState
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.coroutines.transformFlow
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.syntax.strict.orbit
import org.orbitmvi.orbit.syntax.strict.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class TableInviteViewModel @Inject constructor(
    private val tableRepository: TableRepository
) : ViewModel(), ContainerHost<TableInviteState, TableInviteSideEffects> {

    override val container: Container<TableInviteState, TableInviteSideEffects> = container(
        TableInviteState()
    )

    fun loadTable(tableId: String) = orbit {
        subscribeToTable(tableId)
        transformFlow {
            tableRepository.observeTable(tableId)
        }.reduce {
            event?.dynamicLink?.let {
                getQrBitmap(it)
            }
            state.copy(table = event)
        }
    }

    fun subscribeToTable(tableId: String) = orbit {
        transformFlow {
            tableRepository.startTableSubscription(tableId)
        }.reduce {
            state.copy()
        }
    }

    private fun getQrBitmap(code: String) = intent {
        val options = RenderOption().apply {
            content = code
            size = 800
        }
        val bitmap = AwesomeQrRenderer.render(options)
        reduce { state.copy(tableQr = bitmap) }
    }
}