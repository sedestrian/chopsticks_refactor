package com.gaboardi.home.ui

import android.content.Intent
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.*
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import coil.transform.CircleCropTransformation
import com.gaboardi.common.extensions.dp
import com.gaboardi.common.model.ErrorMessage
import com.gaboardi.commonui.extensions.setGone
import com.gaboardi.commonui.extensions.setVisible
import com.gaboardi.home.R
import com.gaboardi.home.adapter.RestaurantsAdapter
import com.gaboardi.home.databinding.FragmentHomeBinding
import com.gaboardi.home.dialog.RestaurantActionsBottomSheetDialog
import com.gaboardi.home.dialog.SortRestaurantsBottomSheetDialog
import com.gaboardi.home.viewmodel.HomeViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private val viewModel: HomeViewModel by viewModels()

    private val sortRestaurantsTag = "SortRestaurants"
    private val restaurantActionsTag = "RestaurantActions"

    private lateinit var binding: FragmentHomeBinding
    private lateinit var adapter: RestaurantsAdapter

    @ExperimentalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        observe()
        setupAppbarHeightForTitle()
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        ViewCompat.setOnApplyWindowInsetsListener(binding.buttonAdd, insetsListener)

        setupAdapter()
        setupRestaurantRecycler()
        listen()
    }

    private fun setupAppbarHeightForTitle() {
        val title = getString(R.string.home_title)
        val bounds = Rect()
        val size =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 30f, resources.displayMetrics)

        Paint().apply {
            typeface = Typeface.create("comfortaa_bold", Typeface.BOLD)
            textSize = size
        }.getTextBounds(title, 0, title.length, bounds)

        val height = bounds.height() + 128.dp + 112.dp

        binding.appBar.layoutParams.height = height
        binding.collapsingToolbar.expandedTitleMarginBottom = 64.dp + 54.dp
    }

    private fun setupAdapter() {
        adapter = RestaurantsAdapter()
        adapter.onRestaurantClicked {
            val action = HomeFragmentDirections.actionHomeFragmentToRestaurantDetailFlow(it.id)
            findNavController().navigate(action)
        }

        adapter.onRestaurantLongClicked {
            val dialog = RestaurantActionsBottomSheetDialog {
                viewModel.deleteRestaurant(it.id)
            }
            dialog.show(childFragmentManager, restaurantActionsTag)
        }

        adapter.onNavigationClicked {
            val uri = Uri.parse(it)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    private fun setupRestaurantRecycler() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
    }

    @ExperimentalCoroutinesApi
    private fun listen() {
        binding.buttonAdd.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToRestaurantCreationFlow()
            findNavController().navigate(action)
        }

        binding.filterIcon.setOnClickListener {
            val dialog = SortRestaurantsBottomSheetDialog(viewModel.sortChannel) {
                viewModel.sortChannel.trySend(it)
            }
            dialog.show(childFragmentManager, sortRestaurantsTag)
        }

        binding.profilePicture.setOnClickListener {
            viewModel.logout()
        }

        binding.searchBox.doAfterTextChanged {
            it?.toString()?.let { filter ->
                viewModel.filterChannel.trySend(filter)
            }
        }

        binding.searchIcon.setOnClickListener {
            if (!binding.searchBox.isVisible) {
                binding.searchLayout.updateLayoutParams<CoordinatorLayout.LayoutParams> {
                    val searchPosition = binding.searchLayout.x
                    width = searchPosition.toInt()
                }
                binding.searchBox.setVisible()
            } else {
                binding.searchLayout.updateLayoutParams<CoordinatorLayout.LayoutParams> {
                    width = CoordinatorLayout.LayoutParams.WRAP_CONTENT
                }
                binding.searchBox.setGone()
            }
        }
    }

    @FlowPreview
    private fun observe() {
        viewModel.restaurants.observe(viewLifecycleOwner) {
            adapter.setItems(it)
        }

        viewModel.logoutFinished.observe(viewLifecycleOwner) { event ->
            event.consume {
                val action = HomeFragmentDirections.actionHomeFragmentToSplashFlow()
                findNavController().navigate(action)
            }
        }

        viewModel.profilePicture.observe(viewLifecycleOwner) {
            binding.profilePicture.load(it){
                placeholder(R.drawable.anonymous_padding)
                error(R.drawable.anonymous_padding)
                transformations(CircleCropTransformation())
            }
        }

        viewModel.restaurantsError.observe(viewLifecycleOwner) { event ->
            event.consume {
                val errorColor = ContextCompat.getColor(requireContext(), R.color.error_background)
                val textColor = ContextCompat.getColor(requireContext(), R.color.error_text)
                val message = when(it){
                    is ErrorMessage.StringError -> it.message
                    is ErrorMessage.ResourceError -> getString(it.message)
                }
                Snackbar.make(binding.coordinator, message, Snackbar.LENGTH_LONG)
                    .setBackgroundTint(errorColor)
                    .setTextColor(textColor)
                    .show()
            }
        }
    }

    private val insetsListener = OnApplyWindowInsetsListener { view, insets ->
        val sysWindow = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        binding.root.updatePadding(top = sysWindow.top, bottom = sysWindow.bottom)
        insets
    }
}