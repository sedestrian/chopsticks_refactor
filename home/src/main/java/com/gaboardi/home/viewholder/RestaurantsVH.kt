package com.gaboardi.home.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.commonui.extensions.setGone
import com.gaboardi.commonui.extensions.setVisible
import com.gaboardi.commonui.model.RestaurantType
import com.gaboardi.home.R
import com.gaboardi.home.databinding.RestaurantsItemBinding
import com.gaboardi.restaurant.model.ui.*
import java.time.DayOfWeek
import java.time.LocalDateTime

class RestaurantsVH(val binding: RestaurantsItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(
        restaurant: RestaurantUI,
        listener: ((restaurant: RestaurantUI) -> Unit)?,
        navigation: ((query: String) -> Unit)?,
        longClick: ((restaurant: RestaurantUI) -> Unit)?
    ) {
        binding.name.text = restaurant.name
        binding.address.text = restaurant.address
        val resource = RestaurantType.fromId(restaurant.category).string
        binding.category.setText(resource)

        binding.root.setOnClickListener {
            listener?.invoke(restaurant)
        }
        binding.root.setOnLongClickListener {
            longClick?.invoke(restaurant)
            true
        }
        binding.mapsButton.setOnClickListener {
            navigation?.invoke("google.navigation:q=${restaurant.name},${restaurant.address}")
        }

        setupPrices(restaurant.prices)
        setupVotes(restaurant)
    }

    fun bindForChanges(changes: RestaurantItemChanges, navigation: ((query: String) -> Unit)?) {
        if (changes.name != null) {
            binding.name.text = changes.name
        }
        if (changes.address != null) {
            binding.address.text = changes.address
            binding.mapsButton.setOnClickListener {
                navigation?.invoke("google.navigation:q=${binding.name.text},${changes.address}")
            }
        }
        changes.category?.let { category ->
            val resource = RestaurantType.fromId(category).string
            binding.category.setText(resource)
        }
        changes.rating?.let { rating ->
            showRating()
            binding.ratingBar.rating = rating.toFloat()
        }
        changes.prices?.let { prices ->
            setupPrices(prices)
        }
    }

    private fun setupVotes(restaurant: RestaurantUI) {
        if (restaurant.averageRating == null) {
            hideRating()
        } else {
            showRating()
            val rating = restaurant.averageRating ?: 0.0
            binding.ratingBar.rating = rating.toFloat()
        }
    }

    private fun setupPrices(prices: List<Price>) {
        val filter = getDayFilter()
        val times = prices.filter { it.days == filter }

        val priceDay = times.firstOrNull { it.time == Time.LUNCH.id }
        val priceNight = times.firstOrNull { it.time == Time.DINNER.id }

        if (priceDay == null && priceNight == null) {
            hidePrices()
        } else {
            if (priceNight != null) {
                showNight()
                binding.priceNight.text =
                    binding.root.context.getString(R.string.price_value, priceNight.cost)
            } else {
                hideNight()
            }

            if (priceDay != null) {
                showDay()
                binding.priceDay.text =
                    binding.root.context.getString(R.string.price_value, priceDay.cost)
            } else {
                hideDay()
            }
        }
    }

    private fun hideRating() {
        binding.ratingLabel.setGone()
        binding.ratingBar.setGone()
    }

    private fun showRating() {
        binding.ratingLabel.setVisible()
        binding.ratingBar.setVisible()
    }

    private fun hidePrices() {
        hideDay()
        hideNight()
        binding.priceLabel.setGone()
    }

    private fun showPrices() {
        showDay()
        showNight()
        binding.priceLabel.setVisible()
    }

    private fun hideDay() {
        binding.priceDayIcon.setGone()
        binding.priceDay.setGone()
    }

    private fun showDay() {
        binding.priceDayIcon.setVisible()
        binding.priceDay.setVisible()
    }

    private fun hideNight() {
        binding.priceNightIcon.setGone()
        binding.priceNight.setGone()
    }

    private fun showNight() {
        binding.priceNightIcon.setVisible()
        binding.priceNight.setVisible()
    }

    private fun getDayFilter(): Int {
        val today = LocalDateTime.now()
        val dayOfWeek = today.dayOfWeek
        return if (dayOfWeek >= DayOfWeek.SATURDAY && dayOfWeek <= DayOfWeek.SUNDAY)
            Days.WEEKEND.id else Days.WEEK.id
    }
}