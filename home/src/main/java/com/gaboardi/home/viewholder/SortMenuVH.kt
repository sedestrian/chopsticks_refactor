package com.gaboardi.home.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.commonui.extensions.setGone
import com.gaboardi.commonui.extensions.setVisible
import com.gaboardi.home.databinding.SortMenuItemBinding
import com.gaboardi.restaurant.model.ui.SortMenuItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ConflatedBroadcastChannel

class SortMenuVH(val binding: SortMenuItemBinding) : RecyclerView.ViewHolder(binding.root) {
    @ExperimentalCoroutinesApi
    fun bind(item: SortMenuItem, selected: SortMenuItem?, onSortItemSelected: ((item: SortMenuItem) -> Unit)?){
        if(selected != null && selected == item){
            binding.selected.setVisible()
        }else{
            binding.selected.setGone()
        }
        binding.icon.setImageResource(item.icon)
        binding.name.setText(item.text)
        binding.container.setOnClickListener {
            onSortItemSelected?.invoke(item)
        }
    }
}