package com.gaboardi.home.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.gaboardi.home.adapter.SortMenuAdapter
import com.gaboardi.home.databinding.SortRestaurantsDialogBinding
import com.gaboardi.restaurant.model.ui.SortMenuItem
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.consume
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.launch

class SortRestaurantsBottomSheetDialog @ExperimentalCoroutinesApi constructor(
    val channel: ConflatedBroadcastChannel<SortMenuItem>,
    val onSortItemSelected: ((item: SortMenuItem) -> Unit)?
): BottomSheetDialogFragment() {
    private lateinit var binding: SortRestaurantsDialogBinding
    private lateinit var adapter: SortMenuAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SortRestaurantsDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = SortMenuAdapter {
            onSortItemSelected?.invoke(it)
            dismiss()
        }
        lifecycleScope.launch {
            channel.openSubscription().consumeEach {
                adapter.setSelectedItem(it)
            }
        }
        binding.recycler.layoutManager = LinearLayoutManager(requireContext())
        binding.recycler.adapter = adapter
    }
}