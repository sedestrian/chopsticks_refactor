package com.gaboardi.home.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaboardi.home.databinding.RestaurantActionsBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.channels.ConflatedBroadcastChannel

class RestaurantActionsBottomSheetDialog(val onDelete: (() -> Unit)?): BottomSheetDialogFragment() {
    private lateinit var binding: RestaurantActionsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = RestaurantActionsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.deleteLayout.setOnClickListener {
            onDelete?.invoke()
            dismiss()
        }
    }
}