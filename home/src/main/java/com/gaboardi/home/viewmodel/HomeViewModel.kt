package com.gaboardi.home.viewmodel

import android.net.Uri
import androidx.lifecycle.*
import com.gaboardi.common.model.ErrorMessage
import com.gaboardi.common.model.Event
import com.gaboardi.common.model.State
import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.gaboardi.restaurant.model.ui.SortMenuItem
import com.gaboardi.restaurant.usecase.DeleteRestaurantUseCase
import com.gaboardi.restaurant.usecase.ObserveRestaurantsUseCase
import com.gaboardi.user.repository.UserRepository
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val observeRestaurantsUseCase: ObserveRestaurantsUseCase,
    private val deleteRestaurantUseCase: DeleteRestaurantUseCase,
    private val loginRepository: UserRepository,
    private val auth: FirebaseAuth,
) : ViewModel() {
    @ExperimentalCoroutinesApi
    val sortChannel = ConflatedBroadcastChannel(SortMenuItem.RATING_ASC)

    @ExperimentalCoroutinesApi
    val filterChannel = ConflatedBroadcastChannel("")

    private val _restaurantsLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    val restaurantsLoading: LiveData<Boolean> = _restaurantsLoading

    @FlowPreview
    @ExperimentalCoroutinesApi
    private val _restaurants: LiveData<List<RestaurantUI>> =
        sortChannel
            .asFlow()
            .flatMapLatest {
                observeRestaurantsData(it)
            }
            .combine(filterChannel.asFlow()) { data, filter ->
                val result = data.filter {
                    it.name.contains(filter, true) ||
                            it.address.contains(filter, true)
                }
                result
            }
            .asLiveData()

    @FlowPreview
    val restaurants: LiveData<List<RestaurantUI>> = _restaurants

    private val _restaurantsError: MutableLiveData<Event<ErrorMessage>> = MutableLiveData()
    val restaurantsError: LiveData<Event<ErrorMessage>> = _restaurantsError

    private val _profilePicture: MutableLiveData<Uri> = MutableLiveData()
    val profilePicture: LiveData<Uri> = _profilePicture

    private val _logoutLoading: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val logoutLoading: LiveData<Event<Boolean>> = _logoutLoading

    private val _logoutFinished: MutableLiveData<Event<Unit>> = MutableLiveData()
    val logoutFinished: LiveData<Event<Unit>> = _logoutFinished

    private val _logoutError: MutableLiveData<Event<ErrorMessage>> = MutableLiveData()
    val logoutError: LiveData<Event<ErrorMessage>> = _logoutError

    init {
        getProfilePicture()
    }

    private fun observeRestaurantsData(sort: SortMenuItem): Flow<List<RestaurantUI>> {
        return observeRestaurantsUseCase.observeRestaurants(sort)
    }

    fun deleteRestaurant(restaurantId: String) {
        viewModelScope.launch {
            deleteRestaurantUseCase.delete(restaurantId).collect { state ->
                when (state) {
                    is State.Loading -> {
                    }
                    is State.Success -> {
                    }
                    is State.Error -> {
                        _restaurantsError.postValue(Event(state.message))
                    }
                }
            }
        }
    }

    fun logout() {
        viewModelScope.launch {
            loginRepository.logout().collect { state ->
                when (state) {
                    is State.Loading -> _logoutLoading.postValue(Event(true))
                    is State.Success -> {
                        _logoutFinished.postValue(Event(Unit))
                        _logoutLoading.postValue(Event(false))
                    }
                    is State.Error -> {
                        _logoutError.postValue(Event(state.message))
                        _logoutLoading.postValue(Event(false))
                    }
                }
            }
        }
    }

    private fun getProfilePicture() {
        val user = auth.currentUser
        user?.let {
            _profilePicture.postValue(it.photoUrl)
        }
    }
}