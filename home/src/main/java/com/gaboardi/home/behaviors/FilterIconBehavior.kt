package com.gaboardi.home.behaviors

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.*
import com.gaboardi.common.extensions.dp
import com.gaboardi.home.R
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.appbar.MaterialToolbar

class FilterIconBehavior(val context: Context, attrs: AttributeSet?) :
    CoordinatorLayout.Behavior<ImageView>(context, attrs) {
    private var listener: AppBarLayout.OnOffsetChangedListener? = null

    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: ImageView,
        dependency: View
    ): Boolean {
        return dependency is AppBarLayout
    }

    override fun onDependentViewChanged(
        parent: CoordinatorLayout,
        child: ImageView,
        dependency: View
    ): Boolean {
        val appBarLayout = dependency as? AppBarLayout

        return appBarLayout?.let { appBar ->
            val maxScrollDistance = appBar.totalScrollRange

            appBar.removeOnOffsetChangedListener(listener)
            listener = AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                val expandedPercentageFactor: Float = (maxScrollDistance + verticalOffset) / maxScrollDistance.toFloat()

                child.y = appBarLayout.bottom.toFloat() - 16.dp - child.height
                child.x = appBarLayout.width - child.width - (48.dp * (1 - expandedPercentageFactor)) - child.marginRight
            }
            appBar.addOnOffsetChangedListener(listener)
            true
        } ?: false
    }
}