package com.gaboardi.home.behaviors

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.*
import com.gaboardi.common.extensions.dp
import com.gaboardi.home.R
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.appbar.MaterialToolbar

class SearchIconBehavior(context: Context, attrs: AttributeSet) :
    CoordinatorLayout.Behavior<ConstraintLayout>(context, attrs) {

    private var statusBarHeight = 0

    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: ConstraintLayout,
        dependency: View
    ): Boolean {
        return dependency is AppBarLayout
    }

    override fun onApplyWindowInsets(
        coordinatorLayout: CoordinatorLayout,
        child: ConstraintLayout,
        insets: WindowInsetsCompat
    ): WindowInsetsCompat {
        val bars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        statusBarHeight = bars.top
        return insets
    }

    override fun onDependentViewChanged(
        parent: CoordinatorLayout,
        child: ConstraintLayout,
        dependency: View
    ): Boolean {
        val appbar = dependency as AppBarLayout
        val coll =
            appbar.children.first { it is CollapsingToolbarLayout } as CollapsingToolbarLayout
        val toolbar = coll.children.first { it is MaterialToolbar } as MaterialToolbar
        val min = toolbar.height + statusBarHeight
        val progress =
            (appbar.bottom.toFloat() - min.toFloat()) / (appbar.height.toFloat() - min.toFloat())

        layout(progress, child, appbar)

        return true
    }

    private fun layout(progress: Float, child: ConstraintLayout, appBar: AppBarLayout) {
        val searchWidth = child.findViewById<ImageView>(R.id.searchIcon)?.width ?: 0
        val searchBar = child.findViewById<EditText>(R.id.searchBox)

        child.y = appBar.bottom.toFloat() - child.height
        child.x =
            appBar.width - child.width - (48.dp * (1 - progress)) - child.marginRight - 32.dp
        child.updateLayoutParams {
            width = if (searchBar != null && searchBar.isVisible)
                appBar.width - ((48.dp * (1 - progress)).toInt() + child.marginRight + 32.dp + searchWidth)
            else
                ViewGroup.LayoutParams.WRAP_CONTENT
        }
        if (searchBar != null && searchBar.isVisible)
            appBar.alpha = progress
        else
            appBar.alpha = 1f
    }
}