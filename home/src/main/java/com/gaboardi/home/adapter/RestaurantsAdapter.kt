package com.gaboardi.home.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.home.databinding.RestaurantsItemBinding
import com.gaboardi.home.viewholder.RestaurantsVH
import com.gaboardi.restaurant.model.ui.RestaurantItemChanges
import com.gaboardi.restaurant.model.ui.RestaurantUI
import com.google.android.material.shape.ShapeAppearanceModel

class RestaurantsAdapter : RecyclerView.Adapter<RestaurantsVH>() {
    private lateinit var inflater: LayoutInflater
    private val items: MutableList<RestaurantUI> = mutableListOf()

    private var restaurantLongClicked: ((restaurant: RestaurantUI) -> Unit)? = null
    private var restaurantClicked: ((restaurant: RestaurantUI) -> Unit)? = null
    private var navigationClicked: ((query: String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantsVH {
        if (!this::inflater.isInitialized)
            inflater = LayoutInflater.from(parent.context)
        val binding = RestaurantsItemBinding.inflate(inflater, parent, false)
        return RestaurantsVH(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RestaurantsVH, position: Int) {
        holder.bind(items[position],{
            restaurantClicked?.invoke(it)
        },{
            navigationClicked?.invoke(it)
        },{
            restaurantLongClicked?.invoke(it)
        })
    }

    override fun onBindViewHolder(
        holder: RestaurantsVH,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if(payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        }else{
            payloads.forEach {
                holder.bindForChanges(it as RestaurantItemChanges) {
                    navigationClicked?.invoke(it)
                }
            }
        }
    }

    fun onRestaurantLongClicked(listener: ((restaurant: RestaurantUI) -> Unit)?){
        restaurantLongClicked = listener
    }

    fun onRestaurantClicked(listener: ((restaurant: RestaurantUI) -> Unit)?){
        restaurantClicked = listener
    }

    fun onNavigationClicked(listener: ((query: String) -> Unit)?){
        navigationClicked = listener
    }

    fun setItems(newItems: List<RestaurantUI>){
        val diff = RestaurantDiffUtil(items, newItems)
        val diffResult = DiffUtil.calculateDiff(diff)
        diffResult.dispatchUpdatesTo(this)
        items.clear()
        items.addAll(newItems)
    }
}