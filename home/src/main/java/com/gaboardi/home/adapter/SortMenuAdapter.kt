package com.gaboardi.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.home.databinding.SortMenuItemBinding
import com.gaboardi.home.viewholder.SortMenuVH
import com.gaboardi.restaurant.model.ui.SortMenuItem
import kotlinx.coroutines.ExperimentalCoroutinesApi

class SortMenuAdapter @ExperimentalCoroutinesApi constructor(
    val onSortItemSelected: ((item: SortMenuItem) -> Unit)?
) : RecyclerView.Adapter<SortMenuVH>() {
    private lateinit var inflater: LayoutInflater
    private var selected: SortMenuItem? = null

    val items = listOf(
        SortMenuItem.RATING_ASC,
        SortMenuItem.RATING_DESC,
        SortMenuItem.DATE_ASC,
        SortMenuItem.DATE_DESC,
        SortMenuItem.NAME_ASC,
        SortMenuItem.NAME_DESC
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortMenuVH {
        if (!this::inflater.isInitialized) inflater = LayoutInflater.from(parent.context)
        val binding = SortMenuItemBinding.inflate(inflater, parent, false)
        return SortMenuVH(binding)
    }

    override fun onBindViewHolder(holder: SortMenuVH, position: Int) {
        holder.bind(items[position], selected) {
            onSortItemSelected?.invoke(it)
        }
    }

    fun setSelectedItem(item: SortMenuItem) {
        val oldIndex = items.indexOf(selected)
        val newIndex = items.indexOf(item)
        selected = item
        if (oldIndex != newIndex)
            notifyItemChanged(oldIndex)
        notifyItemChanged(newIndex)
    }

    override fun getItemCount(): Int = items.size
}