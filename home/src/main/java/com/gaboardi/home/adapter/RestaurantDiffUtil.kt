package com.gaboardi.home.adapter

import androidx.recyclerview.widget.DiffUtil
import com.gaboardi.restaurant.model.ui.RestaurantItemChanges
import com.gaboardi.restaurant.model.ui.RestaurantUI

class RestaurantDiffUtil(
    private val oldList: List<RestaurantUI>,
    private val newList: List<RestaurantUI>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size
    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem == newItem
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        val changes = RestaurantItemChanges(
            if (oldItem.name != newItem.name)
                newItem.name else null,

            if (oldItem.address != newItem.address)
                newItem.address else null,

            if (oldItem.category != newItem.category)
                newItem.category else null,

            if (newItem.averageRating != null &&
                oldItem.averageRating != newItem.averageRating
            )
                newItem.averageRating else null,

            if (!oldItem.prices.containsAll(newItem.prices) || !newItem.prices.containsAll(oldItem.prices))
                newItem.prices else null
        )

        return if (changes.isEmpty()) null else changes
    }
}